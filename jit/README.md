# JIT Backend

The JIT backend consists of two main parts. The Session Service (REST API) and Streaming Service ([WebRTC](https://en.wikipedia.org/wiki/WebRTC) streaming). There also need to exist a [STUN](https://en.wikipedia.org/wiki/STUN) or [TURN](https://en.wikipedia.org/wiki/Traversal_Using_Relays_around_NAT) Service for WebRTC connection establishment, there are many [free alternatives](https://gist.github.com/sagivo/3a4b2f2c7ac6e1b5267c2f1f59ac6c6b), or you can host your own. Here's an architectural overview:

![architecture.png](architecture.png)

The backend services run in docker containers. You will need access to Codemill docker repository. To get access, contact your sales representative, or if you don't have one yet, fill in the [contact form](https://www.codemill.se/contact), and we will reach out to you.

## Session Service

The Session Service spawns a Streaming Service when the Frontend client requests to open a video file and helps the Frontend client exchange information with the Streaming Service in order to establish WebRTC connectivity.

### Installation

1. (Prerequisite): [Install docker](https://docs.docker.com/get-docker/)
1. Login to the docker repository. You will be prompted for username and password.
    ```sh
    docker login codemill-docker.jfrog.io
    ```
1. Pull the Session Service image:
    ```sh
    docker pull codemill-docker.jfrog.io/accurate-player/accurate-player-jit/jit-session-service:latest
    ```
1. Read the documentation for the Session Service 
    ```sh
    docker run --rm codemill-docker.jfrog.io/accurate-player/accurate-player-jit/jit-session-service:latest /opt/jitsession/docker-run.sh --help
    ```
   
    ```
    usage: main.pyc [-h] [--public-ip PUBLIC_IP] [--host HOST] [--port PORT]
                    [--container-port-min CONTAINER_PORT_MIN]
                    [--container-port-max CONTAINER_PORT_MAX]
                    [--max-running-containers MAX_RUNNING_CONTAINERS] [--turn]
                    [--image IMAGE | --local-image LOCAL_IMAGE] [--prune] [--rm]
                    [--verbose] [--dev] [--enable-strace]
    
    JIT session service
    
    options:
      -h, --help            show this help message and exit
      --public-ip PUBLIC_IP
                            Used when building the jitUrl in /session response. If
                            omitted, env PUBLIC_IP is used. If not set, the
                            default 127.0.0.1 is used.
      --host HOST           Host for HTTP server. If omitted, env JIT_SESSION_HOST
                            is used. If not set, the default 0.0.0.0 is used.
      --port PORT           Port for HTTP server. If omitted, env JIT_SESSION_PORT
                            is used. If not set, the default 8080 is used.
      --container-port-min CONTAINER_PORT_MIN
                            Container port randomiser range min value. If omitted,
                            env JIT_CONTAINER_PORT_MIN is used. If not set, the
                            default 49152 is used.
      --container-port-max CONTAINER_PORT_MAX
                            Container port randomiser range max value. If omitted,
                            env JIT_CONTAINER_PORT_MAX is used. If not set, the
                            default 65535 is used.
      --max-running-containers MAX_RUNNING_CONTAINERS
                            Max concurrent running streaming services. If omitted,
                            env JIT_MAX_RUNNING_CONTAINERS is used. If not set,
                            the default 64 is used.
      --turn, -t            Starts a TURN server on default ports (3478 and 5349)
      --image IMAGE, -i IMAGE
                            Remote docker image URL (default: codemill-
                            docker.jfrog.io/accurate-player/accurate-player-
                            jit/jit-streaming-service:latest)
      --local-image LOCAL_IMAGE, -l LOCAL_IMAGE
                            Specify a local docker image instead of using
                            Codemill's docker registry
      --prune, -p           Prune all non-running containers with matching label
                            (jit-session-service-streaming-node), before serving
      --rm                  Force remove all already running containers with
                            matching label (jit-session-service-streaming-node),
                            before serving
      --verbose, -v         Verbose logging
      --dev                 Shortcut for: -v --rm --prune --turn --host 0.0.0.0
                            --port 8080, specific parameters may be overridden
      --enable-strace       QEMU_STRACE=1 is sent upon docker run, needed to debug
                            with strace when running on Mac
    ```
1. Run the docker image
    ```sh
   docker run \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    --label jit-session \
    --rm \
    --cap-add SYS_ADMIN \
    --cap-add SYS_RESOURCE \
    --security-opt apparmor:unconfined \
    -p 8080:8080 \
    --env PUBLIC_IP=$(curl -s ipinfo.io/ip)
    codemill-docker.jfrog.io/accurate-player/accurate-player-jit/jit-session-service:latest
    ```

   The approach of bind-mounting the Docker socket (`--volume /var/run/docker.sock:/var/run/docker.sock`) allows the Session Service to start Streaming Service containers as "sibling" containers. This is sometimes refered to as "docker-out-of-docker".

    | Environment variable         | Required | Default | Description                                                                                    |
    |------------------------------|----------|---------|------------------------------------------------------------------------------------------------|
    | `PUBLIC_IP`                  | Yes      | -       | Public IP-adress of the service.                                                               |
    | `DOCKER_REGISTRY_USERNAME`   | Yes      | -       | Docker registry username |
    | `DOCKER_REGISTRY_PASSWORD`   | Yes      | -       | Docker registry password |
    | `JIT_SESSION_HOST`           | No       | 0.0.0.0 | Adress to listen on                                                                            |
    | `JIT_SESSION_PORT`           | No       | 8080    | Port to use                                                                                    |
    | `JIT_CONTAINER_PORT_MIN`     | No       | 49152   | Container port randomiser range min value.                                                     |
    | `JIT_CONTAINER_PORT_MAX`     | No       | 65535   | Container port randomiser range max value.                                                     |
    | `JIT_MAX_RUNNING_CONTAINERS` | No       | 64      | Max concurrent running Streaming services                                                      |

**Note**: The Streaming Services started by the Session Service will [publish the assigned port](https://docs.docker.com/network/#published-ports) (in the range between `JIT_CONTAINER_PORT_MIN` and `JIT_CONTAINER_PORT_MAX`. If you have a firewall you need to make sure to allow network traffic on both TCP and UDP for these services.

## Streaming Service

The Streaming Service is connected to the Frontend via [WebRTC](https://en.wikipedia.org/wiki/WebRTC). The Frontend client controlls the video stream by sending/receiving commands to the Streaming Service over WebRTC.

To establish a direct connection between the Frontend and the Streaming Service a [STUN](https://en.wikipedia.org/wiki/STUN) or [TURN](https://en.wikipedia.org/wiki/Traversal_Using_Relays_around_NAT) Service is used.

This service is managed by the Session Service, you do not need to run this service manually.


## Known limitations
* It is not possible to play multiple video- or audio files. Currently, you can only play a single source file, you cannot have the video for example in one file and audio in another.
* You can play muxed audio, but only the first two channels (stereo) in each track. This means it is not possible to play 5.1, 7.1, multi-channel etc.
* The `DiscreteAudioPlugin` is not supported. This means you cannot load separate discrete audio files that sync up with the video. 
* The `ChannelControlPlugin` is not supported. This means you cannot use audio visualization components, or use functionality that controls audio output on channel level, or audio routing.
* It is not possible to play muxed subtitle tracks. You can however load subtitles as side-car files via the `TimedTextPlugin` in Accurate Player.

## FAQ
* > Can it play *insert format here*?

  We have tested it with a variaty of formats and codecs, e.g: H264, H265, Jpeg2000, ProRes 4444, AV1, JPEG-XS, XDCAM/Mpeg2. However, we encourage you to try it out to be sure.

* > Can it play HDR?

  The support is very limited. You can play HDR source files, but the representation will not be accurate to the source.

* > Does it support Watermarking?

  No

* > Does it play IMF containers?

  No

* > Does it support DRM?

  No

* > Why is the player taking a long time to load or stutter when I'm playing?
  
  Here is a list of a few things that can impact loading speed/playback:
  * It takes a long time to initialize the WebRTC connection (ICE). 
    * Make sure the STUN/TURN server that you are using is fast
  * Different formats/codecs have different decoding complexity
  * Some codecs do not support multi-threaded decoding
  * Larger (high bitrate, resolution etc) files takes a longer time to load
  * Network speed between the streaming service and media storage is slow
  * Network speed between the streaming service and client (browser) is slow
  * The server does not have enough resources. Most likely not enough CPUs when it comes to multi-threaded decoding.
  
