# Introduction

This article will showcase how you get started developing an application that uses Accurate Player JIT. Our hope it that this article will showcase how easy it is to set up the player and get started. Here is a sneak-peak of what we will
achieve:

![Finished application](docs/media/after_load.png)

Let's dig in!

# Parts

We've divided the tutorial into 2 different topics, we recommend that you start setting up the backend and then go on to create the frontend application.

1. [Backend](docs/BACKEND.md)
1. [Frontend](docs/FRONTEND.md)
