import './style.css'
import {JITPlayer} from "@accurate-player/accurate-player-jit";
import {
  Player,
  PlayerEventType,
} from "@accurate-player/accurate-player-core";

import { HotkeyPlugin } from "@accurate-player/accurate-player-plugins";

import "@accurate-player/accurate-player-controls";

let player: Player;

const applicationContainer = document.querySelector<HTMLDivElement>('#app')!;
applicationContainer.innerHTML = `
    <button id="loadbutton">Load video</button>
    <div id="status">Idle</div>
    <div class="video-container" >
      <video id="videomaster" autoplay></video>
      <apc-controls class="apc-video"></apc-controls>
    </div>
    <input type="range" id="quality-input-el" class="quality-input" min="1" max="100" value="50" disabled>
    <div>
      <label for="override-quality-el">Override quality manually</label>
      <input type="checkbox" id="override-quality-el" class="override-quality">
    </div>
`

const loadButton: HTMLButtonElement = document.querySelector("#loadbutton")!;
const overrideQualityCheckbox = document.getElementById("override-quality-el") as HTMLAudioElement;
const qualityInput = document.getElementById("quality-input-el") as HTMLAudioElement;

loadButton.addEventListener("click", () => {
  initPlayer();
  loadVideo();
});

function initPlayer() {
  const videoMaster: HTMLVideoElement = document.querySelector("#videomaster")!;
  const statusDiv: HTMLDivElement = document.querySelector("#status")!;
  player = new JITPlayer(videoMaster, window.LICENSE_KEY, {
    jitBackend: "http://localhost:8080",
    iceServers: [{
      urls: [
        "stun:stun.l.google.com:19302",
        "stun:stun1.l.google.com:19302",
        "stun:stun2.l.google.com:19302",
        "stun:stun3.l.google.com:19302",
        "stun:stun4.l.google.com:19302",
      ]
    }],
  });

  player.on(PlayerEventType.Loading, () => {
    statusDiv.innerText = "Loading (this may take a while)...";
  });
  player.on(PlayerEventType.Loaded, () => {
    statusDiv.innerText = "Loaded";
    player.api.play();
  });

  new HotkeyPlugin(player);

  const apControls = document.querySelector("apc-controls") as any;
  apControls.init(videoMaster, player);

  overrideQualityCheckbox.checked = false;
  overrideQualityCheckbox.addEventListener('change', function() {
    qualityInput.disabled = !overrideQualityCheckbox.checked;
    updateManualQuality();
  });
  qualityInput.addEventListener('change', function() {
    updateManualQuality();
  });
  player.on(PlayerEventType.StatusChanged, () => {
    if (!overrideQualityCheckbox.checked) {
      qualityInput.value = player.api.reportedQuality;
    }
  });
}

function loadVideo() {
  let jitVideoFile: JITVideoFile = {
    src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/timecode/sintel-2048-timecode-stereo.mp4",
  }
  jitVideoFile.frameRate = {
    numerator: 24,
    denominator: 1,
  };

  jitVideoFile.jit = {};
  jitVideoFile.jit.widthPx = 2048 / 2;
  jitVideoFile.jit.heightPx = 872 / 2;
  jitVideoFile.jit.lowres = 1;

  player.api.loadVideoFile(jitVideoFile);
}

function updateManualQuality()
{
  let value = Math.round(qualityInput.value as number);

  if (overrideQualityCheckbox.checked && value && value >= 1 && value <= 100)
  {
    player?.api.setQuality(value);
  }
  else
  {
    player?.api.setQuality(null);
  }
}
