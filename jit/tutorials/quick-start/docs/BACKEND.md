# JIT Backend - Quick start

This is a step-by-step tutorial that covers how you get the JIT backend up and running on localhost. At the end we will have the [JIT Session Service](../../../README.md#session-service) running, ready to respond to streaming requests from our [frontend application](./FRONTEND.md).

## Prerequisites

First, you need credentials to our docker image repository (`codemill-docker.jfrog.io`). This will be a username and password that you can get from your sales representative.

You also need `docker` installed, visit https://docs.docker.com/get-docker/ to get instructions for your operating system.

Test that your docker credentials are working, type this in your terminal:

```bash
docker login codemill-docker.jfrog.io
```

You will be prompted for your username and password, go ahead and type those in. You will get a message that says: `Login Succeeded` if successful. If you were unsuccessful, please double-check your username and password and try again.

After you are logged in, make sure you have access rights to the docker image that we are going to use, by trying to pull (download) the image:

```sh
docker pull codemill-docker.jfrog.io/accurate-player/accurate-player-jit/jit-session-service:latest
```

Make sure you did not get any error messages.

## Run the backend

Now we have to set up some environment variables that the image requires. Create a file called `.env` in a folder of your chosing:

```sh
mkdir backend
cd backend
touch .env
```

Edit the contents of the file using the editor of your choice and insert this content:

```
DOCKER_REGISTRY_USERNAME=<insert your docker username here>
DOCKER_REGISTRY_PASSWORD=<insert your docker password here>
PUBLIC_IP=<insert your public ip here>
```

Example:

```
echo "DOCKER_REGISTRY_USERNAME=myusername" >> .env
echo "DOCKER_REGISTRY_PASSWORD=mypassword" >> .env
echo "PUBLIC_IP=$(curl -s ipinfo.io/ip)" >> .env
```

Now we are ready to start the backend service with `docker`:

```
docker run \
--volume /var/run/docker.sock:/var/run/docker.sock \
--label jit-session \
--rm \
--cap-add SYS_ADMIN \
--cap-add SYS_RESOURCE \
--security-opt apparmor:unconfined \
--env-file .env \
-p 8080:8080 \
codemill-docker.jfrog.io/accurate-player/accurate-player-jit/jit-session-service:latest
```

*Note: You may need to add `--platform linux/amd64` as an argument if you are running on an ARM based cpu, like Apple M series.*

This starts the JIT Session Service on port localhost:8080, and it should be reachable from `http://localhost:8080`. You can try to reach it by calling the healthcheck endpoint:

```
curl http://localhost:8080/healthcheck
```

It should respond with status code 200 and `OK` in the reponse body.

We may also add the `--turn` argument (after the image name) when starting the image in order to start a STUN/TURN server on the side. This is needed to establish WebRTC connections. The turn server starts at `stun:localhost:3478`. You can also run without this flag and use any of the [free alternatives listed here](https://gist.github.com/sagivo/3a4b2f2c7ac6e1b5267c2f1f59ac6c6b).

You can read more about this option and more in the [JIT Session Service in the reference documentation](../../../README.md#session-service).

Now you have a running JIT backend!

Next up is creating a [frontend application](./FRONTEND.md).

← [Parts](../README.md#parts) | [Frontend](FRONTEND.md) →
