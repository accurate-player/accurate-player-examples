# Introduction

This tutorial will showcase the Timeline component that is part of Accurate Player SDK. We will show how easy it is to get started and fill your timeline with your content.

We will first go over how to structure content on the timeline in groups and rows, then we continue to go through some different content types you can add to the
timeline like markers, thumbnails, and waveforms to name a few.

The components are built on the Web Components standards to create custom elements that are framework agnostic. That means that you can use the components with
your favorite framework. With that said, some frameworks have some interoperability issues, but, in most cases, it is possible to work around those
issues. [Custom Element Everywhere](https://custom-elements-everywhere.com/) test frameworks and lists how well they interoperate if you are interested in learning more about
that.

We use [lit-html](https://lit-html.polymer-project.org/) in the html examples in this tutorial, but it should be fairly easy to translate to the framework of your liking.
Here's a brief explanation of the syntax, consult the [lit-html documentation](https://lit-html.polymer-project.org/guide/writing-templates) for further explanation:

Example lit-html syntax:

```html
<input class="${myClass}" ?disabled="${isDisabled}" .value="${value}" @input="${myCallbackFunction}"></element>
```

| Syntax | Explanation |
| --- | --- |
| `class="${myClass}"` | Attribute binding |
| `?disabled="${isDisabled}"` | Boolean attribute binding |
| `.value="${value}"` | Property binding |
| `@input="${myCallbackFunction}"` | Register event listener |

We also want to mention that we do not provide all the CSS and surrounding code in the code snippets that follow, we only include the most important parts. For a 
complete picture, checkout the source code in this repository, more specifically `index.html` and `src/Application.ts`.

Our hope is that this tutorial will showcase how easy it is to set up a basic timeline and fill it up with content. Let's dig in!

# Parts

We've divided the tutorial into 5 different topics, we recommend that you go through them one by one.

1. [Basics](docs/BASICS.md)
1. [Layout](docs/LAYOUT.md)
1. [Markers](docs/MARKERS.md)
1. [Thumbnails](docs/THUMBNAILS.md)
1. [Waveforms](docs/WAVEFORMS.md)


# Conclusion

If we bring together that we've learnt; layout, markers, thumbnails, and waveforms we get an application that looks like this:

!["Final application"](docs/images/application.jpg "Final application")

This concludes our timeline tutorial. We are very happy to answer any questions and show you more of our products. All the examples in this tutorial is available in our 
[examples repository](https://gitlab.com/accurate-player/accurate-player-examples) and hosted on our [demo page](https://futureurl.accurate.video). You'll need a license 
key to access the examples, and we are happy to provide you with one if you reach out to us.
