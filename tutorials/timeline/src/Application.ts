import { css, html, LitElement, PropertyValues, TemplateResult } from "lit";
import { customElement, property, query } from "lit/decorators.js";
import { ProgressivePlayer as AccuratePlayer } from "@accurate-player/accurate-player-progressive"
import { HotkeyPlugin, PointPlugin, SmoothTimeUpdatePlugin } from "@accurate-player/accurate-player-plugins";
import {
  ApScrollArea,
  ApTimelineAudioWaveform,
  ApTimelineBasic,
  ApTimelineGroup,
  ApTimelineMarker,
  ApTimelineMetadata,
  ApTimelineRow,
  ApTimelineThumbnail,
  ApTimelineThumbnails,
  AudioWaveformDrawType,
  Marker,
  RendererType
} from "@accurate-player/accurate-player-components-external";

import "@accurate-player/accurate-player-controls";

import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
import { FrameRate, PlayerEventType, SeekMode, TimeFormat } from "@accurate-player/accurate-player-core";
import WaveformData from "waveform-data";

/*
 * Component library components need to be registered with the customElements registry
 * before they can be used (as they are WebComponents, read more: https://developer.mozilla.org/en-US/docs/Web/Web_Components).
 *
 * Calling register() will call customElements.define() under the hood.
 *
 */
ApTimelineBasic.register(); // <ap-timeline-basic />
ApTimelineGroup.register(); // <ap-timeline-group />
ApTimelineRow.register(); // <ap-timeline-row />
ApScrollArea.register(); // <ap-scroll-area />
ApTimelineMetadata.register(); // <ap-timeline-metadata />
ApTimelineMarker.register(); // <ap-timeline-marker />
ApTimelineThumbnails.register(); // <ap-timeline-thumbnails />
ApTimelineThumbnail.register(); // <ap-timeline-thumbnail />
ApTimelineAudioWaveform.register(); // <ap-timeline-audio-waveform />

/**
 * Application root element.
 */
@customElement('av-application')
export class Application extends LitElement {
  static styles = css`
    :host {
      background: var(--AP-BACKGROUND-BODY);
      display: flex;
      padding: 20px;
      flex-direction: column;
      width: calc(100% - 40px);
      height: calc(100% - 40px);
    }

    div.container-video {
      display: flex;
      flex: 1;
      justify-content: center;
      min-height: 0;
      position: relative;
      margin-bottom: 20px;
    }

    div.container-timeline {
      display: flex;
      max-height: 50vh;
      flex: 1;
    }

    #id-master {
      min-height: 30vh;
      min-width: 70vw;
      max-width: 100%;
      max-height: 60vh;
    }
  `;

  @query("#id-master")
  master: HTMLVideoElement | undefined;

  @query("apc-controls")
  controls: any;

  @property()
  private player: AccuratePlayer | undefined;

  @property()
  private markers: Partial<Marker>[];

  @property()
  private thumbnails: Partial<Marker>[];

  @property()
  private waveformData: WaveformData;

  private hotkeyPlugin: HotkeyPlugin;
  private smoothTimeUpdatePlugin: SmoothTimeUpdatePlugin;
  private pointPlugin: PointPlugin;

  protected firstUpdated(changedProperties: PropertyValues) {
    super.firstUpdated(changedProperties);
    this.initializeAccuratePlayer();
  }

  protected render(): TemplateResult {
    return html`
      <div class="container-video">
        <video id="id-master" crossorigin="anonymous"></video>
        <apc-controls></apc-controls>
      </div>
      <div class="container-timeline">
        <!-- Render methods for the different steps that got us to the final result -->
          <!--           ${this.renderEmptyTimeline()}-->
          <!--        ${this.renderGroupsAndRows()}-->
<!--        ${this.renderMarkers()}-->
<!--           ${this.renderThumbnails()}-->
          <!-- ${this.renderWaveforms()} -->
                  ${this.renderAllCombined()}
      </div>
    `;
  }

  /**
   * Renders an empty timeline (step 1)
   */
  private renderEmptyTimeline(): TemplateResult {
    return html`
      <ap-timeline-basic .player="${this.player}" flex-height></ap-timeline-basic>
    `
  }

  /**
   * Renders timeline with groups and rows (step 2)
   */
  private renderGroupsAndRows(): TemplateResult {
    return html`
      <ap-timeline-basic .player="${this.player}" flex-height>
        <ap-timeline-row>
          <span slot="header">Row header 1</span>
          <span>Row content 1</span>
        </ap-timeline-row>
        <ap-scroll-area style="max-height: calc(100% - 32px)">
          <ap-timeline-group @apclick="${(e: Event) => {
            if (e.target instanceof ApTimelineGroup) {
              e.target.collapsed = !e.target.collapsed;
            }
          }}">
            <span slot="header">Group header 1</span>
            <ap-timeline-row>
              <span slot="header">Row header 2</span>
              <span>Row content 2</span>
            </ap-timeline-row>
            <ap-timeline-row>
              <span slot="header">Row header 3</span>
              <span>Row content 3</span>
            </ap-timeline-row>
            <ap-timeline-row>
              <span slot="header">Row header 4</span>
              <span>Row content 4</span>
            </ap-timeline-row>
          </ap-timeline-group>
          <ap-timeline-row>
            <span slot="header">Row header 5</span>
            <span>Row content 5</span>
          </ap-timeline-row>
        </ap-scroll-area>
      </ap-timeline-basic>
    `
  }

  /**
   * Renders markers (step 3)
   */
  private renderMarkers(): TemplateResult {
    return html`
      <ap-timeline-basic .player="${this.player}" flex-height>
        <ap-timeline-row>
          <span slot="header">Row header 1</span>
          <ap-timeline-metadata .markerStyle="${{backgroundColor: "var(--AP-WARNING)"}}" .collapsed="${false}">
            <ap-timeline-marker start="0" end="9000"></ap-timeline-marker>
            <ap-timeline-marker .data="${[
              {start: 0, end: 1000},
              {start: 1200, end: 1600},
              {start: 8000, end: 9100}
            ]}"></ap-timeline-marker>
            <ap-timeline-marker .data="${[
              {start: 2000, end: 2100},
              {start: 4000, end: 6000}
            ]}"></ap-timeline-marker>
          </ap-timeline-metadata>
        </ap-timeline-row>
        <ap-timeline-row>
          <span slot="header">Row header 2</span>
          <ap-timeline-metadata .rendererType="${RendererType.WEBGL}" .collapsed="${false}">
            <ap-timeline-marker .data="${this.markers}"></ap-timeline-marker>
          </ap-timeline-metadata>
        </ap-timeline-row>
      </ap-timeline-basic>
    `
  }

  /**
   * Renders thumbnails (step 4)
   */
  private renderThumbnails(): TemplateResult {
    return html`
      <ap-timeline-basic .player="${this.player}" flex-height>
        <ap-timeline-row>
          <span slot="header">Row header 1</span>
          <ap-timeline-thumbnails height="50">
            <ap-timeline-thumbnail .data="${this.thumbnails}"></ap-timeline-thumbnail>
          </ap-timeline-thumbnails>
        </ap-timeline-row>
      </ap-timeline-basic>
    `
  }

  /**
   * Renders waveforms (step 5)
   */
  private renderWaveforms(): TemplateResult {
    return html`
      <ap-timeline-basic .player="${this.player}" flex-height>
        <ap-timeline-row>
          <span slot="header">Row header 1</span>
          <ap-timeline-audio-waveform
            @apdatarequest="${this.onWaveformDataRequest2}"
            .dataRequestDebounceTime="${0}"
            .type="${AudioWaveformDrawType.STROKE}"
            .waveformStyle="${{
              stroke: "--AP-PRIMARY",
            }}"
            .sampleMin=${-127}
            .sampleMax=${127}>
          </ap-timeline-audio-waveform>
        </ap-timeline-row>
      </ap-timeline-basic>
    `
  }

  /**
   * Renders all steps combined
   */
  private renderAllCombined(): TemplateResult {
    return html`
      <ap-timeline-basic .player="${this.player}" flex-height>
        <ap-timeline-row>
          <span slot="header">Row header 1</span>
          <ap-timeline-thumbnails .height="${50}" @apmarkerclick="${this.onThumbnailClick}">
            <ap-timeline-thumbnail .data="${this.thumbnails}"></ap-timeline-thumbnail>
          </ap-timeline-thumbnails>
        </ap-timeline-row>
        <ap-timeline-row>
          <span slot="header">Row header 1</span>
          <ap-timeline-audio-waveform
            @apdatarequest="${this.onWaveformDataRequest2}"
            .dataRequestDebounceTime="${0}"
            .type="${AudioWaveformDrawType.STROKE}"
            .waveformStyle="${{
              stroke: "--AP-PRIMARY",
            }}"
            .sampleMin=${-127}
            .sampleMax=${127}>
          </ap-timeline-audio-waveform>
        </ap-timeline-row>
        <ap-scroll-area style="max-height: calc(100% - 32px - 50px)">
          <ap-timeline-group @apclick="${(e: Event) => {
            if (e.target instanceof ApTimelineGroup) {
              e.target.collapsed = !e.target.collapsed;
            }
          }}">
            <span slot="header">Group header 1</span>
            <ap-timeline-row>
              <span slot="header">Row header 2</span>
              <ap-timeline-metadata .markerStyle="${{backgroundColor: "var(--AP-WARNING)"}}" @apmarkerclick="${(e: CustomEvent) => {
                const [marker, api, mouseEvent] = e.detail.args;
                console.log(marker);
                console.log(api.nextMarker());
                console.log(mouseEvent.pageX, mouseEvent.pageY);
              }}">
                <ap-timeline-marker start="0" end="9000"></ap-timeline-marker>
                <ap-timeline-marker .data="${[
                  {start: 0, end: 1000},
                  {start: 1200, end: 1600},
                  {start: 8000, end: 9100}
                ]}"></ap-timeline-marker>
                <ap-timeline-marker .data="${[
                  {start: 2000, end: 2100},
                  {start: 4000, end: 6000}
                ]}"></ap-timeline-marker>
              </ap-timeline-metadata>
            </ap-timeline-row>
            <ap-timeline-row>
              <span slot="header">Row header 3</span>
              <ap-timeline-metadata .rendererType="${RendererType.WEBGL}">
                <ap-timeline-marker .data="${this.markers}"></ap-timeline-marker>
              </ap-timeline-metadata>
            </ap-timeline-row>
            <ap-timeline-row>
              <span slot="header">Row header 4</span>
              <span>Row content 4</span>
            </ap-timeline-row>
          </ap-timeline-group>
        </ap-scroll-area>
      </ap-timeline-basic>
    `
  }

  private initializeAccuratePlayer() {
    if (this.master == null) {
      console.warn("Failed to initialize Accurate Player: video element could not be found.");
      return;
    }
    if (window.LICENSE_KEY == null) {
      console.warn("Failed to initialize Accurate Player: no license key found, did you provide one in license-key.js?");
      return;
    }
    if (this.controls == null) {
      console.warn("Failed to initialize Accurate Player: could not find <apc-controls />");
      return;
    }
    this.player = new AccuratePlayer(this.master, window.LICENSE_KEY);
    this.hotkeyPlugin = new HotkeyPlugin(this.player);
    this.smoothTimeUpdatePlugin = new SmoothTimeUpdatePlugin(this.player);
    this.pointPlugin = new PointPlugin(this.player);
    this.player.api.loadVideoFile({
      label: "Big Buck Bunny",
      src:
        "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/original/Bunny_H264_Webproxy_TC2.mp4",
      enabled: true,
      channelCount: 2,
      frameRate: {numerator: 25, denominator: 1},
      dropFrame: false,
    });
    this.player.on(PlayerEventType.Loaded, async () => {
      this.markers = this.getRandomMarkerData();
      this.thumbnails = await this.getSpritemapThumbnails();
      this.waveformData = await this.fetchWaveformData();
    });
    // The Polymer 3.0 project is currently written in Javascript. Will be converted to TypeScript with typings shortly!
    this.controls.init(this.master, this.player, {
      saveLocalSettings: true // Enables storing of simple settings as volume, timeFormat and autoHide.
    });

  }

  private async fetchWaveformData(): Promise<WaveformData> {
    const response = await fetch('/waveformdata.dat');
    const buffer = await response.arrayBuffer();
    return WaveformData.create(buffer);
  }

  private getRandomMarkerData(): Partial<Marker>[] {
    const durationFrames = this.player?.api.getDuration(TimeFormat.Frame) as number;
    return [...Array(2000)].map(() => {
      const start = Math.floor(Math.random() * durationFrames);
      const end = start + Math.min(100, Math.floor(Math.random() * (durationFrames - start)));
      return {start, end};
    });
  }

  private getRandomThumbnails(): Partial<Marker>[] {
    const durationFrames = this.player?.api.getDuration(TimeFormat.Frame) as number;
    const frameRate = this.player?.api.getActiveFile()?.frameRate as FrameRate;
    const fps = frameRate.numerator / frameRate.denominator;
    const thumbnails = [];
    for (let i = 0; i < durationFrames; i += fps) {
      thumbnails.push({
        start: i,
        src: `https://picsum.photos/160/90?random=${i}`
      })
    }
    return thumbnails;
  }

  private async getSpritemapThumbnails(): Promise<Partial<Marker>[]> {
    const response = await fetch("/spritemap_manifest.json");
    const manifest = await response.json();
    const frameRate = this.player?.api.getActiveFile()?.frameRate as FrameRate;
    const fps = frameRate.numerator / frameRate.denominator;
    const thumbnails = [];
    for (const sprite of manifest.sprites) {
      thumbnails.push({
        // we convert the timestamp t (flicks) to the time format used by our video file
        start: (sprite.t / 705600000) * fps,
        src: `/big_buck_bunny_spritemap.jpg`,
        // srcDrawRect describes what portion of the spritemap to draw
        srcDrawRect: {
          x: sprite.x,
          y: sprite.y,
          width: manifest.width,
          height: manifest.height
        }
      })
    }
    return thumbnails;
  }

  private onThumbnailClick = (e: CustomEvent) => {
    const [marker] = e.detail.args;
    this.player?.api.seek({
      time: marker.start,
      format: TimeFormat.Frame,
      mode: SeekMode.Absolute
    });
  }

  private getSineWaveformData(start: number, end: number) {
    const durationFrames = this.player?.api.getDuration(TimeFormat.Frame) as number;
    const samplesPerFrame = 35;
    const totalNumberOfSamples = durationFrames * samplesPerFrame;
    const fakeSamples = [...Array(totalNumberOfSamples)].map((_, i) => Math.sin((i - 0.5) / 1000));
    return fakeSamples.slice(start * samplesPerFrame, end * samplesPerFrame);
  }

  private onWaveformDataRequest(e: CustomEvent): void {
    const waveformComponent = e.target as ApTimelineAudioWaveform;
    // The range is the portion of the video that the component needs data from, start and end timestamps (frames)
    // The width of the area that is going to be drawn (in pixels)
    // Ideally we supply data for the requested range with a resolution that satisfies width (i.e at least width number of samples)
    const [range, width] = e.detail.args;
    // We use the component api to supply the data:
    waveformComponent.api.addSampleRange({
      start: range.start,
      end: range.end,
      samples: this.getSineWaveformData(range.start, range.end)
    })
    // or we can instruct the component that we do not have data for a specific range as well (that way it won't request it again)
    // waveformComponent.api.addIgnoreRange({
    //   start: range.start,
    //   end: range.end
    // })
  }

  private async getWaveformSamples(
    start: number,
    end: number,
    width: number
  ): Promise<number[]> {
    if (!this.waveformData) {
      console.warn("Waveform data not loaded yet");
      return [];
    }
    const frameRate = this.player?.api.getActiveFile()?.frameRate as FrameRate;
    const fps = frameRate.numerator / frameRate.denominator;
    // Convert to seconds as WaveformData uses it
    const startSec = start / fps;
    const endSec = end / fps;
    // Calculate the "optimal" number of input samples we need per pixel (width)
    const scale = Math.max(
      Math.floor(((endSec - startSec) * this.waveformData.sample_rate) / width),
      128 // this is the minimum supported by our generated data file
    );
    // Resample the data to fit the "optimal" scale
    const resampled = this.waveformData.resample({
      scale,
    });
    const channelData = resampled.channel(0);
    // Get indices for the zoom portion
    const inputIndex = resampled.at_time(startSec);
    const outputIndex = resampled.at_time(endSec);
    // Interleave min and max values get the waveform to go both up and down
    const minValues = channelData.min_array().slice(inputIndex, outputIndex);
    const maxValues = channelData.max_array().slice(inputIndex, outputIndex);
    return minValues.flatMap((value: number, i: number) => [
      value,
      maxValues[i],
    ]);
  }

  private async onWaveformDataRequest2(e: CustomEvent): Promise<void> {
    const waveformComponent = e.target as ApTimelineAudioWaveform;
    const [{start, end}, width] = e.detail.args;
    const samples = await this.getWaveformSamples(start, end, width);
    waveformComponent.api.addSampleRange({
      start: start,
      end: end,
      samples,
      // numSamples tells the component how many data points this data package represents.
      // The waveform data from the "waveform-data" package returns min and max interleaved in the data: [min_1, max_1, min_2, max_2, ..., min_n, max_n]
      // this produces 2 * n number of values in the array even though they represent n number of samples.
      // numSamples defaults to samples.length (2 * n), but that would be incorrect in this case, so we set it to n instead by dividing with 2.
      numSamples: samples.length / 2
    })
  }
}

declare global {
  interface Window {
    LICENSE_KEY: string;
  }
}
