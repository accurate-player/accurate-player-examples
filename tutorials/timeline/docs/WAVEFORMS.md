# Waveforms

The waveform components can be used to draw waveforms in the timeline. The waveform component draws a waveform using the data you provide to it. First, we need to register 
the waveform component:

```ts
import { ApTimelineAudioWaveform } from "@accurate-player/accurate-player-components-external";
ApTimelineAudioWaveform.register(); // <ap-timeline-audio-waveform>
```

Next we include it in a row on our timeline:

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <ap-timeline-audio-waveform
      @apdatarequest="${this.onWaveformDataRequest}"
      .type="${AudioWaveformDrawType.STROKE}"
      sample-min="-1"
      sample-max="1">
    </ap-timeline-audio-waveform>
  </ap-timeline-row>
</ap-timeline-basic>
```

The waveform component uses a demand-driven API, it will tell us when it needs more data by dispatching a `apdatarequest` event. In response to that event, we provide data
to the component by calling `api.addSampleRange`. In the example above we register `this.onWaveformDataRequest` to be called when `apdatarequest` is dispatched. We also
set some rendering properties like `sampleMin` and `sampleMax` to describe the bounds of the data we will provide, as well as a `type` which determines the method that will
be used to draw the waveform. Let's try to draw a basic sine wave for starters:

```ts
getSineWaveformData(start: number, end: number): number[] {
  const durationFrames = this.player?.api.getDuration(TimeFormat.Frame) as number;
  const samplesPerFrame = 35;
  const totalNumberOfSamples = durationFrames * samplesPerFrame;
  const fakeSamples = [...Array(totalNumberOfSamples)].map((_, i) => Math.sin((i - 0.5) / 1000));
  return fakeSamples.slice(start * samplesPerFrame, end * samplesPerFrame);
}

onWaveformDataRequest(e: CustomEvent): void {
  const waveformComponent = e.target as ApTimelineAudioWaveform;
  // The range is the portion of the video that the component needs data from, start and end timestamps (frames)
  // The width of the area that is going to be drawn (in pixels)
  // Ideally we supply data for the requested range with a resolution that satisfies width (i.e at least width number of samples)
  const [range, width] = e.detail.args;
  // We use the component api to supply the data:
  waveformComponent.api.addSampleRange({
    start: range.start,
    end: range.end,
    samples: this.getSineWaveformData(range.start, range.end)
  })
  // or we can instruct the component that we do not have data for a specific range as well (that way it won't request it again)
  // waveformComponent.api.addIgnoreRange({
  //   start: range.start,
  //   end: range.end
  // })
}
```

`getSineWaveformData` generates sine wave samples for the duration of the video, 35 samples per frame. As we zoom in / out and move on the timeline the waveform
component will dispatch `apdatarequest` as soon as it determines that it does not have data for the portion of the video that the user navigated to. We call
`addSampleRange` with data for the requested portion, and the waveform component renders using the data we provided. Let's see how
this looks:

!["Waveform of a sine wave"](images/waveform_sine.gif "Waveform sine wave")

Nice one! This demonstrates how to use the waveform component API, but it is not very useful as it does not represent the audio in our video. Let's play with some real data
instead.

BBC has some [OSS waveform tools](https://waveform.prototyping.bbc.co.uk/) that we can use. We'll use the `audiowaveform` program to generate a waveform data file, along
with the `waveform-data.js` library to parse and make use of that file in our timeline application. You can of course use whatever tools you like for doing waveform data
extraction, this is just one example of that using the tools provided by BBC.

First, we generate the waveform data file using `ffmpeg` and `audiowaveform`:

```bash
# audiowaveform does not support mp4 input so we convert it to wav
ffmpeg -i Bunny_H264_Webproxy_TC2.mp4 Bunny_H264_Webproxy_TC2.wav
# 8-bit resolution, 128 input samples per data point
audiowaveform -i Bunny_H264_Webproxy_TC2.wav -z 128 -b 8 -o waveformdata.dat
```

Then we load the generated `waveformdata.dat` using the `waveform-data.js` library in our application:

```ts
import WaveformData from "waveform-data";

async fetchWaveformData(): Promise<WaveformData> {
  const response = await fetch('/waveformdata.dat');
  const buffer = await response.arrayBuffer();
  return WaveformData.create(buffer);
}

this.waveformData = await this.fetchWaveformData();
```

Now that we have loaded the file we can update our `apdatarequest` event handler to use this data instead:

```ts
async getWaveformSamples(
  start: number,
  end: number,
  width: number
): Promise<number[]> {
  if (!this.waveformData) {
    console.warn("Waveform data not loaded yet");
    return [];
  }
  const frameRate = this.player?.api.getActiveFile()?.frameRate as FrameRate;
  const fps = frameRate.numerator / frameRate.denominator;
  // Convert to seconds as WaveformData uses it
  const startSec = start / fps;
  const endSec = end / fps;
  // Calculate the "optimal" number of input samples we need per pixel (width)
  const scale = Math.max(
    Math.floor(((endSec - startSec) * this.waveformData.sample_rate) / width),
    128 // this is the minimum supported by our generated data file
  );
  // Resample the data to fit the "optimal" scale
  const resampled = this.waveformData.resample({
    scale,
  });
  const channelData = resampled.channel(0);
  // Get indices for the zoom portion
  const inputIndex = resampled.at_time(startSec);
  const outputIndex = resampled.at_time(endSec);
  // Interleave min and max values, we wan't both to draw a waveform that goes in both directions
  const minValues = channelData.min_array().slice(inputIndex, outputIndex);
  const maxValues = channelData.max_array().slice(inputIndex, outputIndex);
  return minValues.flatMap((value: number, i: number) => [
    value,
    maxValues[i],
  ]);
}

async onWaveformDataRequest(e: CustomEvent): Promise<void> {
  const waveformComponent = e.target as ApTimelineAudioWaveform;
  const [{start, end}, width] = e.detail.args;
  const samples = await this.getWaveformSamples(start, end, width);
  waveformComponent.api.addSampleRange({
    start: start,
    end: end,
    samples,
    // numSamples specifies how many data points this data package represents.
    // getWaveformSamples returns min and max interleaved in the data: [min_1, max_1, min_2, max_2, ..., min_n, max_n]
    // this produces 2 * n number of values in the array even though they represent n number of samples.
    // numSamples defaults to samples.length (2 * n), but that would be incorrect in this case, so we set it to n instead by dividing with 2.
    numSamples: samples.length / 2
  })
}
```

We also update some rendering properties to match the data we extracted:

```html
<ap-timeline-audio-waveform
  @apdatarequest="${this.onWaveformDataRequest2}"
  .dataRequestDebounceTime="${0}"
  .type="${AudioWaveformDrawType.STROKE}"
  .waveformStyle="${{
    stroke: "--AP-PRIMARY",
  }}"
  .sampleMin=${-127}
  .sampleMax=${127}>
</ap-timeline-audio-waveform>
```

We get 8-bit data, so we update `sampleMin` and `sampleMax` to match that, we also set the `dataRequestDebounceTime` to `0` since we read the file into memory, the debounce
is useful if we need to make network requests to fetch more data, but not in this case as we already have the data in memory. We also change the stroke color to the
primary AP color using `waveformStyle` to make the waveform a bit more pleasing. Okay, let's see what we got:

!["Final waveform example"](images/waveform_real.gif "Waveform example")

We got an accurate waveform representation of the sound in our video! As you can see from these examples, the waveform component handles the rendering of waveforms, providing
data is up to you. 

← [Thumbnails](THUMBNAILS.md) | [Conclusion](../README.md#conclusion) →
