# Layout composition

There are a few components that are used to compose the layout of our timeline content, we will cover those in this section. Content is placed in rows on the
timeline, and rows, in turn, can be placed into groups. Groups are collapsible rows that can help you categorize your content. Further, we can also make parts of the timeline
scrollable by using a scroll area that covers a collection of rows and/or groups. Let's start by importing and registering the components:

```ts
import { ApTimelineRow, ApTimelineGroup, ApScrollArea } from "@accurate-player/accurate-player-components-external";

ApTimelineRow.register();   // <ap-timeline-row />
ApTimelineGroup.register(); // <ap-timeline-group />
ApScrollArea.register();    // <ap-scroll-area />
```

Now let's add a few rows:

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <span>Row content 1</span>
  </ap-timeline-row>
  <ap-timeline-row>
    <span slot="header">Row header 2</span>
    <span>Row content 2</span>
  </ap-timeline-row>
  <ap-timeline-row>
    <span slot="header">Row header 3</span>
    <span>Row content 3</span>
  </ap-timeline-row>
</ap-timeline-basic>
```

![Layout with 2 rows](images/3_rows.jpg "3 Rows")

The children of the row are placed in the content area of the row; `"Row content 1"` etc. Elements that you put in the `header` slot (by setting the `slot` attribute to 
`header`) are placed on the right side of the content area. As you might have noticed the headers are not centered, you'll have to apply that styling yourself.

Now let's add a few more rows and place some of them in a collapsible group:

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <span>Row content 1</span>
  </ap-timeline-row>
  <ap-timeline-group @apclick="${(e: Event & { target: ApTimelineGroup }) => {
    e.target.collapsed = !e.target.collapsed
  }}">
    <span slot="header">Group header 1</span>
    <ap-timeline-row>
      <span slot="header">Row header 2</span>
      <span>Row content 2</span>
    </ap-timeline-row>
    <ap-timeline-row>
      <span slot="header">Row header 3</span>
      <span>Row content 3</span>
    </ap-timeline-row>
    <ap-timeline-row>
      <span slot="header">Row header 4</span>
      <span>Row content 4</span>
    </ap-timeline-row>
  </ap-timeline-group>
  <ap-timeline-row>
    <span slot="header">Row header 5</span>
    <span>Row content 5</span>
  </ap-timeline-row>
</ap-timeline-basic>
```

Groups also have a `header` slot just like rows. They also have a `content` slot available if you want to put elements in the content area of the group. The default slot 
(when you don't provide a `slot` attribute) is reserved for rows.

In this example, we've also registered an event handler for the `apclick` event. Clicking on the group will trigger the event, and we toggle the collapsed state to open/close 
the group:

!["Example of a collapsing group"](images/group_collapse.gif "Collapsible group")

This is useful if you want to categorize your content and for example let the user show what they care about. If we have many groups and rows we probably 
want to make
parts of our groups and rows scrollable to avoid the timeline taking up all our screen space. The scroll area component is useful in this case. Let's wrap all but the
first row in a scroll area with a `max-height` of `120px` and see what effect we get from that:

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <span>Row content 1</span>
  </ap-timeline-row>
  <ap-scroll-area style="max-height: 120px;">
    <ap-timeline-group @apclick="${(e: Event & { target: ApTimelineGroup }) => {
      e.target.collapsed = !e.target.collapsed
    }}">
      <span slot="header">Group header 1</span>
      <ap-timeline-row>
        <span slot="header">Row header 2</span>
        <span>Row content 2</span>
      </ap-timeline-row>
      <ap-timeline-row>
        <span slot="header">Row header 3</span>
        <span>Row content 3</span>
      </ap-timeline-row>
      <ap-timeline-row>
        <span slot="header">Row header 4</span>
        <span>Row content 4</span>
      </ap-timeline-row>
    </ap-timeline-group>
    <ap-timeline-row>
      <span slot="header">Row header 5</span>
      <span>Row content 5</span>
    </ap-timeline-row>
  </ap-scroll-area>
</ap-timeline-basic>
```

!["Scroll area example"](images/scroll_area.gif "Scroll area")

As you can see the first row remains static and the rest of the groups and rows in the scroll area are now scrollable.

This covers the basics of timeline layout composition, next up we'll look into adding some markers to our rows.

← [Basics](BASICS.md) | [Markers](MARKERS.md) →
