# Thumbnails

You can add thumbnails to your timeline to improve the user experience by giving the user the ability to get a fast visual overview of the video content. It makes it
easier to find interesting sections of the video and may speed up the workflow. You can use the thumbnail components to attach any image to a timestamp, so you may also
find other uses for it than thumbnails, we'll leave that to your imagination.

The thumbnail components are similar to marker components, we have a container component, and a thumbnail component we need to register:

```ts
import { ApTimelineThumbnails, ApTimelineThumbnail } from "@accurate-player/accurate-player-components-external";

ApTimelineThumbnails.register(); // <ap-timeline-thumbnails />
ApTimelineThumbnail.register(); // <ap-timeline-thumbnail />
```

To add thumbnails to a row we add them to the row content slot:

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <ap-timeline-thumbnails height="50">
      <ap-timeline-thumbnail start="0" src="https://picsum.photos/160/90?random=1"></ap-timeline-thumbnail>
      <ap-timeline-thumbnail start="24" src="https://picsum.photos/160/90?random=2"></ap-timeline-thumbnail>
      <ap-timeline-thumbnail start="48" src="https://picsum.photos/160/90?random=3"></ap-timeline-thumbnail>
    </ap-timeline-thumbnails>
  </ap-timeline-row>
</ap-timeline-basic>
```

Here we added 3 thumbnails on frames 0, 24, and 48 respectively. Zooming in a bit on the timeline reveals the thumbnails on those positions:

!["Example of 3 thumbnails"](images/3_thumbnails.jpg "3 Thumbnails")

Let's add more! Let's fill up the whole duration of our video with some random images. We are using [picsum.photos](https://picsum.photos/) here to get random images.
Let's create one thumbnail per second:

```ts
getRandomThumbnails(): Partial<Marker>[] {
  const durationFrames = this.player?.api.getDuration(TimeFormat.Frame) as number;
  const frameRate = this.player?.api.getActiveFile()?.frameRate as FrameRate;
  const fps = frameRate.numerator / frameRate.denominator;
  const thumbnails = [];
  for (let i = 0; i < durationFrames; i += fps) {
    thumbnails.push({
      start: i,
      src: `https://picsum.photos/160/90?random=${i}`
    })
  }
  return thumbnails;
}
this.thumbnails = this.getRandomThumbnails();
```

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <ap-timeline-thumbnails height="50">
      <ap-timeline-thumbnail .data="${this.thumbnails}"></ap-timeline-thumbnail>
    </ap-timeline-thumbnails>
  </ap-timeline-row>
</ap-timeline-basic>
```

We use the `data` property here to provide all data all at once instead of using multiple thumbnail elements. We should have thumbnails all over our timeline now, let's see:

!["Thumbnail navigation"](images/thumbnail_nav.gif "Thumbnail navigation")

More thumbnails are revealed when we zoom further in always filling up the available space. Thumbnails also show when we hover the timestamps (as can be
seen at the end) this helps users to find where they want to navigate more quickly.

However, if we inspect the network tab in our developer tools, we can see that we do a _lot_ of network requests to fetch all the images. Even though the timeline only
fetches (and caches) the images just in time before they are displayed, we still get a lot of calls:

!["Developer console showing a lot of network calls"](images/lots_of_network_calls.jpg "Lots of network calls")

A good practice to avoid this is to put multiple images in one image, we call this a _sprite map_. Here's an example of how such an image might look like:

!["Thumbnail sprite map example"](images/big_buck_bunny_spritemap.jpg "Thumbnail sprite map")

Here's an accompanying manifest file that describes the content of the sprite map image:

<details>
<summary><strong>Click this show sprite map manifest contents</strong></summary>

```json
{
  "width": 240,
  "height": 135,
  "sprites": [
    {
      "x": 0,
      "y": 0,
      "t": 4036032000
    },
    {
      "x": 240,
      "y": 0,
      "t": 12164544000
    },
    {
      "x": 480,
      "y": 0,
      "t": 20293056000
    },
    {
      "x": 720,
      "y": 0,
      "t": 28421568000
    },
    {
      "x": 960,
      "y": 0,
      "t": 36550080000
    },
    {
      "x": 1200,
      "y": 0,
      "t": 44678592000
    },
    {
      "x": 1440,
      "y": 0,
      "t": 52807104000
    },
    {
      "x": 0,
      "y": 135,
      "t": 60935616000
    },
    {
      "x": 240,
      "y": 135,
      "t": 69064128000
    },
    {
      "x": 480,
      "y": 135,
      "t": 77192640000
    },
    {
      "x": 720,
      "y": 135,
      "t": 85321152000
    },
    {
      "x": 960,
      "y": 135,
      "t": 93449664000
    },
    {
      "x": 1200,
      "y": 135,
      "t": 101578176000
    },
    {
      "x": 1440,
      "y": 135,
      "t": 109706688000
    },
    {
      "x": 0,
      "y": 270,
      "t": 117835200000
    },
    {
      "x": 240,
      "y": 270,
      "t": 125963712000
    },
    {
      "x": 480,
      "y": 270,
      "t": 134092224000
    },
    {
      "x": 720,
      "y": 270,
      "t": 142220736000
    },
    {
      "x": 960,
      "y": 270,
      "t": 150349248000
    },
    {
      "x": 1200,
      "y": 270,
      "t": 158477760000
    },
    {
      "x": 1440,
      "y": 270,
      "t": 166606272000
    },
    {
      "x": 0,
      "y": 405,
      "t": 174734784000
    },
    {
      "x": 240,
      "y": 405,
      "t": 182863296000
    },
    {
      "x": 480,
      "y": 405,
      "t": 190991808000
    },
    {
      "x": 720,
      "y": 405,
      "t": 199120320000
    },
    {
      "x": 960,
      "y": 405,
      "t": 207248832000
    },
    {
      "x": 1200,
      "y": 405,
      "t": 215377344000
    },
    {
      "x": 1440,
      "y": 405,
      "t": 223505856000
    },
    {
      "x": 0,
      "y": 540,
      "t": 231634368000
    },
    {
      "x": 240,
      "y": 540,
      "t": 239762880000
    },
    {
      "x": 480,
      "y": 540,
      "t": 247891392000
    },
    {
      "x": 720,
      "y": 540,
      "t": 256019904000
    },
    {
      "x": 960,
      "y": 540,
      "t": 264148416000
    },
    {
      "x": 1200,
      "y": 540,
      "t": 272276928000
    },
    {
      "x": 1440,
      "y": 540,
      "t": 280405440000
    },
    {
      "x": 0,
      "y": 675,
      "t": 288533952000
    },
    {
      "x": 240,
      "y": 675,
      "t": 296662464000
    },
    {
      "x": 480,
      "y": 675,
      "t": 304790976000
    },
    {
      "x": 720,
      "y": 675,
      "t": 312919488000
    },
    {
      "x": 960,
      "y": 675,
      "t": 321048000000
    },
    {
      "x": 1200,
      "y": 675,
      "t": 329176512000
    },
    {
      "x": 1440,
      "y": 675,
      "t": 337305024000
    },
    {
      "x": 0,
      "y": 810,
      "t": 345433536000
    },
    {
      "x": 240,
      "y": 810,
      "t": 353562048000
    },
    {
      "x": 480,
      "y": 810,
      "t": 361690560000
    },
    {
      "x": 720,
      "y": 810,
      "t": 369819072000
    },
    {
      "x": 960,
      "y": 810,
      "t": 377947584000
    },
    {
      "x": 1200,
      "y": 810,
      "t": 386076096000
    },
    {
      "x": 1440,
      "y": 810,
      "t": 394204608000
    },
    {
      "x": 0,
      "y": 945,
      "t": 402333120000
    }
  ]
}
```

</details>

The thumbnail components support rendering of a portion of an image, so we can leverage this to avoid having to fetch multiple images and just fetch the sprite map once.

The manifest describes the `width` and `height` of the sprites and each sprite describes the coordinates where the sprite is located in the image (`x` and `y`). `t`
describes the video timestamp (in [flicks](https://github.com/facebookarchive/Flicks)) where the thumbnail was taken from. We can use this together with
the
`srcDrawRect` property of the thumbnail marker which can be
used to
specify what portion of the image to draw:

```ts
async getSpritemapThumbnails(): Promise<Partial<Marker>[]> {
  const response = await fetch("/spritemap_manifest.json");
  const manifest = await response.json();
  const frameRate = this.player?.api.getActiveFile()?.frameRate as FrameRate;
  const fps = frameRate.numerator / frameRate.denominator;
  const thumbnails = [];
  for (const sprite of manifest.sprites) {
    thumbnails.push({
      // we convert the timestamp t (flicks) to the time format used by our video file
      start: (sprite.t / 705600000) * fps,
      src: `/big_buck_bunny_spritemap.jpg`,
      // srcDrawRect describes what portion of the spritemap to draw
      srcDrawRect: {
        x: sprite.x,
        y: sprite.y,
        width: manifest.width,
        height: manifest.height
      }
    })
  }
  return thumbnails;
}
this.thumbnails = await getSpritemapThumbnails();
```

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <ap-timeline-thumbnails height="50">
      <ap-timeline-thumbnail .data="${this.thumbnails}"></ap-timeline-thumbnail>
    </ap-timeline-thumbnails>
  </ap-timeline-row>
</ap-timeline-basic>
```

Now we have thumbnails from the video we are looking at as a bonus, much more useful:

!["Timeline with thumbnails from the sprite map"](images/thumbnail_spritemap.gif "Sprite map thumbnails")

We can also hook up some event listeners to the thumbnail container, this can be used to trigger a seek if the user clicks on a thumbnail:

```ts
function onThumbnailClick(e: CustomEvent): void {
  const [marker] = e.detail.args;
  this.player?.api.seek({
    time: marker.start,
    format: TimeFormat.Frame,
    mode: SeekMode.Absolute
  });
}
```

```html
<ap-timeline-thumbnails @apmarkerclick="${this.onThumbnailClick}">
  <ap-timeline-thumbnail .data="${this.thumbnails}"></ap-timeline-thumbnail>
</ap-timeline-thumbnails>
```

!["Thumbnail click handler example"](images/thumbnails_click.gif "Thumbnail click handler example")

More options and information about the thumbnail components can be found in our documentation. Next up we'll add waveforms to our timeline to visualize the sound in our video!

← [Markers](MARKERS.md) | [Waveforms](WAVEFORMS.md) →
