# Markers

Markers can be used to display time-based metadata that is relevant to your content. A timeline marker is essentially a box that marks the start and end of that metadata.
This can be used to show auto QC results, subtitles, review annotations, and much more.

To add markers to our rows we need to register the metadata, and the marker component:

```ts
import { ApTimelineMetadata, ApTimelineMarker } from "@accurate-player/accurate-player-components-external";

ApTimelineMetadata.register();   // <ap-timeline-metadata />
ApTimelineMarker.register(); // <ap-timeline-marker />
```

The metadata component is the container we put all our markers in, it handles layout and rendering. The marker component is used to provide the marker data to the 
metadata container. We use it as follows:

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <ap-timeline-metadata .markerStyle="${{backgroundColor: "var(--AP-WARNING)"}}">
    <ap-timeline-marker start="0" end="9000"></ap-timeline-marker>
    <ap-timeline-marker .data="${[
              {start: 0, end: 1000},
              {start: 1200, end: 1600},
              {start: 8000, end: 9100}
            ]}"></ap-timeline-marker>
    <ap-timeline-marker .data="${[
              {start: 2000, end: 2100},
              {start: 4000, end: 6000}
            ]}"></ap-timeline-marker>
    </ap-timeline-metadata>
  </ap-timeline-row>
</ap-timeline-basic>
```

!["Basic marker example"](images/markers_basic.jpg "Basic markers")

As you can see in the example you can either add one marker component per marker, or you can provide marker data for multiple markers at the same time using the `data`
property. If you have many markers the `data` property is the recommended approach to avoid creating a lot of DOM nodes. We also change the styles of the markers by
providing a `markerStyle` to the metadata container to color them yellow. Let's see how this looks:

Collisions between markers are handled by default by splitting them into non colliding groups. We can collapse the markers into one row by setting `collapsed`
on the metadata container. If we do that opacity is used to indicate collisions instead of dividing the markers into different rows:

!["Collapsed markers"](images/collapsed_markers.jpg "Collapsed markers")

More layout and styling options can be found in our documentation. Let's add another row with some more markers:

```ts
import { TimeFormat } from "@accurate-player/accurate-player-core";

getRandomMarkerData(): Partial<Marker>[] {
  const durationFrames = this.player?.api.getDuration(TimeFormat.Frame) as number;
  return [...Array(2000)].map(() => {
    const start = Math.floor(Math.random() * durationFrames);
    const end = start + Math.min(100, Math.floor(Math.random() * (durationFrames - start)));
    return {start, end};
  });
}
this.markers = this.getRandomMarkerData();
```

```html
<ap-timeline-basic .player="${this.player}" flex-height>
  <ap-timeline-row>
    <span slot="header">Row header 1</span>
    <ap-timeline-metadata .rendererType="${RendererType.WEBGL}">
      <ap-timeline-marker .data="${this.markers}"></ap-timeline-marker>
    </ap-timeline-metadata>
  </ap-timeline-row>
</ap-timeline-basic>
```

We create 2000 markers with random positions on the timeline, and we provide that data via the `data` property. We also change the `rendererType` to WebGL, this will 
improve
the rendering time now that we have this huge amount of data. You can see below that the timeline performs well even with a lot of data moving around. Zooming in and
out and moving on the timeline is still smooth:

!["Markers example with a lot of data"](images/timeline_smooth.gif "Smooth rendering")

We can also attach event listeners on the metadata container to get notified when the user interact with the markers:

```ts
function onMarkerClick(e: CustomEvent) {
  const [marker, api, mouseEvent] = e.detail.args;
  // The marker the user clicked on
  console.log(marker);
  // The marker api can be used to fetch the next and previous marker for example
  console.log(api.nextMarker());
  // The mouse event that triggered the click
  console.log(mouseEvent.pageX, mouseEvent.pageY);
}
```

```html
<ap-timeline-metadata @apmarkerclick="${onMarkerClick}">
```

There are also events for mouse enter, mouse leave, selection changes, and so on. More information about this and more configuration options is available in the
documentation. Next up we'll look into adding thumbnails to the timeline.

← [Layout](LAYOUT.md) | [Thumbnails](THUMBNAILS.md) →
