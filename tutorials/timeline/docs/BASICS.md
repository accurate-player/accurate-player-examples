# Basics

First off we need to install the accurate player components library to get access to the components.

```bash
pnpm run add @accurate-player/accurate-player-components-external --dev
```

Next, we need to import the components that we are going to use

```typescript
import { ApTimelineBasic } from "@accurate-player/accurate-player-components-external";
```

We also need to include a css theme file, there are two predefined themes that can be imported from `dist/styles/theme-dark.css` or `dist/styles/theme-light.css`
respectively. You can of course also include your theme colors by copying one of the included theme files and alter the color variables to your liking. If you use a bundler like webpack or similar you
can probably do something like this to import it from `node_modules`:

```typescript
import "~@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
```

Before we can include any of the components we need to register them with the [CustomElementsRegistry](https://developer.mozilla.org/en-US/docs/Web/API/CustomElementRegistry),
you need to do this once for each component that you use:

```typescript
ApTimelineBasic.register(); // <ap-timeline-basic />
```

This will make the component registered for the tag `<ap-timeline-basic />`. Component tag names are set to the class name converted to lowercase kebab case per default. Now
we can use this tag in our html. We need to provide a player reference to the component, and then we are good to go:

```html
<ap-timeline-basic .player="${player}" flex-height></ap-timeline-basic>
```

Let's see what it looks like!

![Basic timeline](images/timeline_basic.gif "Basic timeline")

It's now possible to search in the video by dragging the current time indicator. In the top left corner, there's a time input that can be used to seek a specific time code
or frame. There's also a zoom control at the bottom of the timeline that you can use to zoom in, zoom out and navigate on the timeline with. When zoomed in all the way
down to frame level, you'll also see a frame indicator that shows the duration of the current frame. Next up we're going to look at how we can structure content on the timeline.

← [Parts](../README.md#parts) | [Layout](LAYOUT.md) →
