import { css, html, LitElement, PropertyValues, TemplateResult, } from "lit";
import { customElement, property, query } from "lit/decorators.js"
import { ProgressivePlayer as AccuratePlayer } from "@accurate-player/accurate-player-progressive";

import { ApSelectionTool, SelectionToolApi, } from "@accurate-player/accurate-player-components-external";

import { HotkeyPlugin } from "@accurate-player/accurate-player-plugins";

import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
import { PlayerEventType, SeekMode, TimeFormat, } from "@accurate-player/accurate-player-core";

/*
 * Component library components need to be registered with the customElements registry
 * before they can be used (as they are WebComponents, read more: https://developer.mozilla.org/en-US/docs/Web/Web_Components).
 *
 * Calling register() will call customElements.define() under the hood.
 *
 */
ApSelectionTool.register();

interface SelectionToolInitEvent {
  absoluteRect: ClientRect;
  relativeRect: ClientRect;
}

interface SelectionToolEvent extends SelectionToolInitEvent {
  dirty: boolean;
}

interface OffsetRect {
  top: number;
  right: number;
  bottom: number;
  left: number;
}

interface SnapOptions {
  instant?: boolean;
  snapToCenter?: boolean;
  snapToSides?: boolean;
  tolerance?: number;
}

interface GuidelineLayout {
  predefinedLayout?: "diagonal" | "triangle";
  horizontalPattern?: number | number[];
  verticalPattern?: number | number[];
}

interface AnimationOptions {
  enabled: boolean;
  duration: number;
  easingFunction: (x: number) => number;
  triggerEvent: boolean;
}

interface Snapshot {
  frame: number;
  url: string;
  area: OffsetRect;
}

/**
 * Application root element.
 */
@customElement("av-application")
export class Application extends LitElement {
  static styles = css`
    :host {
      background: var(--AP-BACKGROUND-BODY);
      display: flex;
      padding: 20px;
      flex-direction: column;
      width: calc(100% - 40px);
      height: calc(100% - 40px);
    }

    div.container-video {
      display: flex;
      flex: 1;
      justify-content: center;
      margin-bottom: 20px;
      max-height: 50vh;
      position: relative;
    }

    div.flex-column {
      display: flex;
      flex: 1 1 100%;
      flex-direction: column;
    }

    div.grid {
      display: grid;
      grid-template-columns: 3fr 1fr;
      height: 100%;
    }

    div.snapshots {
      padding: 16px;
      overflow-y: scroll;
    }

    img.preview {
      border: 1px solid white;
      margin-bottom: 8px;
      cursor: pointer;
    }

    label {
      color: white;
    }
  `;

  @query("#player")
  videoElement: HTMLVideoElement | undefined;

  @property()
  private player: AccuratePlayer | undefined;

  private hotkeyPlugin: HotkeyPlugin;

  @property()
  private isPlaying = false;

  @property()
  private snapshots: Snapshot[] = [];

  @property()
  private forceAspectRatio = false;

  @property()
  private guidelineStyle = "default";

  private selection: OffsetRect;
  private videoAspectRatio: number;
  private pickedAspectRatio: string;
  @property()
  private highlight = false;
  @property()
  private showBounds = true;
  @property()
  private overlaySource = "";// "docs/media/overlay.png"
  private readonly snapOptionsDefault: SnapOptions = {
    instant: true,
    snapToSides: true,
    snapToCenter: true,
    tolerance: 10,
  };
  @property()
  private snapOptions?: SnapOptions = this.snapOptionsDefault;

  private guidelineLayout: GuidelineLayout = {
    //predefinedLayout: "triangle",
  };

  private animationOptions: AnimationOptions = {
    enabled: true,
    duration: 250,
    triggerEvent: false,
  }

  get api(): SelectionToolApi | undefined {
    return this.renderRoot?.querySelector<ApSelectionTool>(
      ApSelectionTool.elementName
    )?.api;
  }

  protected firstUpdated(changedProperties: PropertyValues) {
    super.firstUpdated(changedProperties);
    this.initializeAccuratePlayer();
  }

  async initializeAccuratePlayer() {
    if (this.videoElement == null) {
      console.warn(
        "Failed to initialize Accurate Player: video element could not be found."
      );

      return;
    }
    if (window.LICENSE_KEY == null) {
      console.warn(
        "Failed to initialize Accurate Player: no license key found, did you provide one in license-key.js?"
      );

      return;
    }

    this.player = new AccuratePlayer(this.videoElement, window.LICENSE_KEY);
    this.hotkeyPlugin = new HotkeyPlugin(this.player);

    this.player.api.loadVideoFile({
      label: "Big Buck Bunny",
      src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/original/Bunny_H264_Webproxy_TC2.mp4",
      enabled: true,
      channelCount: 2,
      frameRate: {numerator: 25, denominator: 1},
      dropFrame: false,
    });

    this.player.on(PlayerEventType.Loaded, () => {
      console.log("Video file loaded");
      this.api?.resetSelectionArea();
      this.videoAspectRatio =
        Number(this.videoElement?.videoHeight) /
        Number(this.videoElement?.videoWidth);
    });

    const settings = {
      saveLocalSettings: true, // Enables storing of simple settings as volume, timeFormat and autoHide.
    };
  }

  captureImage(): void {
    this.snapshots = [
      ...this.snapshots,
      {
        frame: this.player?.api.getCurrentTime(TimeFormat.Frame) as number,
        url: this.api?.extractImageUrl("jpeg") ?? "",
        area: this.selection,
      },
    ];

    requestAnimationFrame(() => {
      // this.scrollArea?.api.scrollToBottom();
    });
  }

  toggleSnapping(): void {
    if (this.snapOptions !== undefined) {
      this.snapOptions = undefined;
    }
    else {
      this.snapOptions = this.snapOptionsDefault;
    }
  }

  changeSnapshot(snapshot: Snapshot): void {
    this.player?.api.pause().then(() => {
      requestAnimationFrame(() => {
        this.player?.api
          .seek({
            time: snapshot.frame,
            format: TimeFormat.Frame,
            mode: SeekMode.Absolute,
          })
          .then(() => {
            this.api?.selectArea(snapshot.area);
            this.isPlaying = !this.player?.api.getStatus().paused;
          });
      });
    });
  }

  onSelectonToolInitialized = (event: SelectionToolEvent): void => {
    this.selection = event.relativeRect;
  };

  onSelectionToolChanged = (event: SelectionToolEvent): void => {
    this.selection = event.relativeRect;
  };

  onSnapshotClicked = (): void => {
    if (this.player?.api.getStatus().paused) {
      this.captureImage();
    }
    else {
      this.player?.api.pause().then(() => {
        this.captureImage();
        this.player?.api.play();
      });
    }
  };

  onPlayPauseClicked = (): void => {
    this.player?.api.togglePlay().then(() => {
      this.isPlaying = !this.player?.api.getStatus().paused;
      console.log(this.isPlaying);
    });
  };

  render(): TemplateResult {
    return html`
      <div class="grid">
        <div class="flex-column">
          <div class="container-video">
            <ap-selection-tool
              .aspectRatio="${this.pickedAspectRatio ?? this.videoAspectRatio}"
              .highlightSides="${this.highlight}"
              .highlightAxes="${this.highlight}"
              .forceAspectRatio="${this.forceAspectRatio}"
              .overlayImageSource="${this.overlaySource}"
              .guidelineLayout="${this.guidelineLayout}"
              .guidelineStyle="${this.guidelineStyle}"
              .snapOptions="${this.snapOptions}"
              .animationOptions="${this.animationOptions}"
              .showEndpoints=${true}
              .showBounds=${this.showBounds}
              .apInit="${this.onSelectonToolInitialized}"
              .apChange="${this.onSelectionToolChanged}"
            >
              <video id="player" crossorigin="anonymous"></video>
            </ap-selection-tool>
          </div>
          <div>
            <button style="width: 60px" @click="${this.onPlayPauseClicked}">
              ${this.isPlaying ? "❚❚" : "▶"}
            </button>
            <button @click="${this.onSnapshotClicked}">Snapshot</button>
            <label>
              <input
                type="checkbox"
                .checked=${this.forceAspectRatio}
                @change="${() =>
                  (this.forceAspectRatio = !this.forceAspectRatio)}"
              />
              Force aspect ratio
            </label>
            <label>
              <input
                type="checkbox"
                .checked=${!!this.snapOptions}
                @change="${() => this.toggleSnapping()}"
              />
              Enable snapping
            </label>
            <label>
              <input
                type="checkbox"
                .checked=${this.highlight}
                @change="${() => (this.highlight = !this.highlight)}"
              />Higlight edges/axes
            </label>
          </div>
        </div>
        <div class="flex-column snapshots">
          ${this.snapshots.map(
            (snapshot) => html`<img
              class="preview"
              src="${snapshot.url}"
              @click="${() => this.changeSnapshot(snapshot)}"
            />`
          )}
        </div>
      </div>
    `;
  }
}

declare global {
  interface Window {
    LICENSE_KEY: string;
  }
}
