import { css, html, LitElement, PropertyValues, TemplateResult, } from "lit";
import { customElement, property } from "lit/decorators.js"

import { ApSelectionTool, SelectionToolApi, } from "@accurate-player/accurate-player-components-external";

import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";

/*
 * Component library components need to be registered with the customElements registry
 * before they can be used (as they are WebComponents, read more: https://developer.mozilla.org/en-US/docs/Web/Web_Components).
 *
 * Calling register() will call customElements.define() under the hood.
 *
 */
ApSelectionTool.register();

interface SelectionToolInitEvent {
  absoluteRect: ClientRect;
  relativeRect: ClientRect;
}

interface SelectionToolEvent extends SelectionToolInitEvent {
  dirty: boolean;
}

interface OffsetRect {
  top: number;
  right: number;
  bottom: number;
  left: number;
}

interface SnapOptions {
  instant?: boolean;
  snapToCenter?: boolean;
  snapToSides?: boolean;
  tolerance?: number;
}

interface GuidelineLayout {
  predefinedLayout?: "diagonal" | "triangle";
  horizontalPattern?: number | number[];
  verticalPattern?: number | number[];
}

interface AnimationOptions {
  enabled: boolean;
  duration: number;
  easingFunction: (x: number) => number;
  triggerEvent: boolean;
}

interface Snapshot {
  url: string;
  area: OffsetRect;
}

/**
 * Application root element.
 */
@customElement("av-application")
export class Application extends LitElement {
  static styles = css`
    :host {
      background: var(--AP-BACKGROUND-BODY);
      display: flex;
      padding: 20px;
      flex-direction: column;
      width: calc(100% - 40px);
      height: calc(100% - 40px);
    }

    div.container-video {
      display: flex;
      flex: 1;
      justify-content: center;
      margin-bottom: 20px;
      max-height: 50vh;
      position: relative;
    }

    div.flex-column {
      display: flex;
      flex: 1 1 100%;
      flex-direction: column;
    }

    div.grid {
      display: grid;
      grid-template-columns: 3fr 1fr;
      height: 100%;
    }

    div.snapshots {
      padding: 16px;
      overflow-y: scroll;
    }

    img.preview {
      border: 1px solid white;
      margin-bottom: 8px;
      cursor: pointer;
    }

    label {
      color: white;
    }
  `;


  @property()
  private snapshots: Snapshot[] = [];

  @property()
  private forceAspectRatio = false;

  @property()
  private guidelineStyle = "default";

  @property()
  private highlight = false;
  @property()
  private showBounds = false;
  @property()
  private overlaySource = "";// "docs/media/overlay.png"
  private readonly snapOptionsDefault: SnapOptions = {
    instant: true,
    snapToSides: true,
    snapToCenter: true,
    tolerance: 10,
  };
  @property()
  private snapOptions?: SnapOptions = this.snapOptionsDefault;
  @property()
  private guidelineLayout: GuidelineLayout = {
    //predefinedLayout: "triangle",
  };

  @property()
  private animationOptions: AnimationOptions = {
    enabled: true,
    duration: 250,
    triggerEvent: false,
  }

  private selection: OffsetRect;
  private videoAspectRatio: number;
  private pickedAspectRatio: string;


  get api(): SelectionToolApi | undefined {
    return this.renderRoot?.querySelector<ApSelectionTool>(
      ApSelectionTool.elementName
    )?.api;
  }

  protected firstUpdated(changedProperties: PropertyValues) {
    super.firstUpdated(changedProperties);

  }

  captureImage(): void {
    this.snapshots = [
      ...this.snapshots,
      {
        url: this.api?.extractImageUrl("jpeg") ?? "",
        area: this.selection,
      },
    ];
  }

  toggleSnapping(): void {
    if (this.snapOptions !== undefined) {
      this.snapOptions = undefined;
    }
    else {
      this.snapOptions = this.snapOptionsDefault;
    }
  }

  changeSnapshot(snapshot: Snapshot): void {
    requestAnimationFrame(() => {
      this.api?.selectArea(snapshot.area);
    });
  }

  onSelectonToolInitialized = (event: SelectionToolEvent): void => {
    this.selection = event.relativeRect;
  };

  onSelectionToolChanged = (event: SelectionToolEvent): void => {
    this.selection = event.relativeRect;
  };

  onSnapshotClicked = (): void => {
    this.captureImage();
  };


  render(): TemplateResult {
    return html`
      <div class="grid">
        <div class="flex-column">
          <div class="container-video">
            <ap-selection-tool
              .aspectRatio="${this.pickedAspectRatio ?? this.videoAspectRatio}"
              .highlightSides="${this.highlight}"
              .highlightAxes="${this.highlight}"
              .forceAspectRatio="${this.forceAspectRatio}"
              .overlayImageSource="${this.overlaySource}"
              .guidelineLayout="${this.guidelineLayout}"
              .guidelineStyle="${this.guidelineStyle}"
              .snapOptions="${this.snapOptions}"
              .animationOptions="${this.animationOptions}"
              .showEndpoints=${true}
              .showBounds=${this.showBounds}
              .apInit="${this.onSelectonToolInitialized}"
              .apChange="${this.onSelectionToolChanged}"
            >
              <img id="player" src="docs/media/sample.png"/>
            </ap-selection-tool>
          </div>
          <div>
            <button @click="${this.onSnapshotClicked}">Snapshot</button>
            <label>
              <input
                type="checkbox"
                .checked=${this.forceAspectRatio}
                @change="${() =>
                  (this.forceAspectRatio = !this.forceAspectRatio)}"
              />
              Force aspect ratio
            </label>
            <label>
              <input
                type="checkbox"
                .checked=${!!this.snapOptions}
                @change="${() => this.toggleSnapping()}"
              />
              Enable snapping
            </label>
            <label>
              <input
                type="checkbox"
                .checked=${this.highlight}
                @change="${() => (this.highlight = !this.highlight)}"
              />Higlight edges/axes
            </label>
          </div>
        </div>
        <div class="flex-column snapshots">
          ${this.snapshots.map(
            (snapshot) => html`<img
              class="preview"
              src="${snapshot.url}"
              @click="${() => this.changeSnapshot(snapshot)}"
            />`
          )}
        </div>
      </div>
    `;
  }
}

declare global {
  interface Window {
    LICENSE_KEY: string;
  }
}
