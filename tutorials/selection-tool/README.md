# Introduction

This tutorial will showcase the Selection Tool component that is part of Accurate Player SDK.

The components are built on the Web Components standards to create custom elements that are framework agnostic. That means that you can use the components with
your favorite framework. With that said, some frameworks have some interoperability issues, but, in most cases, it is possible to work around those
issues. [Custom Element Everywhere](https://custom-elements-everywhere.com/) test frameworks and lists how well they interoperate if you are interested in learning more about
that.

We use [lit-html](https://lit-html.polymer-project.org/) in the html examples in this tutorial, but it should be fairly easy to translate to the framework of your liking.
Here's a brief explanation of the syntax, consult the [lit-html documentation](https://lit-html.polymer-project.org/guide/writing-templates) for further explanation:

Example lit-html syntax:

```html
<input
  class="${myClass}"
  ?disabled="${isDisabled}"
  .value="${value}"
  @input="${myCallbackFunction}"
/>
```

| Syntax                           | Explanation               |
| -------------------------------- | ------------------------- |
| `class="${myClass}"`             | Attribute binding         |
| `?disabled="${isDisabled}"`      | Boolean attribute binding |
| `.value="${value}"`              | Property binding          |
| `@input="${myCallbackFunction}"` | Register event listener   |

We also want to mention that we do not provide all the CSS and surrounding code in the code snippets that follow, we only include the most important parts. For a
complete picture, checkout the source code in this repository, more specifically `index.html` and `src/Application.ts`.

Our hope is that this tutorial will showcase how easy it is to set up the selection tool. Let's dig in!

# Basics

The _Application.ts_ is showing an example of how to use the Selection Tool in combination with a video element.

First off we need to install the accurate player components library to get access to the components.

```bash
pnpm add @accurate-player/accurate-player-components-external --dev
```

Next, we need to import the selection tool:

```typescript
import { ApSelectionTool } from "@accurate-player/accurate-player-components-external";
```

We also need to include a css theme file, there are two predefined themes that can be imported from `dist/styles/theme-dark.css` or `dist/styles/theme-light.css`
respectively. You can of course also include your theme colors by copying one of the included theme files and alter the color variables to your liking. If you use a bundler like webpack or similar you
can probably do something like this to import it from `node_modules`:

```typescript
import "~@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
```

Now we can include the selection tool and register it with the [CustomElementsRegistry](https://developer.mozilla.org/en-US/docs/Web/API/CustomElementRegistry):

```typescript
ApSelectionTool.register(); // <ap-selection-tool />
```

This will make the component registered for the tag `<ap-selection-tool />`. Component tag names are set to the class name converted to lowercase kebab case per default. Now we can use this tag in our html. The selection tool will wrap the media element that we want to capture from:

```html
<ap-selection-tool>
  <video id="player" crossorigin="anonymous"></video>
</ap-selection-tool>
```

![selection-tool-basic.png](docs/media/selection-tool-basic.png)

# Sample program

Now that we have the selection tool we will add a way to store snapshots we capture and display them in a list.

![selection-tool-list.png](docs/media/selection-tool-list.png)

Each time the Snapshot button is clicked a new image is captured using the _API_ method `extractImageUrl` of the selection tool and added to the list of snapshots. The image represents what is inside the selection area at that current time.
An argument to `extractImageUrl` can be used to specify the format of the captured image; "png", "jpeg" or "webp".

```typescript
this.snapshots = [
  ...this.snapshots,
  {
    frame: this.player?.api.getCurrentTime(TimeFormat.Frame) as number,
    url: this.api?.extractImageUrl("jpeg") ?? "",
    area: this.selection,
  },
];
```

Clicking on any of the captured images will pause the video if it is playing and then seek the video to the time that the snapshot was captured. Also, the selection area that was used when taking the snapshot is restored by calling the api method `selectArea` of the selection tool.

```typescript
this.player?.api
  .seek({
    time: snapshot.frame,
    format: TimeFormat.Frame,
    mode: SeekMode.Absolute,
  })
  .then(() => {
    this.api?.selectArea(snapshot.area);
    this.isPlaying = !this.player?.api.getStatus().paused;
  });
```

When clicking any of the checkboxes for "Force aspect ratio", "Enable snapping" and "Highlight edges/axes", corresponding properties 
in the program will be updated, like:

`this.forceAspectRatio = !this.forceAspectRatio`

and then the selection-tool will detect the change and be updated: 

```html
<ap-selection-tool
  ...
  .forceAspectRatio="${this.forceAspectRatio}"
  ...
>
  <video id="player" crossorigin="anonymous"></video>
</ap-selection-tool>
```


## Sample program based on an image

The _Application2.ts_ is showing an example of how the selection tool can be used in combination with an image instead of a video.
To run this sample change the row in `index.html` from:

`<script type="module" src="/src/Application.ts"></script>`

to 

`<script type="module" src="/src/Application2.ts"></script>`

The main difference is of course that the selection tool wraps an `img` html element instead of a `video` element.

```html
<ap-selection-tool>
  <img id="player" src="docs/media/sample.png" />
</ap-selection-tool>
```

This makes the example a bit more simple because the video does not need to be paused or seeked to a different time when clicking on the different snapshot images.
Also, when taking a snapshot there is no longer the concept of storing a frame number at the moment of capture.

```typescript
this.snapshots = [
  ...this.snapshots, 
  {
    url: this.api?.extractImageUrl("jpeg") ?? "",
    area: this.selection,
  },
];
```

There is also no need to initialize Accurate Player and loading the video file.


## Properties

### snapOptions

This is an interface with the following properties:

| Property | Description | Default | Required
| --- | --- | --- | ---
| `snapToCenter: boolean` | Enables the selection area to snap to either center axis. | `true` | Yes
| `snapToSides: boolean` | Enables the selection area to snap to any side of the selectable area. | `true` | Yes
| `instant: boolean` | Whether the selection area should snap instantly or only after release. | `false` | Yes
| `tolerance: number` | The proximity in pixels required for a part of the selection area to snap to its corresponding target. | `20` | Yes

#### snapToCenter

When this property is set to `true` the selection area will snap to both the vertical and horizontal center of the video.

![snapcenter.gif](docs/media/snapcenter.gif)


#### snapToSides

When this property is set to `true` the selection area will snap to the left and right side of the video as well as to the top and bottom.

![snapsides.gif](docs/media/snapsides.gif)

#### snapInstant

When this property is set to `true` the selection area will snap instantly when the selection area is within the distance specified by the `tolerance` property to the target.
When it is set to `false` the snapping will occur after the mouse button is released.

In the example below the `snapInstant` is set to `false` and in the above two examples it is set to `true`.

![snapinstant.gif](docs/media/snapinstant.gif)

#### tolerance

This property specifies how close, in number of pixels, the selection area should be to the target for snapping to occur.

In the example below the `tolerance` is set to `6`. In the examples above the `tolerance` is set to `20`.
![snaptolerance6.gif](docs/media/snaptolerance6.gif)


### forceAspectRatio
Forces the selection area to maintain the current aspect ratio. No matter how the mouse is being moved the aspect ratio is not changed.
This property is not required and the default value is `false`. 

![forceaspectratio.gif](docs/media/forceaspectratio.gif)

In the example below the `forceAspectRatio` is set to `false`. The selection area aspect ratio is updated while the mouse is being dragged.

![forceaspectratiooff.gif](docs/media/forceaspectratiooff.gif)

### overlayImageSource
With this property the url to an image to be used as an overlay in the selection area can be specified. It is recommended that a png image with transparency is used to not block the entire selection area.
This property is not required.

```html
<ap-selection-tool
  ...
  .overlayImageSource="${"path/to/overlay.png"}"
  ...
>
</ap-selection-tool>
```

![overlayimage.png](docs/media/overlayimage.png)


### guidelineLayout

Defines a grid pattern that is drawn across the selection area. Patterns can be assigned as integers defining the amount of lines to render along the corresponding orientation, or as arrays of floating point numbers between 0 and 1 representing the relative distribution of lines to render along the corresponding orientation.

This is an interface with the following properties:

| Property | Description | Default | Required
| --- | --- | --- | ---
| `predefinedLayout: "diagonal"  "triangle"` | A predefined layout type provided by the selection tool. |  | No
| `horizontalPattern: number  number[]` | The lines displayed horizontally across the selection area. Can be defined as a static amount or as an array of floating point numbers. |  | No
| `verticalPattern: number  number[]` | The lines displayed vertically across the selection area. Can be defined as a static amount or as an array of floating point numbers. |  | No


Example of "diagonal" guidelines:

```
guidelineLayout: {
  predefinedLayout: "diagonal"
}
```

![Diagonal guidelines](docs/media/guidelinediagonal.png "Diagonal guidelines")

Example of "triangle" guidelines:

```
guidelineLayout: {
  predefinedLayout: "triangle"
}
```

![Triangle guidelines](docs/media/guidelinetriangle.png "Triangle guidelines")

Example of horizontal and vertical guidelines:

```
guidelineLayout: {
  horizontalPattern: 2,
  verticalPattern: [0.1, 0.2, 0.5, 0.8, 0.9]
}
```


![Triangle guidelines](docs/media/guidelinegrid.png "Triangle guidelines")

### guidelineStyle

Defines the render style for any applied guidelines.
Supported values:

`default`: Standard lines colored according to css values\
`drop-shadow`: Default with drop shadow applied\
`color-inversion`: Line colors inverted from background\


### animationOptions

Options for enabling and customizing transitional animations for changes to the selection tool that do not involve direct user input.

Example:

```
animationOptions: {
  enabled: true,
  duration: 100,
  triggerEvent: false,
}
```

![Animation](docs/media/animationoptions.gif "Animation")


## Actions

### extractImageUrl

`api.extractImageUrl(format, quality)	(format: "png" | "jpeg" | "webp", quality?: boolean) => string`

This is the method to actually capture a snapshot of a media element (video or image). The area inside the 
selection area is what is being used to generate the new image. Parameters can be used to specify what the format of the image should be.
Note that the resulting image is not saved anywhere after calling this method. The value returned is a data url that can be used as a source tag for a html image  (the url is shortened by ....):

`data:image/jpeg;base64,/9j/4AAQSkZJRgABAQ....`

That data url can be directly used on an image html element like:

`<img class="preview" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAA....">`


### removeBlackBorders

If the video has black borders (letterboxed) those can be removed by calling this action. It will then not be possible
to move the selection area into the black borders. If the `showBounds` property is set to `true` the selectable area will get an outline and it will be more clear that the black borders has been removed.

If the property `highlightSides` is set to `true` then the boundaries will highlight when the selection area touches the edges of the black borders (see example image below).

![Remove black borders](docs/media/removeblackborders.png "Remove black borders")

The same effect can be accomplished by setting the property `suppressBlackBorders` to `true`.

### performPixelAnalysis

This is a more generic form of `removeBlackBorders` that can be used when the borders have a different color than black and also when the color is not exactly the same in the entire unwanted region but can vary slightly (i.e. noise). 

### maximizeSelectionArea, minimizeSelectionArea, selectArea, stretchSelectionArea, centerSelectionArea, resetSelectionArea

All these actions manipulate the selection area in various ways. `maximizeSelectionArea` and `resetSelectionArea` respects the current value of `forceAspectRatio`.
