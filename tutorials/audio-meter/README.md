# Introduction

This tutorial will showcase the Audio Meter component that is part of Accurate Player SDK.

We will first showcase how to display an audio meter for the muxed audio of the video, then we will look into how this can be expanded show audio levels of discrete audio files.

The components are built on the Web Components standards to create custom elements that are framework agnostic. That means that you can use the components with
your favorite framework. With that said, some frameworks have some interoperability issues, but, in most cases, it is possible to work around those
issues. [Custom Element Everywhere](https://custom-elements-everywhere.com/) test frameworks and lists how well they interoperate if you are interested in learning more about
that.

We use [lit-html](https://lit-html.polymer-project.org/) in the html examples in this tutorial, but it should be fairly easy to translate to the framework of your liking.
Here's a brief explanation of the syntax, consult the [lit-html documentation](https://lit-html.polymer-project.org/guide/writing-templates) for further explanation:

Example lit-html syntax:

```html
<input
  class="${myClass}"
  ?disabled="${isDisabled}"
  .value="${value}"
  @input="${myCallbackFunction}"
/>
```

| Syntax                           | Explanation               |
| -------------------------------- | ------------------------- |
| `class="${myClass}"`             | Attribute binding         |
| `?disabled="${isDisabled}"`      | Boolean attribute binding |
| `.value="${value}"`              | Property binding          |
| `@input="${myCallbackFunction}"` | Register event listener   |

We also want to mention that we do not provide all the CSS and surrounding code in the code snippets that follow, we only include the most important parts. For a
complete picture, checkout the source code in this repository, more specifically `index.html` and `src/Application.ts`.

Our hope is that this tutorial will showcase how easy it is to set up the audio meter. Let's dig in!

# Basics

First off we need to install the accurate player components library to get access to the components.

```bash
pnpm add @accurate-player/accurate-player-components-external --dev
```

Next, we need to import the audio meter and the `ChannelControlPlugin` which is a requirement for the audio meter to work:

```typescript
import { ApAudioMeterBasic } from "@accurate-player/accurate-player-components-external";

import { ChannelControlPlugin } from "@accurate-player/accurate-player-plugins";
```

We also need to include a css theme file, there are two predefined themes that can be imported from `dist/styles/theme-dark.css` or `dist/styles/theme-light.css`
respectively. You can of course also include your theme colors by copying one of the included theme files and alter the color variables to your liking. If you use a bundler like webpack or similar you
can probably do something like this to import it from `node_modules`:

```typescript
import "~@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
```

Now we can include the audio meter and register it with the [CustomElementsRegistry](https://developer.mozilla.org/en-US/docs/Web/API/CustomElementRegistry) and instantiate the `ChannelControlPlugin`:

```typescript
ApAudioMeterBasic.register(); // <ap-audio-meter-basic />

const channelControlPlugin = new ChannelControlPlugin(player);
```

This will make the component registered for the tag `<ap-audio-meter-basic />`. Component tag names are set to the class name converted to lowercase kebab case per default. Now we can use this tag in our html. We need to provide a player reference to the component, and then we are good to go:

```html
<ap-audio-meter-basic .player="${player}"></ap-audio-meter-basic>
```

Let's see what it looks like!

![Basic Audio Meter](docs/media/audio-meter-basic.gif "Audio Meter")

The audio meter also includes a settings menu that can be accessed through the settings button in the top left of the component. The settings menu allows you to change the functionality and appearance of the audio meter. It's possible to hide the settings button as well, e.g. if you want to trigger the settings menu in a different part of your application, or if you don't want to display it at all:

|                                                                                                          |                                                                                     |
| -------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| ![Audio Meter Settings Button](docs/media/audio-meter-settings-button.png "Audio Meter Settings Button") | ![Audio Meter Settings](docs/media/audio-meter-settings.png "Audio Meter Settings") |

# Plugins

With the basic setup done we will take a look at the optional plugins that interact with the audio meter.

## SmoothTimeUpdatePlugin

The `SmoothTimeUpdatePlugin` can be used to get more frequent updates, when active the update of sound levels will be responsive. We recommend that you use this plugin to get a better user experience.

```ts
import { SmoothTimeUpdatePlugin } from "@accurate-player/accurate-player-plugins";

const smoothTimeUpdatePlugin = new SmoothTimeUpdatePlugin(player);
```

## AudioScrubPlugin

The `AudioScrubPlugin` will simulate audio scrubbing when seeking. To enable audio scrubbing you only need to create the plugin. It will be enabled for all audio tracks by default and play a short segment of audio with every seek.

```ts
import { AudioScrubPlugin } from "@accurate-player/accurate-player-plugins";

const audioScrubPlugin = new AudioScrubPlugin(player);
```

![Audio Meter Scrub](docs/media/audio-meter-scrub.gif "Audio Meter Scrub")

## DiscreteAudioPlugin

The `DiscreteAudioPlugin` adds support for adding external or discrete audio tracks to the player. The plugin synchronizes audio and video playback.

When adding more discrete audio tracks we can add an audio meter per track.

```ts
import { DiscreteAudioPlugin } from "@accurate-player/accurate-player-plugins";

const discreteAudioPlugin = new DiscreteAudioPlugin(player);

// Add audio tracks
// Note. if ChannelControlPlugin is to be used, channelCount of each track must be set.
discreteAudioPlugin.initDiscreteAudioTracks([
  {
    track: {
      id: "example-track",
      channelCount: 2,
      label: "example-track",
      src: "/sample.mp3",
    },
    enabled: true,
  },
]);
```

Per default the audio meter will read audio levels from the loaded video file. In order to read audio levels from a different source, e.g. a discrete audio track, you set the `source` property to the `id` of the discrete audio track.

```html
<ap-audio-meter-basic .player="${this.player}"></ap-audio-meter-basic>
<ap-audio-meter-basic
  .player="${this.player}"
  .source="${'example-track'}"
></ap-audio-meter-basic>
```

![Audio Meter Double](docs/media/audio-meter-double.gif "Audio Meter Double")

When we have multiple audio meters we might want to have the same settings for all the audio meters. To achieve that we can use the `ApAudioMeterSettings` component separately.

```ts
import {
  ApAudioMeterBasic,
  ApAudioMeterSettings,
  AudioMeterSettings,
  PeakMode,
} from "@accurate-player/accurate-player-components-external";

ApAudioMeterBasic.register();     // <ap-audio-meter-basic />
ApAudioMeterSettings.register();  // <ap-audio-meter-settings />

const audioMeterSettings: AudioMeterSettings = {
  channelHeight: 400,
  channelWidth: 24,
  max: 0,
  min: -96,
  peakFadeTime: 1500,
  peakMode: PeakMode.DYNAMIC,
  showValleys: false,
  threshold: 0,
};

handleSettingsChange(event: CustomEvent<{ args: AudioMeterSettings[] }>): void {
  this.audioMeterSettings = event.detail.args[0];
}
```

By setting `.settingsEnabled="${false}"` on the audio meter we disable the built-in settings dialog, instead we add the settings dialog component ourselves. We add some state that toggles the visibility of the dialog, `settingsVisible`, as well as state for the different audio meter settings. We set the dialog to show on the click of a button and set it to hide on the `apclose` event that the settings dialog component emits when the user tries to close it. We add data bindings for all settings and hook those up to the audio meters as well. Now the settings for all audio meters are controlled from the same settings dialog:

```html
<button @click="${() => (this.settingsVisible = true)}">
  audio meter settings
</button>
${this.settingsVisible ? html`<ap-audio-meter-settings
  close-button
  @apsettingschange="${this.handleSettingsChange}"
  @apclose="${() => (this.settingsVisible = false)}"
  .min="${this.audioMeterSettings.min}"
  .max="${this.audioMeterSettings.max}"
  .peakMode="${this.audioMeterSettings.peakMode}"
  .showValleys="${this.audioMeterSettings.showValleys}"
  .peakFadeTime="${this.audioMeterSettings.peakFadeTime}"
  .threshold="${this.audioMeterSettings.threshold}"
  .channelWidth="${this.audioMeterSettings.channelWidth}"
  .channelHeight="${this.audioMeterSettings.channelHeight}"
></ap-audio-meter-settings
>` : null}

<ap-audio-meter-basic
  .player="${this.player}"
  .settingsEnabled="${false}"
  .min="${this.audioMeterSettings.min}"
  .max="${this.audioMeterSettings.max}"
  .peakMode="${this.audioMeterSettings.peakMode}"
  .showValleys="${this.audioMeterSettings.showValleys}"
  .peakFadeTime="${this.audioMeterSettings.peakFadeTime}"
  .threshold="${this.audioMeterSettings.threshold}"
  .channelWidth="${this.audioMeterSettings.channelWidth}"
  .channelHeight="${this.audioMeterSettings.channelHeight}"
></ap-audio-meter-basic>
<ap-audio-meter-basic
  .player="${this.player}"
  .source="${'example-track'}"
  .settingsEnabled="${false}"
  .min="${this.audioMeterSettings.min}"
  .max="${this.audioMeterSettings.max}"
  .peakMode="${this.audioMeterSettings.peakMode}"
  .showValleys="${this.audioMeterSettings.showValleys}"
  .peakFadeTime="${this.audioMeterSettings.peakFadeTime}"
  .threshold="${this.audioMeterSettings.threshold}"
  .channelWidth="${this.audioMeterSettings.channelWidth}"
  .channelHeight="${this.audioMeterSettings.channelHeight}"
></ap-audio-meter-basic>
```

![Audio Meter Custom Settings](docs/media/audio-meter-custom-settings.png "Audio Meter Custom Settings")

# Extended functionality

## Mono mix

When setting the property `monoMix` to `true` (default is `false`) the audio meter will show a single bar representing the total dB value for all tracks currently playing, both muxed and discrete tracks.

```html
<ap-audio-meter-basic .player="${player}" .monoMix="${true}"></ap-audio-meter-basic>
```
The picture below shows a mono mix audio meter when playing a stereo audio track.

![Audio Meter Mono Mix](docs/media/audio-meter-monomix.png "Audio Meter Mono Mix")


## Solo/mute buttons and channel labels

When setting the property `soloMuteEnabled` to `true` (default is `false`) the audio meter will show
buttons beneath the channel where the user can click on the `S`-button to only play that channel (all other channels and tracks will be muted) or 
the `M`-button to mute that specific channel.

To add labels to the channels the property `channelLabels` can be set with an array with string values 
for each channel. Only up to three characters per channel will be shown so make sure to not make any labels longer than
that, or it will be cut of. The labels will appear directly beneath the channel (above the solo/mute buttons if those are also enabled). 

```html
<ap-audio-meter-basic
  .player="${player}"
  .soloMuteEnabled="${true}"
  .channelLabels="${['L', 'R']}"
></ap-audio-meter-basic>
```

The picture below shows an audio meter width both solo/mute buttons and channel labels.

![Audio Meter Solo Mute](docs/media/audio-meter-solo-mute.png "Audio Meter Solo Mute")
