import {
  css,
  html,
  LitElement,
  PropertyValues,
  TemplateResult,
} from "lit";
import { customElement, property, query } from "lit/decorators.js";
import { ProgressivePlayer as AccuratePlayer } from "@accurate-player/accurate-player-progressive";

import {
  ApAudioMeterBasic,
  ApAudioMeterSettings,
  ApTooltip,
  AudioMeterSettings,
  PeakMode,
} from "@accurate-player/accurate-player-components-external";

import {
  AudioScrubPlugin,
  ChannelControlPlugin,
  DiscreteAudioPlugin,
  HotkeyPlugin,
  SmoothTimeUpdatePlugin,
} from "@accurate-player/accurate-player-plugins";

import "@accurate-player/accurate-player-controls";

import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
import { PlayerEventType } from "@accurate-player/accurate-player-core";

/*
 * Component library components need to be registered with the customElements registry
 * before they can be used (as they are WebComponents, read more: https://developer.mozilla.org/en-US/docs/Web/Web_Components).
 *
 * Calling register() will call customElements.define() under the hood.
 *
 */
ApAudioMeterBasic.register();
ApAudioMeterSettings.register();
ApTooltip.register();

/**
 * Application root element.
 */
@customElement("av-application")
export class Application extends LitElement {
  static styles = css`
    :host {
      background: var(--AP-BACKGROUND-BODY);
      display: flex;
      padding: 20px;
      flex-direction: column;
      width: calc(100% - 40px);
      height: calc(100% - 40px);
    }

    div.container-video {
      display: flex;
      flex: 1;
      justify-content: center;
      margin-bottom: 20px;
      max-height: 50vh;
      position: relative;
    }

    div.grid {
      display: flex;
      flex: 1 1 100%;
      padding-right: 200px;
    }

    ap-audio-meter-basic {
      margin-left: 16px;
    }

    ap-audio-meter-settings {
      z-index: 10000;
      position: absolute;
      top: 0;
      left: 0;
    }
  `;

  @query("#player")
  videoElement: HTMLVideoElement | undefined;

  @query("apc-controls")
  controls: any;

  @property()
  private player: AccuratePlayer | undefined;

  @property()
  private settingsVisible = false;

  @property()
  private audioMeterSettings: AudioMeterSettings = {
    channelHeight: 400,
    channelWidth: 24,
    max: 0,
    min: -96,
    peakFadeTime: 1500,
    peakMode: PeakMode.DYNAMIC,
    showValleys: false,
    threshold: 0,
  };

  private audioScrubPlugin: AudioScrubPlugin;
  private channelControlPlugin: ChannelControlPlugin;
  private discreteAudioPlugin: DiscreteAudioPlugin;
  private hotkeyPlugin: HotkeyPlugin;
  private smoothTimeUpdatePlugin: SmoothTimeUpdatePlugin;

  protected firstUpdated(changedProperties: PropertyValues) {
    super.firstUpdated(changedProperties);
    this.initializeAccuratePlayer();
  }

  async initializeAccuratePlayer() {
    if (this.videoElement == null) {
      console.warn(
        "Failed to initialize Accurate Player: video element could not be found."
      );

      return;
    }
    if (window.LICENSE_KEY == null) {
      console.warn(
        "Failed to initialize Accurate Player: no license key found, did you provide one in license-key.js?"
      );

      return;
    }
    if (this.controls == null) {
      console.warn(
        "Failed to initialize Accurate Player: could not find <apc-controls />"
      );

      return;
    }

    this.player = new AccuratePlayer(this.videoElement, window.LICENSE_KEY);
    this.audioScrubPlugin = new AudioScrubPlugin(this.player);
    this.channelControlPlugin = new ChannelControlPlugin(this.player);
    this.discreteAudioPlugin = new DiscreteAudioPlugin(this.player);
    this.hotkeyPlugin = new HotkeyPlugin(this.player);
    this.smoothTimeUpdatePlugin = new SmoothTimeUpdatePlugin(this.player);

    // Add audio tracks
    // Note. if ChannelControlPlugin is to be used, channelCount of each track must be set.
    this.discreteAudioPlugin.initDiscreteAudioTracks([
      {
        track: {
          id: "example-track",
          channelCount: 2,
          label: "example-track",
          src: "/sample.mp3",
        },
        enabled: true,
      },
    ]);

    this.player.api.loadVideoFile({
      label: "Big Buck Bunny",
      src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/original/Bunny_H264_Webproxy_TC2.mp4",
      enabled: true,
      channelCount: 2,
      frameRate: { numerator: 25, denominator: 1 },
      dropFrame: false,
    });

    this.player.on(PlayerEventType.Loaded, () => {
      console.log("Video file loaded");
    });

    const settings = {
      saveLocalSettings: true, // Enables storing of simple settings as volume, timeFormat and autoHide.
    };
    // The Polymer 3.0 project is currently written in Javascript. Will be converted to TypeScript with typings shortly!
    this.controls.init(this.videoElement, this.player, settings);
  }

  handleSettingsChange(
    event: CustomEvent<{ args: AudioMeterSettings[] }>
  ): void {
    this.audioMeterSettings = event.detail.args[0];
  }

  render(): TemplateResult {
    return html`
      <div class="grid">
        <div class="container-video">
          <video id="player" crossorigin="anonymous"></video>
          <apc-controls></apc-controls>
        </div>
        <div style="position: relative; display: flex; flex-direction: column;">
          <button
            style="margin: 0 0 8px 16px"
            @click="${() => (this.settingsVisible = true)}"
          >
            audio meter settings
          </button>
          ${this.settingsVisible
            ? html`<ap-audio-meter-settings
                close-button
                @apsettingschange="${this.handleSettingsChange}"
                @apclose="${() => (this.settingsVisible = false)}"
                .min="${this.audioMeterSettings.min}"
                .max="${this.audioMeterSettings.max}"
                .peakMode="${this.audioMeterSettings.peakMode}"
                .showValleys="${this.audioMeterSettings.showValleys}"
                .peakFadeTime="${this.audioMeterSettings.peakFadeTime}"
                .threshold="${this.audioMeterSettings.threshold}"
                .channelWidth="${this.audioMeterSettings.channelWidth}"
                .channelHeight="${this.audioMeterSettings.channelHeight}"
              ></ap-audio-meter-settings>`
            : null}
          <div style="display: flex">
            <ap-audio-meter-basic
              .player="${this.player}"
              .settingsEnabled="${false}"
              .min="${this.audioMeterSettings.min}"
              .max="${this.audioMeterSettings.max}"
              .peakMode="${this.audioMeterSettings.peakMode}"
              .showValleys="${this.audioMeterSettings.showValleys}"
              .peakFadeTime="${this.audioMeterSettings.peakFadeTime}"
              .threshold="${this.audioMeterSettings.threshold}"
              .channelWidth="${this.audioMeterSettings.channelWidth}"
              .channelHeight="${this.audioMeterSettings.channelHeight}"
            ></ap-audio-meter-basic>
            <ap-audio-meter-basic
              .player="${this.player}"
              .source="${"example-track"}"
              .settingsEnabled="${false}"
              .min="${this.audioMeterSettings.min}"
              .max="${this.audioMeterSettings.max}"
              .peakMode="${this.audioMeterSettings.peakMode}"
              .showValleys="${this.audioMeterSettings.showValleys}"
              .peakFadeTime="${this.audioMeterSettings.peakFadeTime}"
              .threshold="${this.audioMeterSettings.threshold}"
              .channelWidth="${this.audioMeterSettings.channelWidth}"
              .channelHeight="${this.audioMeterSettings.channelHeight}"
              .soloMuteEnabled="${true}"
              .channelLabels="${['L', 'R']}"
            ></ap-audio-meter-basic>
             <ap-audio-meter-basic
              .player="${this.player}"
              .monoMix="${true}"
              .settingsEnabled="${false}"
              .min="${this.audioMeterSettings.min}"
              .max="${this.audioMeterSettings.max}"
              .peakMode="${this.audioMeterSettings.peakMode}"
              .showValleys="${this.audioMeterSettings.showValleys}"
              .peakFadeTime="${this.audioMeterSettings.peakFadeTime}"
              .threshold="${this.audioMeterSettings.threshold}"
              .channelWidth="${12}"
              .channelHeight="${this.audioMeterSettings.channelHeight}"
            ></ap-audio-meter-basic>
          </div>
        </div>
      </div>
    `;
  }
}

declare global {
  interface Window {
    LICENSE_KEY: string;
  }
}
