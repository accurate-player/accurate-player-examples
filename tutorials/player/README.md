# Introduction

This article will showcase the Accurate Player Progressive which we will use when we dive deeper into the Accurate Player SDK. We will cover setup, parts of the API, and Accurate Player Controls. This should get you started with your usage of Accurate Player.

If the content is going to be DASH or HLS we provide [ABR Player](https://accurate.video/docs/api-reference/accurate-player-dash-api/). If the plan is to make an editor or EDL preview player we have the [Cutlist Player](https://accurate.video/docs/api-reference/accurate-player-cutlist-api/).

Our hope it that this article will showcase how easy it is to set up the player and get started. Let's dig in!

# Parts

We've divided the tutorial into 2 different topics, we recommend that you start with the basics to get just the player up and running.

1. [Basics](docs/BASICS.md)
1. [Plugins](docs/PLUGINS.md)
