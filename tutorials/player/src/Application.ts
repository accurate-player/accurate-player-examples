import { css, html, LitElement, PropertyValues, TemplateResult } from "lit";
import { customElement, property, query } from "lit/decorators.js";
import { ProgressivePlayer as AccuratePlayer } from "@accurate-player/accurate-player-progressive";

import {
  AudioScrubPlugin,
  ChannelControlPlugin,
  DiscreteAudioPlugin,
  HotkeyPlugin,
  IMSCParser,
  IMSCRenderer,
  PointPlugin,
  PointValidationFailedEvent,
  SCCParser,
  SCCRenderer,
  SmoothTimeUpdatePlugin,
  TimedTextPlugin,
  WebVTTParser,
  WebVTTRenderer,
} from "@accurate-player/accurate-player-plugins";

import "@accurate-player/accurate-player-controls";

import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
import { PlayerEventType, SubtitleFormat } from "@accurate-player/accurate-player-core";

/**
 * Application root element.
 */
@customElement("av-application")
export class Application extends LitElement {
  static styles = css`
    :host {
      background: var(--AP-BACKGROUND-BODY);
      display: flex;
      padding: 20px;
      flex-direction: column;
      width: calc(100% - 40px);
      height: calc(100% - 40px);
    }

    div.container-video {
      display: flex;
      justify-content: center;
      position: relative;
      margin-bottom: 20px;
      align-self: center;
    }

    video {
      min-width: 100%;
      height: auto;
    }
  `;

  @query("#player")
  videoElement: HTMLVideoElement | undefined;

  @query("apc-controls")
  controls: any;

  @property()
  private player: AccuratePlayer | undefined;

  private audioScrubPlugin: AudioScrubPlugin;
  private channelControlPlugin: ChannelControlPlugin;
  private discreteAudioPlugin: DiscreteAudioPlugin;
  private hotkeyPlugin: HotkeyPlugin;
  private pointPlugin: PointPlugin;
  private smoothTimeUpdatePlugin: SmoothTimeUpdatePlugin;
  private timedTextPlugin: TimedTextPlugin;

  protected firstUpdated(changedProperties: PropertyValues) {
    super.firstUpdated(changedProperties);
    this.initializeAccuratePlayer();
  }

  async initializeAccuratePlayer() {
    if (this.videoElement == null) {
      console.warn(
        "Failed to initialize Accurate Player: video element could not be found."
      );

      return;
    }
    if (window.LICENSE_KEY == null) {
      console.warn(
        "Failed to initialize Accurate Player: no license key found, did you provide one in license-key.js?"
      );

      return;
    }
    if (this.controls == null) {
      console.warn(
        "Failed to initialize Accurate Player: could not find <apc-controls />"
      );

      return;
    }

    this.player = new AccuratePlayer(this.videoElement, window.LICENSE_KEY);
    this.audioScrubPlugin = new AudioScrubPlugin(this.player);
    this.smoothTimeUpdatePlugin = new SmoothTimeUpdatePlugin(this.player);
    this.channelControlPlugin = new ChannelControlPlugin(this.player);

    this.hotkeyPlugin = new HotkeyPlugin(this.player);
    this.hotkeyPlugin.setHotkey(
      "ctrl+shift+m",
      this.customHotkey,
      "Description of hotkey."
    );

    this.pointPlugin = new PointPlugin(
      this.player,
      false,
      (points, eventCallback) => {
        // If the user attempts to add an out point.
        if (points.out) {
          // Trigger an event with appropriate error message.
          eventCallback(
            new PointValidationFailedEvent("Not allowed to add out point!")
          );
          // Return false to cancel point addition.
          return false;
        }

        // Return true to continue the point addition.
        return true;
      }
    );

    this.timedTextPlugin = new TimedTextPlugin(
      this.player,
      [WebVTTParser, SCCParser, IMSCParser], // Parsers
      [WebVTTRenderer, SCCRenderer, IMSCRenderer], // Renderers
      {
        /**
         * Enables verbose logging if enabled. Default is same as player settings.
         */
        verbose: true,
        /**
         * List of subtitle formats that should use the video start time as fallback for the subtitle "startTime".
         * e.g. If set to [SubtitleFormat.VTT] and the video has a start time. All VTT subtitles will apply the video start time.
         */
        useStartTimeFallback: [],
        /**
         * Default settings object to pass to the renderer constructor
         */
        rendererSettings: {},
        /**
         * Default settings object to pass to the parser constructor
         */
        parserSettings: {},
      }
    );

    this.timedTextPlugin.add(
      {
        id: "vtt_sub_1",
        src: "/vtt_subtitle.vtt",
        label: "VTT Subtitle",
        enabled: true,
        format: SubtitleFormat.VTT,
      },
      {
        id: "scc_sub_1",
        src: "/scc_subtitle.scc",
        label: "SCC Subtitle",
        enabled: false,
        format: SubtitleFormat.SCC,
      },
      {
        id: "imsc_sub_1",
        src: "/imsc_subtitle.ttml",
        label: "IMSC Subtitle",
        enabled: false,
        format: SubtitleFormat.IMSC,
      }
    );

    this.discreteAudioPlugin = new DiscreteAudioPlugin(this.player);

    // Add audio tracks
    // Note. if ChannelControlPlugin is to be used, channelCount of each track must be set.
    this.discreteAudioPlugin.initDiscreteAudioTracks([
      {
        track: {
          id: "example-track",
          channelCount: 2,
          label: "example-track",
          src: "/sample.mp3",
        },
        enabled: true,
      },
    ]);

    this.player.api.loadVideoFile({
      label: "Big Buck Bunny",
      src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/original/Bunny_H264_Webproxy_TC2.mp4",
      enabled: true,
      channelCount: 2,
      frameRate: { numerator: 25, denominator: 1 },
      dropFrame: false,
    });

    this.player.on(PlayerEventType.Loaded, () => {
      console.log("Video file loaded");
    });

    const settings = {
      saveLocalSettings: true, // Enables storing of simple settings as volume, timeFormat and autoHide.
    };
    // The Polymer 3.0 project is currently written in Javascript. Will be converted to TypeScript with typings shortly!
    this.controls.init(this.videoElement, this.player, settings);
  }

  customHotkey(): void {
    console.log("Custom hotkey");
  }

  render(): TemplateResult {
    return html`
      <div class="container-video">
        <video id="player" crossorigin="anonymous"></video>
        <apc-controls></apc-controls>
      </div>
    `;
  }
}

declare global {
  interface Window {
    LICENSE_KEY: string;
  }
}
