# Basics

First off we need to install the accurate player core and progressive player library.

```bash
pnpm add @accurate-player/accurate-player-core @accurate-player/accurate-player-progressive
```

We will add a video element to our page which we will attach Accurate Player to.

```html
<video id="player"></video>
```

Next we will import the player and set it up.

```ts
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";

const videoElement = document.getElementById("player");
const player = new ProgressivePlayer(videoElement);
player.api.loadVideoFile({
  label: "Big Buck Bunny",
  src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/original/Bunny_H264_Webproxy_TC2.mp4",
  enabled: true,
  frameRate: { numerator: 25, denominator: 1 },
  dropFrame: false,
});
```

![player.png](media/player.png)

Now when the player is running we can start interacting with it through the [player API](https://accurate.video/docs/api-reference/accurate-player-progressive-api/).
With the API we can either build custom controls to interact with the player or fetch information about the current file being played.

### Playback

```ts
player.api.play();
player.api.pause();
player.api.increasePlaybackRate();
player.api.decreasePlaybackRate();
```

### Volume

```ts
player.api.mute();
player.api.unmute();
player.api.setVolume(1);
```

### Information

It might sometimes be useful to retrieve information about the current file being played in Accurate Player and for that we have `getActiveFile`. If you instead are interested in more information about the current status, `getStatus` will provide a more comprehensive status.

```ts
player.api.getActiveFile();
--> {
  channelCount: 2,
  dropFrame: false,
  enabled: true,
  frameRate: { numerator: 25, denominator: 1 },
  id: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/original/Bunny_H264_Webproxy_TC2.mp4",
  label: "Big Buck Bunny",
  src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/original/Bunny_H264_Webproxy_TC2.mp4",
}

player.api.getStatus();
--> {
  buffered: { start: 0, end: 2.24 },
  currentTime: 0,
  duration: 572.6,
  ended: false,
  isLooping: false,
  loaded: true,
  loopInterval: undefined,
  muted: false,
  muxedAudio: true,
  paused: true,
  playbackRate: 1,
  videoFiles: [{…}],
  volume: 1,
}
```

Note that the functions above are not a complete set of functions and you can find the full set in the [player API](https://accurate.video/docs/api-reference/accurate-player-progressive-api/).

### Events

It's not only the API functions that allow us to interact with the player. We can also react to Events that the player is emitting. For a full list of available events see the [Core Player API](https://accurate.video/docs/api-reference/accurate-player-progressive-api/).

```ts
import { PlayerEventType } from "@accurate-player/accurate-player-core";

this.player.on(PlayerEventType.Loaded, () => {
  console.log("Video file loaded");
});
```

## Time format

Before digging deeper into some of the available utility and API functions we will take a brief look at the different time formats that are available. Since some functions support multiple different time formats, to select which format that will be used we can use the `enum TimeFormat` which is available in the core package. Below are some examples of the different time formats.

```ts
import { TimeFormat } from "@accurate-player/accurate-player-core";
```

| Format type  | Enum | Format                        | Example           |
| ------------ | ---- | ----------------------------- | ----------------- |
| Timecode     | 0    | SMPTE timecode                | 00:04:53:08       |
| Frame        | 1    | Frame number                  | 7333              |
| Time         | 2    | Time as HH:MM:SS              | 00:04:53          |
| Seconds      | 3    | Time as seconds with decimals | 293.32            |
| Milliseconds | 4    | Time as milliseconds          | 293320            |
| Percent      | 5    | Time as percent of duration   | 51.22598672720922 |

To help with changing between different time formats we provide some utility functions in the core package.

```ts
import {
  toFrames,
  toSeconds,
  frameToSMPTE,
  secondsToTimecode,
} from "@accurate-player/accurate-player-core";

toFrames("00:04:53:08", TimeFormat.Timecode, frameRate, dropFrame, duration);
--> 7333

frameToSMPTE(7333, frameRate, dropFrame);
--> "00:04:53:08"

toSeconds("00:04:53:08", TimeFormat.Timecode, frameRate, dropFrame, duration);
--> 293.32

secondsToTimecode(293.32, frameRate, dropFrame);
--> "00:04:53:08"
```

The player api exposes functions to get both the duration of the video file and getting the current time, both of these functions have support for getting the time in a specified [Time format](#Time-format).

```ts
player.api.getDuration(TimeFormat.Timecode);
--> "00:09:32:15"

player.api.getCurrentTime(TimeFormat.Timecode);
--> "00:00:00:00"
```

Another function that is good to have a look at is `seek`. Seeking can be done in three different modes, available as an `enum SeekMode` in the core package. Seeking can be done with any of the [Time formats](#Time-format). When calling `seek` the function will return a promise that resolves once the seek is completed.

```ts
import { SeekMode } from "@accurate-player/accurate-player-core";
```

| SeekMode         | Enum | Mode                                       |
| ---------------- | ---- | ------------------------------------------ |
| RelativeBackward | 0    | Will move backwards the distance specified |
| RelativeForward  | 1    | Will move forwards the distance specified  |
| Absolute         | 2    | Will move absolute the specified time      |

Example usage

```ts
player.api
  .seek({
    format: TimeFormat.Timecode,
    time: "00:04:53:08",
    mode: SeekMode.Absolute,
  })
  .then(() => {
    console.log("Seek done");
  });

player.api
  .seek({
    format: TimeFormat.Seconds,
    time: 1,
    mode: SeekMode.RelativeBackward,
  })
  .then(() => {
    console.log("Seek done");
  });

player.api
  .seek({
    format: TimeFormat.Frame,
    time: 1,
    mode: SeekMode.RelativeForward,
  })
  .then(() => {
    console.log("Seek done");
  });
```

## Support for Variable Frame Rate (VFR)

Accurate player offers support for video files with Variable Frame Rate (VFR). In order to take advantage of these features you will need to load the video files through the method `getMediaInfo` in order to make the necessary adjustments of the player. If we modify the previous example it'll look something like this:

```ts
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";
import { getMediaInfo } from "@accurate-player/probe";

const videoElement = document.getElementById("player");
const player = new ProgressivePlayer(videoElement);
getMediaInfo(
  "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/vfr/output-vfr.mp4"
).then((file) => {
  player.api.loadVideoFile(file);
});
```

This will work with both variable and constant frame rate videos.

## Timecode and VFR.

Since SMPTE timecodes aren't available for VFR we adjusted the player so that Timecode will use a HH:MM:SS.mmm format when loading a video with VFR.

## First frame duplicated issue.

A fairly common issue is that the first frame of a video seems to be duplicated when played in accurate player, causing the entire file being "one frame off". This is usually caused by the audio stream having a slightly earlier first PTS (The audio stream starts slightly before the video), the browser handles this by duplicating the first video frame until the video stream starts.

To handle this scenario we provide a method `getMediaInfo`, that takes an url to a MP4 as input and returns a VideoFile object, with the adjustments needed to play the video frame accurately.

Example:

```ts
import { getMediaInfo } from "@accurate-player/probe";

getMediaInfo(
  "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/timecode/sintel-2048-timecode-stereo.mp4"
).then((videoFile) => {
  player.api.loadVideoFile(videoFile);
});
```

# Accurate Player Controls

To make controls and plugins (a topic we will cover in a later section) easy to manage we provide a package that provides a default UI layer on top of the player. These controls adapt to player configuration and the plugins that are loaded. The controls can be customized with other fonts or colors.

Adding the package

```bash
pnpm add @accurate-player/accurate-player-controls
```

We will now update the markup we used previously to include the `<apc-controls>`. One thing to note is that the container element for the controls must have either position relative or absolute, since the controls fills its parent absolutely.

```html
<div class="container-video">
  <video id="player"></video>
  <apc-controls></apc-controls>
</div>
```

Next we will extend our example so that it provides the controls with a reference to the video element and the player.

```ts
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";

const videoElement = document.getElementById("player");
const player = new ProgressivePlayer(videoElement);
player.api.loadVideoFile({
  label: "Big Buck Bunny",
  src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/original/Bunny_H264_Webproxy_TC2.mp4",
  enabled: true,
  frameRate: { numerator: 25, denominator: 1 },
  dropFrame: false,
});

const controls = document.getElementByTag("apc-controls")[0];
const settings = {
  saveLocalSettings: true, // Enables storing of simple settings as volume, timeFormat and autoHide.
};
controls.init(videoElement, player, settings);
```

![player.png](media/player_with_controls.png)

There are a few more settings that are not used in the example above and here is the full list of settings with descriptions and their default values. A good thing to remember is that the default values for `autoHide` and `volume` will be overwritten by the user settings if `saveLocalSettings` is `true`.

| Setting            | Description                                                                               | Default value                                                                                                   |
| ------------------ | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------- |
| autoHide           | Should the controls hide when the video is playing or the cursor is outside of the player | true                                                                                                            |
| volume             | Sets the initial value for the volume of the player                                       | 1                                                                                                               |
| timeFormat         | :                                                                                         | { id: 0, label: "Timecode" }                                                                                    |
| saveLocalSettings  | Should user settings like time format & volume save in localStorage                       | true                                                                                                            |
| togglePlayOnClick  | Should playback toggle when clicking on the video                                         | true                                                                                                            |
| loadedDebounceTime | Time in ms before player is considered loading                                            | 500                                                                                                             |
| verbose            | If verbose logging should be enabled                                                      | false                                                                                                           |
| labels             | Ability to configure heading texts in subtitle popup                                      | { subtitlePopup: { subtitles: "SUBTITLES", discreteAudio: "DISCRETE AUDIO", audioSettings: "AUDIO SETTINGS" } } |

← [Parts](../README.md#parts) | [Plugins](PLUGINS.md) →
