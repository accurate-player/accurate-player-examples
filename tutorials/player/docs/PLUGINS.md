# Player plugins

We will now take a look at the plugins available for Accurate Player. They are all available through the package `accurate-player-plugins`.

```bash
pnpm add @accurate-player/accurate-player-plugins
```

## Plugins

- [Player plugins](#player-plugins)
  - [Plugins](#plugins)
  - [HotkeyPlugin](#hotkeyplugin)
  - [AudioScrubPlugin](#audioscrubplugin)
    - [Limitations](#limitations)
    - [Accuracy](#accuracy)
  - [ChannelControlPlugin](#channelcontrolplugin)
    - [System requirements](#system-requirements)
    - [Known limitations](#known-limitations)
      - [AudioContext not allowed to start](#audiocontext-not-allowed-to-start)
      - [Firefox playback rate issue](#firefox-playback-rate-issue)
      - [Firefox Linux, only left channel](#firefox-linux-only-left-channel)
      - [Encoding restrictions for ABR](#encoding-restrictions-for-abr)
      - [Safari issues](#safari-issues)
  - [DiscreteAudioPlugin](#discreteaudioplugin)
    - [Limitations](#limitations-1)
  - [PointPlugin](#pointplugin)
  - [SmoothTimeUpdatePlugin](#smoothtimeupdateplugin)
  - [SubtitlePlugin](#subtitleplugin)
  - [VttSubtitlePlugin](#vttsubtitleplugin)
  - [SccSubtitlePlugin](#sccsubtitleplugin)
  - [ImscSubtitlePlugin](#imscsubtitleplugin)
  - [TimedTextPlugin](#timedtextplugin)
    - [Subtitle formats](#subtitle-formats)
  - [Summary](#summary)


## HotkeyPlugin

To make interacting with the player easier than before, we can add hotkeys in addition to the accurate player controls. As soon as we instantiate the `HotkeyPlugin` all the predefined hotkeys will be ready to use.

```ts
import { HotkeyPlugin } from "@accurate-player/accurate-player-plugins";

const hotkeyPlugin = new HotkeyPlugin(player);
```

We can also see that the player controls picks up that we added the `HotkeyPlugin` and will now provide a list of all the hotkeys available.

![menu_hotkeys.gif](media/menu_hotkeys.gif)

With this plugin we aren't limited to using only the predefined hotkeys. You can add your own custom ones by calling `setHotkey`

```ts
hotkeyPlugin.setHotkey(
  "ctrl+shift+m",
  this.customHotkey,
  "Description of hotkey."
);
```

## AudioScrubPlugin

The `AudioScrubPlugin` will simulate audio scrubbing when seeking. To enable audio scrubbing you only need to create the plugin. It will be enabled for all audio tracks by default and play a short segment of audio with every seek.

```ts
import { AudioScrubPlugin } from "@accurate-player/accurate-player-plugins";

const audioScrubPlugin = new AudioScrubPlugin(player);
```

### Limitations

This plugin only works with tracks that uses progressive downloads. I.e. audio tracks through `DiscreteAudioPlugin` and/or video track through `ProgressivePlayer` or `CutlistPlayer`.

### Accuracy

The audio when scrubbing tries to be as accurate as possible, but it is only an estimation. We do not guarantee that the sound heard represent just the sound associated with a frame. You can tweak the audio sample duration to get a more exact result, using the `frameSoundDuration` setting.

## ChannelControlPlugin

The `ChannelControlPlugin` give the possibility to manipulate individual channels within an audio track. The plugin heavily depends on the [Web Audio API](https://www.w3.org/TR/webaudio/). The features of this plugin are:

- gain control at channel level.
- rerouting of individual channels.
- analysing data at channel level.

```ts
import { ChannelControlPlugin } from "@accurate-player/accurate-player-plugins";

const channelControlPlugin = new ChannelControlPlugin(player);
```

When adding the plugin it automatically gives us control over all channels.

![channel_mute.png](media/channel_mute.png)

This is an example of how to both add our tracks manually and control the channels through code instead of using the controls:

```ts
// Initialize plugin with auto register disabled.
const channelControlPlugin = new ChannelControlPlugin(player, {
  autoRegister: false,
});

// Web Audio Api requires that the used HTMLVideoElement have the crossorigin attribute set to anonymous
// if the source domain of the stream differs from the hosted
mediaElement.crossorigin = "anonymous";
channelControlPlugin.registerTracks({
  nativeElement: mediaElement,
  channelCount: 2,
});

// Url may be the video url provided to the player or the url of a discrete audio track if DiscreteAudioPlugin is used.
channelControlPlugin.mute(url, channelIndex);
channelControlPlugin.solo(url, channelIndex);
```

### System requirements

The most robust setup in all cases is to use the setting `preferredOutputCount = 2` that will downmix the channels to a stereo output. This will work, at least, in FireFox and Chrome.

To work with surround sound the browser needs to be able to detect that your system output device supports surround output. This usually works well using Chrome on Windows and OS X.

### Known limitations

Since this plugin is based on the [Web Audio API](https://www.w3.org/TR/webaudio/) there are some browser specific bugs as follows:

#### AudioContext not allowed to start

The [Web Audio API](https://www.w3.org/TR/webaudio/) have a limitation that it is not allowed to start before user interaction. If this happens, you will see a console error stating: `The AudioContext was not allowed to start`. The `AudioContext` must be created or resumed after user interaction. To recover from this, you need to manually call the method `resumeAudioContext` after the first user interaction.

#### Firefox playback rate issue

[Mozilla bug #966247](https://bugzilla.mozilla.org/show_bug.cgi?id=966247) When activating the `AudioContext`, the playback rate is reset to `1x` and makes impossible it to adjust playback rate.

#### Firefox Linux, only left channel

[Mozilla bug #1435598](https://bugzilla.mozilla.org/show_bug.cgi?id=1435598)

In Firefox Linux edition there are issues related to output `5.1` and `7.1` audio. The result is that all channels are mapped to the left channel. Until the bug is resolved we cannot support surround features in Firefox. The bug appears in some form as soon as Firefox detects that system output device has surround capabilities.

#### Encoding restrictions for ABR

The channel layout property is sensitive when using ABR (ABR and HLS players). In order to make it work the following channel layouts are supported:

| channels | layout |
| -------- | ------ |
| 1        | mono   |
| 2        | stereo |
| 3        | 3.0    |
| 4        | 4.0    |
| 5        | 5.0    |
| 6        | 5.1    |
| 7        | N/A    |
| 8        | 7.1    |

#### Safari issues

There are some issues with the implementation of [Web Audio API](https://www.w3.org/TR/webaudio/) in Safari that is not handled in the current implementation of the plugin. For now, the channel plugin does not support Safari.

## DiscreteAudioPlugin

The `DiscreteAudioPlugin` adds support for adding external or discrete audio tracks to the player. The plugin synchronizes audio and video playback.

```ts
import { DiscreteAudioPlugin } from "@accurate-player/accurate-player-plugins";

const discreteAudioPlugin = new DiscreteAudioPlugin(player);

// Add audio tracks
// Note. if ChannelControlPlugin is to be used, channelCount of each track must be set.
discreteAudioPlugin.initDiscreteAudioTracks([
  {
    track: {
      id: "example-track",
      channelCount: 2,
      label: "example-track",
      src: "/sample.mp3",
    },
    enabled: true,
  },
]);
```

The player controls let you control which audio tracks are enabled.

![discrete_tracks.png](media/discrete_tracks.png)

It is also possible to enable and disable tracks via the plugin by calling `enableTrack` and `disableTrack` with the `id` of the track.

```ts
discreteAudioPlugin.enableTrack("example-track");
discreteAudioPlugin.disableTrack("example-track");
```

### Limitations

Browsers have a limit for simultaneous downloads from a single domain. It is not possible to play more than, dependent on browser, 5 discrete audio tracks at the same time (more about the browser limitation [here](https://docs.pushtechnology.com/cloud/latest/manual/html/designguide/solution/support/connection_limitations.html)).

## PointPlugin

The `PointPlugin` adds support for setting frame accurate in- and out points, which can then be used in your application for creating time based metadata.

```ts
import { PointPlugin } from "@accurate-player/accurate-player-plugins";

const pointPlugin = new PointPlugin(player);
```

If we have both the `PointPlugin` and `HotkeyPlugin` we can use the default hotkeys to set our points. The default hotkeys are `i` to set the `inPoint` and `o` to set the `outPoint`. Another option is to use the plugin api functions `setInPoint` and `setOutPoint`.

![in_out_points.png](media/in_out_points.png)

We can also add custom validation logic, e.g. restricting to only allow `inPoint` to be set:

```ts
import { PointPlugin } from "@accurate-player/accurate-player-plugins";

const pointPlugin = new PointPlugin(player, false, (points, eventCallback) => {
  // If the user attempts to add an out point.
  if (points.out) {
    // Trigger an event with appropriate error message.
    eventCallback(
      new PointValidationFailedEvent("Not allowed to add out point!")
    );
    // Return false to cancel point addition.
    return false;
  }

  // Return true to continue the point addition.
  return true;
});
```

## SmoothTimeUpdatePlugin

The `SmoothTimeUpdatePlugin` can be used to get more frequent updates than the native video element can provide. If you are using the player controls it is enough to instantiate the plugin to get the more frequent updates.

```ts
import { SmoothTimeUpdatePlugin } from "@accurate-player/accurate-player-plugins";

const smoothTimeUpdatePlugin = new SmoothTimeUpdatePlugin(player);
```

| Without plugin                                            | With plugin                                       |
| --------------------------------------------------------- | ------------------------------------------------- |
| ![non_smooth_timecode.gif](media/non_smooth_timecode.gif) | ![smooth_timecode.gif](media/smooth_timecode.gif) |

If you are using your own custom element to display time you would listen to the `TimeUpdate` event from the plugin:

```ts
// Add event listener to perform frequent updates.
smoothTimeUpdatePlugin.on(
  SmoothTimeUpdateEventType.TimeUpdate,
  (event: SmoothTimeUpdateEvent) => {
    // Update some visual element using the current frame value
    this.timeDisplay.setCurrentFrame(event.currentFrame);
  }
);
```

## SubtitlePlugin

_This plugin is deprecated in favor of [TimedTextPlugin](#TimedTextPlugin). See [Migration Guide](/MIGRATION.md) for examples._  

The `SubtitlePlugin` adds support for adding custom subtitles. Instead of adding track elements to the video, is uses the js API to add `TextTracks` to the video.

If subtitles are provided using vtt-files the [VttSubtitlePlugin](##VttSubtitlePlugin) should be used instead.

```ts
import { SubtitlePlugin } from "@accurate-player/accurate-player-plugins";

const subtitlePlugin = new SubtitlePlugin(player);

// Initialize subtitles
this.subtitlePlugin.addSubtitleTracks([
  {
    kind: "subtitles",
    language: "en",
    mode: TEXT_TRACK_MODE.Showing,
    cues: [
      {
        id: "id-1",
        startSeconds: 1,
        endSeconds: 5,
        text: "This is a subtitle cue",
      },
    ],
  },
]);
```

![subtitles.png](media/subtitles.png)

## VttSubtitlePlugin

_This plugin is deprecated in favor of [TimedTextPlugin](#TimedTextPlugin). See [Migration Guide](/MIGRATION.md) for examples._

The `VttSubtitlePlugin` adds support for adding external [WebVTT subtitle tracks](https://www.w3.org/TR/webvtt1/) to the player. The subtitles follow the specification for [text track](https://www.w3.org/TR/html51/semantics-embedded-content.html#text-tracks).

```ts
import { VttSubtitlePlugin } from "@accurate-player/accurate-player-plugins";

const vttSubtitlePlugin = new VttSubtitlePlugin(player);

// Initialize subtitles
vttSubtitlePlugin.createSubtitles([
  {
    kind: "subtitles",
    label: "vtt_subtitle",
    srclang: "en",
    src: "/vtt_subtitle.vtt",
    enabled: true,
  },
]);
```

![vtt_subtitles.png](media/vtt_subtitles.png)

## SccSubtitlePlugin

_This plugin is deprecated in favor of [TimedTextPlugin](#TimedTextPlugin). See [Migration Guide](/MIGRATION.md) for examples._

The `SccSubtitlePlugin` adds support for adding external Scenarist Closed Captions (SCC), to the player.

```ts
import { SccSubtitlePlugin } from "@accurate-player/accurate-player-plugins";

const sccSubtitlePlugin = new SccSubtitlePlugin(player);

// Add subtitles
sccSubtitlePlugin.add([{
  id: "scc_sub_1",
  src: "/scc_subtitle.scc",
  enabled: true
}]);
```

![vtt_subtitles.png](media/scc_subtitles.png)

## ImscSubtitlePlugin

_This plugin is deprecated in favor of [TimedTextPlugin](#TimedTextPlugin). See [Migration Guide](/MIGRATION.md) for examples._

The `ImscSubtitlePlugin` adds support for render [IMSC 1.0.1](https://www.w3.org/TR/ttml-imsc1.0.1/) and [IMSC 1.1](https://www.w3.org/TR/ttml-imsc1.1/) by loading external [TTML](https://www.w3.org/TR/2018/REC-ttml2-20181108/) subtitle files into the player.

```ts
import { ImscSubtitlePlugin } from "@accurate-player/accurate-player-plugins";

const imscSubtitlePlugin = new ImscSubtitlePlugin(player);

// Load the IMSC TTML files into the plugin and set the first one marked
// as active as the active subtitle
// (label and language can be set manually if missing from TTML file)
const imscSubtitles = await imscSubtitlePlugin.load([
  {
    src: "/imsc_subtitle.ttml",
    id: "subtitle-en",
    active: true,
  },
]);
```

If we use the player controls we can use those to change which subtitle is active. Another option is the use the plugin api:

```ts
// Switch subtitle
imscSubtitlePlugin.setActiveImscSubtitle("subtitle-eng");

// Temporarily disable subtitles
imscSubtitlePlugin.deactivateImscSubtitles();

// Re-enable subtitles
imscSubtitlePlugin.activateImscSubtitles();

// Remove subtitles and dispose of plugin
imscSubtitlePlugin.destroy();
```

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="media/imsc_subtitles.mp4" type="video/mp4">
  </video>
</figure>

## TimedTextPlugin
The `TimedTextPlugin` can be used to load a variety of different subtitle formats using predefined and/or custom parsers and renderers.

```ts

import {
  IMSCParser,
  IMSCRenderer,
  SCCParser,
  SCCRenderer,
  TimedTextPlugin,
  WebVTTParser,
  WebVTTRenderer,
} from "@accurate-player/accurate-player-plugins";

import { SubtitleFormat } from "@accurate-player/accurate-player-core";

const timedTextPlugin = new TimedTextPlugin(
  player,
  [WebVTTParser, SCCParser, IMSCParser], 
  [WebVTTRenderer, SCCRenderer, IMSCRenderer], 
  {
   /**
     * Enables verbose logging if enabled. Default is same as player settings.
     */
    verbose: true,
    /**
     * List of subtitle formats that should use the video start time as fallback for the subtitle "startTime".
     * e.g. If set to [SubtitleFormat.VTT] and the video has a start time. All VTT subtitles will apply the video start time.
     */
    useStartTimeFallback: [],
    /**
     * Default settings object to pass to the renderer constructor
     */
    rendererSettings,
    /**
     * Default settings object to pass to the parser constructor
     */
    parserSettings,
  },
);

/**
 * Note that the add() function does not take an array
 * but instead rest parameters (indefinite number of arguments)
 */
timedTextPlugin.add(
  {
    id: "vtt_sub_1",
    src: "/vtt_subtitle.vtt",
    label: "VTT Subtitle",
    enabled: true,
    format: SubtitleFormat.VTT,
  },
  {
    id: "scc_sub_1",
    src: "/scc_subtitle.scc",
    label: "SCC Subtitle",
    enabled: false,
    format: SubtitleFormat.SCC,
  },
  {
    id: "imsc_sub_1",
    src: "/imsc_subtitle.ttml",
    label: "IMSC Subtitle",
    enabled: false,
    format: SubtitleFormat.IMSC,
  }
);
```

The `TimedTextPlugin` is created together with a list of parsers and a list of renderers that is required to parse and render the subtitles to be displayed.
There are also `rendererSettings` and `parserSettings` specific settings available for the renderers and parsers.

Subtitles can then be added to the plugin, and the subtitle format have to match the format of the parser and renderer.

### Subtitle formats
| Format 	| Parser       	| Renderer       	|
|--------	|--------------	|----------------	|
| IMSC   	| IMSCParser   	| IMSCRenderer   	|
| VTT    	| WebVTTParser 	| WebVTTRenderer 	|
| SCC    	| SCCParser    	| SCCRenderer    	|
| SRT    	| SRTParser    	| SRTRenderer    	|
| STL    	| STLParser    	| STLRenderer    	|
| TIMESPAN| TimespanParser| TimespanRenderer|




## Summary

| Plugin                 | Progressive | ABR | Cutlist |
| ---------------------- | ----------- | --- | ------- |
| HotkeyPlugin           | ✓           | ✓   | ✓       |
| AudioScrubPlugin       | ✓           | X   | ✓       |
| ChannelControlPlugin   | ✓           | ✓¹  | ✓       |
| DiscreteAudioPlugin    | ✓           | ✓   | ✓       |
| PointPlugin            | ✓           | ✓   | ✓       |
| SmoothTimeUpdatePlugin | ✓           | ✓   | ✓       |
| SubtitlePlugin         | ✓           | ✓   | ✓       |
| ImscSubtitlePlugin     | ✓           | ✓   | ✓       |
| VttSubtitlePlugin      | ✓           | ✓²  | ✓       |
| TimedTextPlugin        | ✓           | ✓   | X       |

1. [Encoding restrictions for ABR](##encoding-restrictions-for-abr)
2. The `VttSubtitlePlugin` will not work with the HlsPlayer while also using its built in vttSubtitles methods. I.e. in that case, a decision has to be made whether to use hls vttSubtitles or external vttSubtitles.

← [Basics](BASICS.md)
