# Introduction

This tutorial will showcase the Gonio- and Correlation Meter components that is part of Accurate Player SDK.

A goniometer is a tool used to identify phase differences between two audio signals.
The practical use is to give you visual feedback of the stereo image. A mono signal will display as a vertical line, a phase inverted stereo signal (each side 180 degrees opposite) would display as a horizontal line.
A hard-panned signal will be a diagonal line. A typical stereo signal is a squiggle all over but definitely vertically centered.
A stereo signal that has been extremely widened or has phase issues will be a squiggle horizontally centered.

A correlation meter, essentially, displays the changing phase relationships between the left and right channels of a stereo signal, 
but in more simple terms, it can be thought of as indicating the state of a stereo signal’s mono compatibility.
The meter is fed the left and right audio channels and displays the integrated phase correlation between them.
So in simple terms, a correlation meter reading of +1 means that it is receiving a (dual) mono signal, whereas a reading of zero means a fully wide stereo image. Normal stereo material will generally display a fluctuating reading between +1 and zero. If the reading trends towards the +1 end the image is getting narrower and heading towards mono, while trends towards zero indicate a much wider image.
Finally, in the case of a dual-mono signal where one channel is polarity-inverted, the meter will indicate at the negative extreme of the scale.

The components are built on the Web Components standards to create custom elements that are framework agnostic. That means that you can use the components with
your favorite framework. With that said, some frameworks have some interoperability issues, but, in most cases, it is possible to work around those
issues. [Custom Element Everywhere](https://custom-elements-everywhere.com/) test frameworks and lists how well they interoperate if you are interested in learning more about
that.

We use [lit-html](https://lit-html.polymer-project.org/) in the html examples in this tutorial, but it should be fairly easy to translate to the framework of your liking.
Here's a brief explanation of the syntax, consult the [lit-html documentation](https://lit-html.polymer-project.org/guide/writing-templates) for further explanation:

Example lit-html syntax:

```html
<input
  class="${myClass}"
  ?disabled="${isDisabled}"
  .value="${value}"
  @input="${myCallbackFunction}"
/>
```

| Syntax                           | Explanation               |
| -------------------------------- | ------------------------- |
| `class="${myClass}"`             | Attribute binding         |
| `?disabled="${isDisabled}"`      | Boolean attribute binding |
| `.value="${value}"`              | Property binding          |
| `@input="${myCallbackFunction}"` | Register event listener   |

We also want to mention that we do not provide all the CSS and surrounding code in the code snippets that follow, we only include the most important parts. For a
complete picture, checkout the source code in this repository, more specifically `index.html` and `src/Application.ts`.

Our hope is that this tutorial will showcase how easy it is to set up the gonio- and correlation meter components. Let's dig in!

# Basics

First off we need to install the accurate player components library to get access to the components.

```bash
pnpm add @accurate-player/accurate-player-components-external --dev
```

Next, we need to import the `ApGoniometer`/`ApCorrelationMeter` component and the `ChannelControlPlugin` which is a requirement for the these components to work:


```typescript
import { ApGoniometer } from "@accurate-player/accurate-player-components-external";
import { ApCorrelationMeter } from "@accurate-player/accurate-player-components-external";

import { ChannelControlPlugin } from "@accurate-player/accurate-player-plugins";
```

We also need to include a css theme file, there are two predefined themes that can be imported from `dist/styles/theme-dark.css` or `dist/styles/theme-light.css`
respectively. You can of course also include your theme colors by copying one of the included theme files and alter the color variables to your liking. If you use a bundler like webpack or similar you
can probably do something like this to import it from `node_modules`:

```typescript
import "~@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
```

Now we can include the gonio- and correlation meters and register them with the [CustomElementsRegistry](https://developer.mozilla.org/en-US/docs/Web/API/CustomElementRegistry) and instantiate the `ChannelControlPlugin`:

```typescript
ApGoniometer.register(); // <ap-goniometer />
ApCorrelationMeter.register(); // <ap-correlation-meter />

const channelControlPlugin = new ChannelControlPlugin(player);
```

This will make the components registered for the tags `<ap-goniometer />` and `<ap-correlation-meter />`. Component tag names are set to the class name converted to lowercase kebab case per default. Now we can use this tag in our html. We need to provide a player reference to the component, and then we are good to go:

```html
<ap-goniometer .player="${player}" .sensitivity="${1}"></ap-goniometer>
<ap-correlation-meter .player="${player}" vertical></ap-correlation-meter>
```


This is how it looks!

![Gonio- and Correlation meter](docs/media/sample1.gif "Gonio- and correlationmeter")


# Common settings
There are a some properties that are common for both the gonio- and the correlation meter.

The `fadeOutDuration` property determines the time in milliseconds that it will take for older indicator positions to fade out.
The default value is 500 ms.

```html
<ap-goniometer .player="${this.player}" .fadeOutDuration="${100}"></ap-goniometer>
```

The line above will result in this look:

![Fadeout 100 ms](docs/media/fadeout-100.gif "Fadeout 100 ms")

```html
<ap-goniometer .player="${this.player}" .fadeOutDuration="${1000}"></ap-goniometer>
```

The line above will result in this look:


![Fadeout 1000 ms](docs/media/fadeout-1000.gif "Fadeout 1000 ms")

The `leftChannelIndex` (default 0) and `rightChannelIndex` (default 1) properties can be used to specify which index to use
for the left and right channel if the defaults can not be used. 

```html
<ap-correlation-meter .player="${this.player}" .leftChannelIndex="${2}" .rightChannelIndex="${3}"></ap-correlation-meter>
```


Per default the gonio- and correlation meters will read audio levels from the loaded video file.
In order to read audio levels from a different source, e.g. a discrete audio track,
you set the `source` property to the id of the discrete audio track.

```html
<ap-correlation-meter .player="${this.player}" .source="${this.sourceId}"></ap-correlation-meter>
```


## Goniometer settings

The `sensitivity` property can be increased/decreased to affect the scale of the lissajous figure.
The default value is 1.

![Goniometer sensitivity](docs/media/goniometer-sensitivity.png "Goniometer sensitivity")

## Correlation meter settings

The `vertical` property can be used to shift between a vertical and horizontal layout:


```html
<ap-correlation-meter .player="${player}" .vertical="${false}"></ap-correlation-meter>
```

![Correlation meter horizontal](docs/media/correlation-horizontal.png "Correlation meter hoizontal")


# Styling

## Goniometer

You can change the style of the goniometer by setting the `goniometerStyle` property:

```typescript
private style: GoniometerStyle = {
  backgroundColor: "midnightblue",
  axisLineColor: "white",
  axisLabelColor: "royalblue",
  primaryColor: "green",
  secondaryColor: "purple"
};
```

```html
<ap-goniometer .player="${this.player}" .goniometerStyle="${this.style}"></ap-goniometer>
```

This will result in this look:

![goniometermeter styling](docs/media/goniometer-style.gif "Goniometer styling")


## Correlation meter

These are the CSS variables that can be set for the correlation meter and their default values.

| CSS Variable                              | Description                                      | Default Value 
|-------------------------------------------|--------------------------------------------------| ---------------
| `--ap-correlation-meter-background-color` | The background color                             | --AP-BACKGROUND-BRAND
| `--ap-correlation-meter-axis-line-color`  | The color of the axis lines                    | --AP-BACKGROUND-INTERACTIVE-1
| `--ap-correlation-meter-axis-label-color` | The color of the axis labels               | --AP-FOREGROUND-2
| `--ap-correlation-meter-indicator-color`  | The color of the indicator for the current value | --AP-PRIMARY


Set the variables in CSS like this:

```css
ap-correlation-meter {
  --ap-correlation-meter-background-color: black;
  --ap-correlation-meter-axis-line-color: darkgreen;
  --ap-correlation-meter-axis-label-color: lime;
  --ap-correlation-meter-indicator-color: cyan;
}
```

And this is what it will look like:

![Correlation meter styling](docs/media/correlation-style.gif "Correlation meter styling")
