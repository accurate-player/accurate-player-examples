import { css, html, LitElement, PropertyValues, TemplateResult, } from "lit";
import { customElement, property, query } from "lit/decorators.js";
import { ProgressivePlayer as AccuratePlayer } from "@accurate-player/accurate-player-progressive";

import { ApGoniometer, ApCorrelationMeter } from "@accurate-player/accurate-player-components-external";

import { AudioScrubPlugin, ChannelControlPlugin, DiscreteAudioPlugin, HotkeyPlugin, SmoothTimeUpdatePlugin, } from "@accurate-player/accurate-player-plugins";

import "@accurate-player/accurate-player-controls";

import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";

/*
 * Component library components need to be registered with the customElements registry
 * before they can be used (as they are WebComponents, read more: https://developer.mozilla.org/en-US/docs/Web/Web_Components).
 *
 * Calling register() will call customElements.define() under the hood.
 *
 */
ApGoniometer.register();
ApCorrelationMeter.register();

/**
 * Application root element.
 */
@customElement("av-application")
export class Application extends LitElement {
  static styles = css`
    :host {
      background: var(--AP-BACKGROUND-BODY);
      display: flex;
      padding: 20px;
      flex-direction: column;
      width: calc(100% - 40px);
      height: calc(100% - 40px);
    }

    div.container-video {
      display: flex;
      flex: 1;
      justify-content: center;
      margin-bottom: 20px;
      max-height: 50vh;
      position: relative;
    }

    div.grid {
      display: flex;
      flex-direction: column;
      flex: 1 1 100%;
    }

    div.components {
      position: relative;
      display: flex;
      flex-direction: row;
    }

    ap-goniometer {
      width: 400px;
      height: 400px;
      margin-right: 1rem;
    }
  `;

  @query("#player")
  videoElement: HTMLVideoElement | undefined;

  @query("apc-controls")
  controls: any;

  @property()
  private player: AccuratePlayer | undefined;

  private audioScrubPlugin: AudioScrubPlugin;
  private channelControlPlugin: ChannelControlPlugin;
  private discreteAudioPlugin: DiscreteAudioPlugin;
  private hotkeyPlugin: HotkeyPlugin;
  private smoothTimeUpdatePlugin: SmoothTimeUpdatePlugin;

  protected firstUpdated(changedProperties: PropertyValues) {
    super.firstUpdated(changedProperties);
    this.initializeAccuratePlayer();
  }

  async initializeAccuratePlayer() {
    if (this.videoElement == null) {
      console.warn(
        "Failed to initialize Accurate Player: video element could not be found."
      );

      return;
    }
    if (window.LICENSE_KEY == null) {
      console.warn(
        "Failed to initialize Accurate Player: no license key found, did you provide one in license-key.js?"
      );

      return;
    }
    if (this.controls == null) {
      console.warn(
        "Failed to initialize Accurate Player: could not find <apc-controls />"
      );

      return;
    }

    this.player = new AccuratePlayer(this.videoElement, window.LICENSE_KEY);
    this.audioScrubPlugin = new AudioScrubPlugin(this.player);
    this.channelControlPlugin = new ChannelControlPlugin(this.player);
    this.discreteAudioPlugin = new DiscreteAudioPlugin(this.player);
    this.hotkeyPlugin = new HotkeyPlugin(this.player);
    this.smoothTimeUpdatePlugin = new SmoothTimeUpdatePlugin(this.player);

    // Add audio tracks
    this.player.api.loadVideoFile({
      label: "Tears of Steel",
      src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/original/tears_of_steel.mp4",
      enabled: true,
      channelCount: 2,
      frameRate: {numerator: 25, denominator: 1},
      dropFrame: false,
    });

    const settings = {
      saveLocalSettings: true, // Enables storing of simple settings as volume, timeFormat and autoHide.
    };
    // The Polymer 3.0 project is currently written in Javascript. Will be converted to TypeScript with typings shortly!
    this.controls.init(this.videoElement, this.player, settings);
  }

  render(): TemplateResult {
    return html`
      <div class="grid">
        <div class="container-video">
          <video id="player" crossorigin="anonymous"></video>
          <apc-controls></apc-controls>
        </div>
        <div class="components">
          <ap-goniometer .player="${this.player}" .sensitivity="${1}"></ap-goniometer>
          <ap-correlation-meter .player="${this.player}" vertical></ap-correlation-meter>
        </div>
      </div>
    `;
  }
}

declare global {
  interface Window {
    LICENSE_KEY: string;
  }
}
