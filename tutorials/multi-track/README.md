# Introduction

Browsers (at the time of writing) does not support playback of multi-track
mp4 files, they will only play the first video, the first audio track and no subtitle tracks. Some browsers support the [audioTracks](https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/audioTracks) api, but it is also limited to play one audio track at the time and requires you to enable experimental flags in the browser. So what you normally need to do is to conform your video material into multiple files to be able to play all tracks. This is a process that usually is performed when ingesting the material, but that might not be feasible to do on large already existing archives for example. This tutorial will showcase how you can skip that step and play single file multi-track mp4 directly in the browser using Accurate Player SDK.

Before going through this tutorial we recommend that you learn about the [player and plugins](../player/README.md). We will assume that you know about those topics going
forward.

Let's dig in!

# Basics

First off we need to install the `@accurate-player/mse` to get access to the `TrackExtractor` utility, the main piece of the solution to play multi-track files. We also
need to install the core-, plugins- and progressive- packages to access to the `ProgressivePlayer`, `DiscreteAudioPlugin` and the `TimedTextPlugin` to load the extracted video, audio and subtitle tracks.

```bash
pnpm add @accurate-player/mse \
         @accurate-player/accurate-player-core \
         @accurate-player/accurate-player-plugins \
         @accurate-player/accurate-player-progressive --dev

```

Next, we need to import the tools we need:

```typescript
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";
import {
  IMSCParser,
  IMSCRenderer,
  SCCParser,
  SCCRenderer,
  TimedTextPlugin,
  WebVTTParser,
  WebVTTRenderer,
} from "@accurate-player/accurate-player-plugins";
import { TrackExtractor } from "@accurate-player/mse";
```

We also need a reference to the `<video>` element, which we'll call `videoElement` going forward. How you get a reference to an element depends on the framework you use, in the tutorial example code we use `lit`s `@query` decorator, this is what it essentially does:

```typescript
const videoElement = document.querySelector("video");
```

Now we're ready to load a multi-track file by instantiating the `TrackExtractor` and calling `TrackExtractor.getTracks(videoElement)` which returns a promise that will
resolve when the tracks are ready to be loaded:

```typescript
const trackExtractor = new TrackExtractor("https://example.com/multitrack.mp4");
const {
  videoFiles,
  discreteAudioTracks,
  timedTextReferences,
} = await trackExtractor.getTracks(videoElement);
```

`getTracks` returns `VideoFile`, `DiscreteAudioTrack` and `TimedTrackReference` objects that can be loaded in the `ProgressivePlayer`, `DiscreteAudioPlugin` and `TimedTextPlugin` as normal. Here we load the first video track found and all audio and subtitle tracks, we enable the first audio track by default:

```typescript
const player = new ProgressivePlayer(videoElement, LICENSE_KEY);
const discreteAudioPlugin = new DiscreteAudioPlugin(player);
const timedTextPlugin = new TimedTextPlugin(
  player,
  [WebVTTParser, SCCParser, IMSCParser],
  [WebVTTRenderer, SCCRenderer, IMSCRenderer]
);

player.api.loadVideoFile(videoFiles[0]);
discreteAudioPlugin.initDiscreteAudioTracks(
  discreteAudioTracks.map((audioTrack, index) => {
    return {
      enabled: index === 0,
      track: audioTrack,
    };
  })
);
timedTextPlugin.add(...timedTextReferences);

```

This concludes the basics of how it works!

![Basic multi-track playback](docs/media/basic.jpg "Basic multi-track playback")

# Options

The `TrackExtractor` accepts options as a second argument. These can be used to turn on verbose logging for debugging purposes, tune the download segment size and more. Example:

```typescript
const trackExtractor = new TrackExtractor(
  "https://example.com/multitrack.mp4",
  {
    verbose: "debug", // don't use verbose logging in production!
    segmentDownloadChunkSizeMinBytes: 1024 * 1024 * 2, // 2mb
    segmentDownloadChunkSizeMinBytes: 1024 * 1024 * 10, // 10mb
  }
);
```

The full list of options is [available in the reference documentation](https://accurate-player.gitlab.io/accurate-player-examples/api/mse/interfaces/TrackExtractorOptions.html) for the MSE package.

**Note**: We recommend that you set `verbose` to `debug` and include the logs in error reports to the Accurate Player team as it will help us identify
bugs easier.

# Tips & Tricks

- You can modify the objects returned from `getTracks(videoElement)` before passing them to the `ProgressivePlayer`, `DiscreteAudioPlugin` and `TimedTextPlugin` to change the labels of the tracks (or other properties) for example:

  ```typescript
  const {
    videoFiles,
    discreteAudioTracks,
    timedTextReferences,
  } = await trackExtractor.getTracks(videoElement);
  videoFiles[0].label = "My custom video label";
  discreteAudioTracks[0].label = "My custom audio track label";
  timedTextReferences[0].label = "My custom subtitle label";
  ```

- The `DiscreteAudioPlugin` disables audio tracks that take a long time to load, this behaviour depends on the `frozenTimeout` option. You may want to adjust this limit if, for example, you have high bitrate content that require longer buffering duration.

  ```typescript
  const discreteAudioPlugin = new DiscreteAudioPlugin(player, {
    frozenTimeout: 1000 * 60, // 1 minute
  });
  ```

  Read more in the [DiscreteAudioPlugin documentation](https://accurate-player.gitlab.io/accurate-player-examples/api/plugins/interfaces/DiscreteAudioSettings.html#frozenTimeout)

- Use `SeekStrategyType.RegularSeek` to make sure downloads are aborted on seek. `SeekStrategyType.QueuedSeek` (default) will not initiate a new seek until the current one is done.

  ```typescript
  player = new ProgressivePlayer(master, LICENSE_KEY, {
    seekStrategy: SeekStrategyType.RegularSeek,
  });
  ```


## CORS Considerations

The multi-track functionality needs to be able to read the `Content-Range` header, when fetching the media, to be able to function properly.
This header is not exposed by browsers by default when doing cross-origin requests.

You need to configure your media storage to set the `Access-Control-Expose-Headers` header to include `Content-Range`.
If you are using [AWS S3](https://aws.amazon.com/s3/) you can do so by specifying this in the [CORS configuration](https://docs.aws.amazon.com/AmazonS3/latest/userguide/enabling-cors-examples.html):

```json {6} title="Example S3 CORS configuration"
[
  {
    "AllowedHeaders": ["*"],
    "AllowedMethods": ["GET", "HEAD"],
    "AllowedOrigins": ["https://*.accurate.video"],
    "ExposeHeaders": ["Content-Range"],
    "MaxAgeSeconds": 3000
  }
]
```

# Known limitations

- This feature is **specifically** targeting the [MP4 file format (.mp4)](https://en.wikipedia.org/wiki/MP4_file_format) as defined by MPEG-4 Part 14
  - [QuickTime file format (.mov)](https://en.wikipedia.org/wiki/QuickTime_File_Format) _may_ work in some cases, as long as the codecs are supported (see below). We recommend that you try it out to verify.
- The video- and audio codec support is limited to what the [Media Source Extensions API](https://developer.mozilla.org/en-US/docs/Web/API/Media_Source_Extensions_API) supports. That in turn is dependent on what the browser vendors have implemented.
  - Usage of H.264 video codec, and AAC audio codec is a common baseline that most browser vendors support.
  - To know if your media can be played with this feature, we recommend that you try it out and see for yourself as it is so dependent on browser support.
- The supported subtitle track formats are:
  - WebVTT (`wvtt`)
  - MPEG-4 Timed Text (`tx3g`)
  - Simple text (`mett`, `sbtt`, `stxt`)
  - TTML/IMSC (`metx`, `stpp`)
  - SCC (`c608`, `c708`)

# FAQ

 - Q: Why do I get typescript type errors like these when using strict mode?
   ```
    Could not find a declaration file for module 'mp4box'. 'node_modules/mp4box/dist/mp4box.all.js' implicitly has an 'any' type. Try `npm i --save-dev @types/mp4box` if it exists or add a new declaration (.d.ts) file containing `declare module 'mp4box';`
   ```
   A: This is because the `mp4box` library (a dependency for `@accurate-player/mse`) does not have typescript typings defined. There's an open issue on github that explains how you can define typings by yourself for `mp4box`: https://github.com/gpac/mp4box.js/issues/233 
