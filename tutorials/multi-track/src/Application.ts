import { css, html, LitElement, PropertyValues, TemplateResult } from "lit";
import { customElement, property, query } from "lit/decorators.js";
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";

import {
  AudioScrubPlugin,
  ChannelControlPlugin,
  DiscreteAudioPlugin,
  HotkeyPlugin,
  SmoothTimeUpdatePlugin,
  TimedTextPlugin,
  WebVTTParser,
  SCCParser,
  IMSCParser,
  WebVTTRenderer,
  SCCRenderer,
  IMSCRenderer,
} from "@accurate-player/accurate-player-plugins";
import { TrackExtractor } from "@accurate-player/mse";

import "@accurate-player/accurate-player-controls";
import { SeekStrategyType } from "@accurate-player/accurate-player-core";

/**
 * Application root element.
 */
@customElement("av-application")
export class Application extends LitElement {
  static styles = css`
    :host {
      background: #000;
      display: flex;
      padding: 20px;
      flex-direction: column;
      width: calc(100% - 40px);
      height: calc(100% - 40px);
    }

    div.container-video {
      display: flex;
      justify-content: center;
      position: relative;
      margin-bottom: 20px;
      align-self: center;
    }

    video {
      max-width: 100%;
      height: auto;
    }
  `;

  @query("#player")
  videoElement: HTMLVideoElement | undefined;

  @query("apc-controls")
  controls: any;

  @property()
  private player: ProgressivePlayer | undefined;

  private audioScrubPlugin: AudioScrubPlugin | undefined;
  private channelControlPlugin: ChannelControlPlugin | undefined;
  private discreteAudioPlugin: DiscreteAudioPlugin | undefined;
  private timedTextPlugin: TimedTextPlugin | undefined;
  private hotkeyPlugin: HotkeyPlugin | undefined;
  private smoothTimeUpdatePlugin: SmoothTimeUpdatePlugin | undefined;

  protected firstUpdated(changedProperties: PropertyValues) {
    super.firstUpdated(changedProperties);
    this.initializeAccuratePlayer();
  }

  async initializeAccuratePlayer() {
    if (this.videoElement == null) {
      console.warn(
        "Failed to initialize Accurate Player: video element could not be found."
      );

      return;
    }
    if (window.LICENSE_KEY == null) {
      console.warn(
        "Failed to initialize Accurate Player: no license key found, did you provide one in license-key.js?"
      );

      return;
    }
    if (this.controls == null) {
      console.warn(
        "Failed to initialize Accurate Player: could not find <apc-controls />"
      );

      return;
    }

    // Create player and plugins
    this.player = new ProgressivePlayer(this.videoElement, window.LICENSE_KEY, {
      seekStrategy: SeekStrategyType.RegularSeek,
    });
    this.discreteAudioPlugin = new DiscreteAudioPlugin(this.player);
    this.timedTextPlugin = new TimedTextPlugin(
      this.player,
      [WebVTTParser, SCCParser, IMSCParser],
      [WebVTTRenderer, SCCRenderer, IMSCRenderer]
    );
    this.hotkeyPlugin = new HotkeyPlugin(this.player);

    // Init controls
    this.controls.init(this.videoElement, this.player);

    // Create the track extractor
    const trackExtractor = new TrackExtractor(
      "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/liden/ToS-multiaudio_fixed_w_subs.mp4"
    );
    // Get the VideoFile and DiscreteAudioTrack objects that can be loaded in the progressive player and discrete audio plugin
    const { videoFiles, discreteAudioTracks, timedTextReferences } =
      await trackExtractor.getTracks(this.videoElement);

    // Load a video file and all audio tracks
    this.player.api.loadVideoFile(videoFiles[0]);
    this.discreteAudioPlugin.initDiscreteAudioTracks(
      discreteAudioTracks.map((audioTrack, index) => {
        return {
          enabled: index === 0,
          track: audioTrack,
        };
      })
    );
    this.timedTextPlugin.add(...timedTextReferences);
  }

  render(): TemplateResult {
    return html`
      <div class="container-video">
        <video id="player" crossorigin="anonymous"></video>
        <apc-controls></apc-controls>
      </div>
    `;
  }
}

declare global {
  interface Window {
    LICENSE_KEY: string;
  }
}
