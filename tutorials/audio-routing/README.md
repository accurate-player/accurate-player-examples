# Introduction

This tutorial will showcase the Audio Routing component that is part of Accurate Player SDK.

We will first showcase how to display the audio routing component for the muxed audio of the video, then we will look into how this can be expanded show audio levels of discrete audio files.

The components are built on the Web Components standards to create custom elements that are framework agnostic. That means that you can use the components with
your favorite framework. With that said, some frameworks have some interoperability issues, but, in most cases, it is possible to work around those
issues. [Custom Element Everywhere](https://custom-elements-everywhere.com/) test frameworks and lists how well they interoperate if you are interested in learning more about
that.

We use [lit-html](https://lit-html.polymer-project.org/) in the html examples in this tutorial, but it should be fairly easy to translate to the framework of your liking.
Here's a brief explanation of the syntax, consult the [lit-html documentation](https://lit-html.polymer-project.org/guide/writing-templates) for further explanation:

Example lit-html syntax:

```html
<input
  class="${myClass}"
  ?disabled="${isDisabled}"
  .value="${value}"
  @input="${myCallbackFunction}"
/>
```

| Syntax                           | Explanation               |
| -------------------------------- | ------------------------- |
| `class="${myClass}"`             | Attribute binding         |
| `?disabled="${isDisabled}"`      | Boolean attribute binding |
| `.value="${value}"`              | Property binding          |
| `@input="${myCallbackFunction}"` | Register event listener   |

We also want to mention that we do not provide all the CSS and surrounding code in the code snippets that follow, we only include the most important parts. For a
complete picture, checkout the source code in this repository, more specifically `index.html` and `src/Application.ts`.

Our hope is that this tutorial will showcase how easy it is to set up the audio routing component. Let's dig in!

# Basics

First off we need to install the accurate player components library to get access to the components.

```bash
pnpm add @accurate-player/accurate-player-components-external --dev
```

Next, we need to import the audio routing component and the `ChannelControlPlugin` which is a requirement for the audio routing component to work:

```typescript
import { ApAudioRoutingBasic } from "@accurate-player/accurate-player-components-external";

import { ChannelControlPlugin } from "@accurate-player/accurate-player-plugins";
```

We also need to include a css theme file, there are two predefined themes that can be imported from `dist/styles/theme-dark.css` or `dist/styles/theme-light.css`
respectively. You can of course also include your theme colors by copying one of the included theme files and alter the color variables to your liking. If you use a bundler like webpack or similar you
can probably do something like this to import it from `node_modules`:

```typescript
import "~@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
```

Now we can include the audio routing component and register it with the [CustomElementsRegistry](https://developer.mozilla.org/en-US/docs/Web/API/CustomElementRegistry) and instantiate the `ChannelControlPlugin`:

```typescript
ApAudioRoutingBasic.register(); // <ap-audio-routing-basic />

const channelControlPlugin = new ChannelControlPlugin(player);
```

This will make the component registered for the tag `<ap-audio-routing-basic />`. Component tag names are set to the class name converted to lowercase kebab case per default. Now we can use this tag in our html. We need to provide a player reference to the component, and then we are good to go:

```html
<ap-audio-routing-basic
 .player="${player}">
</ap-audio-routing-basic>
```

Let's see what it looks like!

![Audio Routing Basic](docs/media/audio-routing-basic.png "Audio Routing Basic")

The audio routing component will list all channels available in the muxed audio, and automatically display the correct number of outputs depending on your current system.

# Settings

There are a few elements of the audio routing component that are configurable.

## Header label

The header label is configurable by setting the property `headerLabel`.

```html
<ap-audio-routing-basic 
  .player="${this.player}"
  .headerLabel="Audio routing"
>
</ap-audio-routing-basic>
```

![Audio Routing Basic Header Label](docs/media/audio-routing-basic-header.png "Audio Routing Basic Header Label")

## Output labels

The output labels are configurable by setting the property `outputLabels`.

```html
<ap-audio-routing-basic
  .player="${this.player}"
  .outputLabels="${['A', 'B']}"
>
</ap-audio-routing-basic>
```

![Audio Routing Basic Output Labels](docs/media/audio-routing-basic-output.png "Audio Routing Basic Output Labels")

## Channel labels

The labels of the channels can be configured in the video and/or the discrete audio object by setting the `channelLabels` property.

### Video audio object

```ts
this.player.api.loadVideoFile({
  label: "Sintel (5.1)",
  src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/original/sintel-2048-surround.mp4",
  enabled: true,
  channelCount: 6,
  frameRate: { numerator: 24, denominator: 1 },
  dropFrame: false,
  channelLabels: ["A", "B", "C", "D", "E"],
});
```

### Discrete audio object

```ts
this.discreteAudioPlugin.initDiscreteAudioTracks([
  {
    track: {
      id: "example-track",
      channelCount: 2,
      label: "Example Track",
      src: "/sample.mp3",
      channelLabels: ["One", "Two"],
    },
    enabled: true,
  },
]);
```

![Audio Routing Basic Channel Labels](docs/media/audio-routing-basic-channel-labels.png "Audio Routing Basic Channel Labels")

## Track filter
By setting the property `trackFilter` the tracks about to be displayed in the audio routing component can be filtered before finally being displayed. By default, all audio tracks are displayed.

```ts
filterTracks(tracks: AudioRoutingTrack[]) {
  return tracks.filter((track) => track.title !== "Stereo Track");
}
```
```html
<ap-audio-routing-basic 
  .player="${this.player}"
  .trackFilter="${this.filterTracks}"
>
</ap-audio-routing-basic>
```


# Plugins

With the basic setup done we will take a look at the optional plugins that interact with the audio routing component.

## DiscreteAudioPlugin

The `DiscreteAudioPlugin` adds support for adding external or discrete audio tracks to the player. The plugin synchronizes audio and video playback.

When adding more discrete audio tracks the discrete audio tracks are added to the audio routing basic component.

```ts
import { DiscreteAudioPlugin } from "@accurate-player/accurate-player-plugins";

const discreteAudioPlugin = new DiscreteAudioPlugin(player);

// Add audio tracks
// Note. if ChannelControlPlugin is to be used, channelCount of each track must be set.
discreteAudioPlugin.initDiscreteAudioTracks([
  {
    track: {
      id: "stereo-track",
      channelCount: 2,
      label: "Stereo Track",
      src: "/sample.mp3",
    },
    enabled: true,
  },
]);
```

## ChannelControlPlugin
The channel control plugin is required in order to use the audio routing component.

### Default routing
The channel control plugin controls the default routing and can be edited by adding the `channelRouter` argument to the `ChannelSettings` of the channel control plugin.

```ts
channelRouter(channelCount: number, outputCount: number) {
  let newRouteMap: number[][] = [];
  for (let i = 0; i < channelCount; i++) {
    newRouteMap[i] = [];
    for (let x = 0; x < outputCount; x++) {
      newRouteMap[i][x] = x === 0 ? 1 : 0;
    }
  }
  return newRouteMap;
}

this.channelControlPlugin = new ChannelControlPlugin(
  this.player,
  {channelRouter: this.channelRouter}
);

```

![Audio Routing Basic Channel Route](docs/media/audio-routing-basic-channel-route.png "Audio Routing Basic Channel Route")
