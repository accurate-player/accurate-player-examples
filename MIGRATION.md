# Migrating to 8.1.0
In this version we introduce a new plugin `TimedTextPlugin` that adds supports for parsing and rendering multiple subtitle formats. With that, we deprecate all our previous subtitle plugins.

The `TrackExtractor` is also modified to extract general `TimedTextReferences` instead of specific subtitle objects.  

The examples below demonstrates how to migrate each individual plugin and also the `TrackExtractor`. Note that even if you use multiple subtitle plugins you will only need one single TimedTextPlugin instance. Just add more parsers and renderers as needed.

## VttSubtitlePlugin

### Before
```typescript
// Add subtitles
const vttSubtitlePlugin = new VttSubtitlePlugin(player);
const subtitle = {
  label: "English",
  src: "http://example.com/subtitle.vtt",
  enabled: false,
};
vttSubtitlePlugin.createVttSubtitles([subtitle]);

// Toggle subtitle
vttSubtitlePlugin.toggleVttSubtitle(subtitle);
```
### After
```typescript
// Add subtitles
const timedTextPlugin = new TimedTextPlugin(
    player,
    [WebVTTParser],
    [WebVTTRenderer],
  );
timedTextPlugin.add(
  {
    id: "vtt-sub-1",
    label: "English",
    src: "http://example.com/subtitle.vtt",
    enabled: false,
    format: SubtitleFormat.VTT,
  }
);

// Toggle subtitle
timedTextPlugin.toggle("vtt-sub-1");
```

## SccSubtitlePlugin

### Before
```typescript
const sccSubtitlePlugin = new SccSubtitlePlugin(player);

// Add subtitles
sccSubtitlePlugin.add([
  {
    id: "sv",
    label: "Swedish",
    language: "sv-SE",
    kind: "captions",
    src: "https://example.com/movie_sv.scc",
    enabled: true
  }
]);

```

### After
```typescript
// Add subtitles
const timedTextPlugin = new TimedTextPlugin(
    player,
    [SCCParser],
    [SCCRenderer],
    {
      rendererSettings: {
        sccEnableDefaultCueStyles: true,
      },
    }
  );
timedTextPlugin.add(
  {
    id: "sv",
    label: "Swedish",
    src: "https://example.com/movie_sv.scc",
    enabled: true,
    format: SubtitleFormat.SCC,
  }
);
```

## ImscSubtitlePlugin

### Before
```typescript
// Add subtitles
const imscSubtitlePlugin = new ImscSubtitlePlugin(player);
const imscSubtitles = await imscSubtitlePlugin.load([
  {
    src: "http://example.com/subtitle.xml",
    id: "subtitle-ger"
  }
]);

// Switch subtitle
imscSubtitlePlugin.setActiveImscSubtitle("subtitle-ger");

// Temporarily disable subtitles
imscSubtitlePlugin.deactivateImscSubtitles();

// Re-enable subtitles
imscSubtitlePlugin.activateImscSubtitles();
```

### After
```typescript
// Add subtitles
const timedTextPlugin = new TimedTextPlugin(
  player,
  [IMSCParser],
  [IMSCRenderer],
);
timedTextPlugin.add(
  {
    id: "imsc-sub-1",
    label: "German",
    src: "http://example.com/subtitle.vtt",
    enabled: true,
    format: SubtitleFormat.IMSC,
  }
);

// Switch subtitle not applicable anymore

// Disable subtitle
timedTextPlugin.disable("imsc-sub-1");

// Enable subtitle
timedTextPlugin.enable("imsc-sub-1");
```

## TrackExtractor

### Before

```typescript
const trackExtractor = new TrackExtractor("https://example.com/multitrack.mp4");
const {
  videoFiles,
  vttSubtitles,
  imscSubtitles,
  sccSubtitles,
} = await trackExtractor.getTracks(videoElement);
const player = new ProgressivePlayer(videoElement, LICENSE_KEY);
const vttSubtitlePlugin = new VttSubtitlePlugin(player);
const imscSubtitlePlugin = new ImscSubtitlePlugin(player);
const sccSubtitlePlugin = new SccSubtitlePlugin(player);

player.api.loadVideoFile(videoFiles[0]);
vttSubtitlePlugin.createVttSubtitles(vttSubtitles);
imscSubtitlePlugin.load(imscSubtitles);
sccSubtitlePlugin.add(sccSubtitles);
```

### After

```typescript
const trackExtractor = new TrackExtractor("https://example.com/multitrack.mp4");
const { videoFiles, timedTextReferences } = await trackExtractor.getTracks(videoElement);
const player = new ProgressivePlayer(videoElement, LICENSE_KEY);
timedTextPlugin = new TimedTextPlugin(
  player,
  [WebVTTParser, SCCParser, IMSCParser],
  [WebVTTRenderer, SCCRenderer, IMSCRenderer]
);

player.api.loadVideoFile(videoFiles[0]);
timedTextPlugin.add(...timedTextReferences);
```


# Migrating from 7.x to 8.x

## CutlistPlayer
Moved `Cut`, `CutlistPlayerEventName`, `CutChangedEvent`, `PreviewChangedEvent` from `@accurate-player/accurate-player-cutlist` to `@accurate-player/accurate-player-core`.

# Migrating from 6.x to 7.x

## VideoFile and DiscreteAudioTrack
Very small API change that only affects the property `updateSrc` of these objects. The signature has changed from `updateSrc(videoFile: VideoFile)` to `updateSrc(src: string, fileId: string)`.

# Migrating from 5.x to 6.x

## HlsPlayer
Minor API change due to updated dependencies. Removed `loadUrl` API, use `loadVideoFile` instead.

# Migrating from 4.5.0 to 5.0.0

## AudioScrubPlugin
- The `AudioScrubTrack` interface changed. The `nativeElement` property is now called `shadowElement`,
and a `trackId` and `masterElement` property were added
- `updateOffset` and `unregisterTracks` now takes track ids or sources instead of html elements.
- `unregisterTracksFromSource` has been replaced by `unregisterTracks`

## ChannelControlPlugin
- the events emitted will now have the track id if available, otherwise fall back to src (previously they always contained the src 
  regardless)

# Migrating from 3.4.2 to 4.0.0

## Npm repository changed
We have migrated to Artifactory, meaning no more releases will be pushed to https://repo.codmill.se. When upgrading to 4.0.0
 you need an artifactory user with correct permissions.
 
Username and password will be provided to you by the Accurate Player team. Contact us if you have not recieved that information, so we can help you setup a new user. 
 
Login to npm repository 
> npm login adduser --registry=https://codemill.jfrog.io/codemill/api/npm/accurate-video/ --scope=@accurate-player

## getMediaInfo
`getMediaInfo` has been moved to a separate package called `@accurate-player/probe`

Add `@accurate-player/probe` as a dependency (using same version as `@accurate-player/accurate-player-core`).

Update import statement for `getMediaInfo` wherever it is used.
```
- import { getMediaInfo } from "@accurate-player/accurate-player-core";
+ import { getMediaInfo } from "@accurate-player/probe";
```

## DashPlayer removed
The deprecated DashPlayer is now removed. Use ABRPlayer instead.

# Migrating from 3.0.0 to 3.1.0

Nothing breaking this release, but previous DashPlayer is deprecated and should be replaced with new ABRPlayer.

## DashPlayer -> AbrPlayer

The new ABRPlayer is based on shaka-player just as DashPlayer, The API:s should be fairly similar, but we done some differences in the API needs to be considered.


### imports
```diff
 - import { DashPlayer } from "@accurate-player/accurate-player-dash";
 + import { AbrPlayer } from "@accurate-player/accurate-player-abr";
```

### Adaption sets. From index to id.

In DashPlayer we previously used the index of `getVideoRepresentations`, `getAudioTracks`, `getSubtitles` as identifier when 
changing representation. We also had a property "id", which did in some cases match the index, but not always, leading to confusion.

In the new AbrPlayer we have updated this so that the "id" property should be used as identifier instead.

```typescript
  /*
   * DashPlayer
   */
  const adaptations = player.api.getVideoRepresentations();

  // Setting the video representation with highest resolution i.e last
  player.api.setVideoRepresentation(adaptations.length -1);


  /*
   * AbrPlayer
   */
   const adaptations = player.api.getVideoRepresentations();

   // Setting the video rerpresentation with highest resolution, using id
   player.api.setVideoRepresentation(adaptations[adaptations.length -1].id);
```

### Removed method "configureKeyOs"

In the DashPlayer, we have a specific method to configure authentication to KeyOs DRM key server.
The result of this method is that requests to the key server have a "customdata" header with the approperate key.

In the new AbrPlayer we have keept the more general "registerRequestFilter", which allows to register custom network interceptors when requesting both licenses and segments. 

```typescript
  /*
   * DashPlayer
   */
  player.api.configureKeyOs("your-custom-data...");
 
  /*
   * AbrPlayer
   */
   import {
    ShakaPlayerRequest,
    ShakaPlayerRequestFilter,
    ShakaPlayerRequestType
     } from "@accurate-player/accurate-player-abr";

   // Define the network interceptor, adding the "customdata" header to license requests.
   const keyOsInterceptor: ShakaPlayerRequestFilter = (type: ShakaPlayerRequestType, request: ShakaPlayerRequest) => {
     if (type === ShakaPlayerRequestType.LICENSE) {
       request.headers["customdata"] = "your-custom-data....";
     }
   };
   // Register the interceptor to shakaplayer
   player.api.registerRequestFilter(keyOsInterceptor);
```

# Migrating from 2.3.0 to 3.0.0

## Channel Control Plugin

Refactored registration of master element when using the HLS or DASH player.
The streams manifest url is now used as identifier, _not_ the src property of the native video-element. 

### 2.3.0

```typescript
  const ccp = new ChannelControlPlugin(player);
  // Using native elements src as identifier.
  ccp.getChannelData(player.api.master.src);
```

### 3.0.0
```typescript
  const ccp = new ChannelControlPlugin(player);
  // Use the videoFiles src as identifier (will only diffe from native elements src when using Dash or Hls player). 
  ccp.getChannelData(player.api.getActiveFile().src);
```

# Migrating from 1.7.1 to 2.0.0

## Subtitle Plugin

Plugin has been renamed from SubtitlePlugin to VttSubtitlePlugin along with events, types, properties and functions.

- Events renamed
  - SubtitleEventType -> VttSubtitleEventType
  - SubtitleAddedEvent -> VttSubtitleAddedEvent
  - SubtitleRemovedEvent -> VttSubtitleRemovedEvent
  - SubtitleErrorEvent -> VttSubtitleErrorEvent
- Types renamed
  - Subtitle -> VttSubtitle
- Properties renamed
  - subtitles -> vttSubtitles 
- Functions renamed
  - createSubtitles -> createVttSubtitles
  - createSubtitle -> createVttSubtitle
  - toggleSubtitle -> toggleVttSubtitle
  - deleteSubtitle -> deleteVttSubtitle
  - deleteAllSubtitles -> deleteAllVttSubtitles


# Migrating from 1.5.x to 1.6.0

## Channel Control Plugin

The methods `isMuted` and `isSolo` are replaced with `getChannelMutedState`. A channel can have four states; *Playing*, 
*ExplicitMuted*, *ImplicitMuted* and *Solo*. The following snippet shows how to implement `isChannelSolo` and `isChannelMuted` in `1.6.0` compared to `1.5.x`.

### 1.5.x
```typescript
isChannelSolo(src: string, index: number): boolean {
  return channelControlPlugin.isChannelSolo(src, index);
}

isChannelMuted(src: string, index: number): boolean {
  return channelControlPlugin.isChannelMuted(src, index);
}
```

### 1.6.0
```typescript
**1.6.0**
isChannelSolo(src: string, index: number): boolean {
  return channelControlPlugin.getChannelMutedState(src, index) === MutedState.Solo;
}

isChannelMuted(src: string, index: number): boolean {
  const mutedState = channelControlPlugin.getChannelMutedState(src, index);
  return mutedState === MutedState.ImplicitMuted || mutedState === MutedState.ExplicitMuted;
}
```

# Migrating from 1.3.x to 1.4.0

There are two breaking changes that you need to be aware of:

* The selectors for the UI layer (accurate-player-controls) has updated its prefix from `ap-` to `apc-`.
* Toggling individual audio channels has moved from core to a separate plugin

## Accurate Player Controls

As mentioned in the introduction, the prefix has been changed due to internal collissions. The steps you need to take to update is the following.

* Change prefix of CSS-variables.
* Change the HTML element name.

**1.3.x**
```html
<style>
  --ap-controls-primary: #6280c0;
</style>
...
<ap-controls></ap-controls>
```

**1.4.x**
```html
<style>
  --apc-primary: #6280c0;
</style>
...
<apc-controls></apc-controls>
```

## Channel Control Plugin

Channel toggling features is moved from core and DiscreteAudioPlugin into ChannelControlPlugin.

**1.3.x**

The volume for individual channels within the master element were set by the api method `setChannels`.
```typescript
// Set channels for master element.
player.api.setChannels([0, 1]);
```

Individual channels within a discrete audio plugin used the equivalent method in the `DiscreteAudioPlugin`.
```typescript
const dap = new DiscreteAudioPlugin(player);
...

// If we go via the player api
player.api.plugins.apDiscreteAudioPlugin.setChannels(src, [0, 1]);

// If we keept the reference to plugin.
dap.setChannels(src, [0,1]);
```

Equivalent to get the current volume for each channel we used the `getChannels` methods

```typescript
const dap = new DiscreteAudioPlugin(player);
...

// Get channel data for the master element.
const masterGains = player.api.getChannels();

// If we go via the player api.
const discreteGains = player.api.plugins.apDiscreteAudioPlugin.getChannels(discreteSrc);

// If we kept the reference to plugin.
const discreteGains = dap.getChannels(discreteSrc);
```


**1.4.x**

We've now collected the channel control features in a separate plugin. The `ChannelControlPlugin` have a setting (enabled by default) to automatically register the master and all tracks that are added and removed from the discrete audio plugin. 

To set the channel volume for the master element using the `ChannelControlPlugin` we need the source url of the video

```typescript
const ccp  = new ChannelControlPlugin(player);
...

// If we go via the player api.
player.api.apChannelControlPlugin.setGains(player.master.src, [0, 1]);

// If we keept the reference to plugin.
ccp.setGains(player.master.src, [0, 1]);
```
The same goes for discrete audio tracks:

```typescript
const ccp  = new ChannelControlPlugin(player);
...

// If we go via the player api.
player.api.apChannelControlPlugin.setGains(discreteSrc, [0, 1]);

// If we kept the reference to plugin.
ccp.setGains(discreteSrc, [0, 1]);
```

To get the current channel volume use  `getGains` in the same manner:

```typescript
const ccp  = new ChannelControlPlugin(player);
...

// src is the source url either from master or a registered discrete audio track.
player.api.apChannelControlPlugin.getGains(src);

// If we keept the reference to plugin.
ccp.getGains(src);
```
