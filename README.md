# Accurate Player SDK Examples

This repository contains tutorials, framework examples and documentation for AP SDK.

In the `tutorials` folder you'll find tutorials for the player, plugins and UI components.

In the `frameworks` folder you'll find minimal examples of how to use the player, plugins and UI components in popular UI frameworks.

## Getting started

To run any of the tutorial- or framework projects you'll need a licence key for AP SDK and access to the npm repository.

1. Login to AP SDK npm repository to gain access to required dependencies, username and password will be provided to you by your sales contact.
> npm login adduser --registry=https://codemill.jfrog.io/codemill/api/npm/accurate-video/ --scope=@accurate-player

***NOTE: If you are using npm version 9+ you need to add the flag `--auth-type=legacy` to the npm command.***

2. Go to `license-key-example.js` and follow the instructions of how to provide your license key correctly.

### Running a demo

Each project folder contains a sample project that is launched by executing:

1. `pnpm install`
2. `pnpm start`

If any problem occurs during the player's license process, make sure that the setup above was followed correctly and retry running `pnpm install` in the project folder.

# Start building your own application

AP SDK itself is divided into a number of different sub-packages, each with its own features based on the requirements and types of content it handles. This is done to keep 
the player as small as possible, and only load the required packages based on your requirements.

We've gathers all resources you need to get started in the [AP SDK Portal](https://sdk.accurate.video/).

## Server requirement

In order to play media from your server, or from an S3 bucket, you need to make sure that the file is publicly accessible, and that the appropriate CORS headers are set.

If the media is served from an S3 bucket, the following CORS policy should be applied to the bucket:
 ```xml
  <?xml version="1.0" encoding="UTF-8"?>
  <CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
     <CORSRule>
         <AllowedOrigin>https://your-domain.com</AllowedOrigin>
         <AllowedMethod>GET</AllowedMethod>
         <AllowedHeader>*</AllowedHeader>
         <ExposeHeader>Content-Range</ExposeHeader>
         <MaxAgeSeconds>600</MaxAgeSeconds>
     </CORSRule>
  </CORSConfiguration>
 ```
 If the media is served from a web server, e.g. nginx, Apache, or IIS, make sure that the following headers are set by the server:

```
Access-Control-Allow-Origin: https://your-domain.com
Access-Control-Allow-Methods: *
Access-Control-Allow-Headers: *
Access-Control-Expose-Headers: *
Access-Control-Max-Age: 600
```
