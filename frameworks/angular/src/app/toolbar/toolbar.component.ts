import { frameToSMPTE, PlayerStatus, SeekMode, TimeFormat, VideoFile } from '@accurate-player/accurate-player-core';
import { ProgressivePlayer } from '@accurate-player/accurate-player-progressive';
import {Component, CUSTOM_ELEMENTS_SCHEMA, Input} from '@angular/core';
import "@material/mwc-icon";

@Component({
  selector: 'toolbar-component',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ToolbarComponent {
  @Input()
  player!: ProgressivePlayer;

  @Input()
  status!: PlayerStatus;

  @Input()
  currentFrame!: number;


  get fps(): number {
    return this.videoFile ? this.videoFile.frameRate.numerator / this.videoFile.frameRate.denominator : 24;
  }
  get videoFile(): VideoFile | undefined {
    return this.player?.api.getActiveFile();
  }
  get timeCode(): string {
    return frameToSMPTE(this.currentFrame ?? 0, this.fps, this.videoFile?.dropFrame!)
  }

  onAction(event: MouseEvent): void {
    const button = event.target as HTMLButtonElement;
    switch (button.value) {
      case "skip-back-long":
        this.player?.api.pause();
        this.player?.api.seek({
          time: 10,
          mode: SeekMode.RelativeBackward,
          format: TimeFormat.Frame
        });
        break;
      case "skip-back":
        this.player?.api.pause();
        this.player?.api.seek({
          time: 1,
          mode: SeekMode.RelativeBackward,
          format: TimeFormat.Frame
        });
        break;
      case "play-pause":
        this.player?.api.togglePlay();
        break;
      case "skip-forward":
        this.player?.api.pause();
        this.player?.api.seek({
          time: 1,
          mode: SeekMode.RelativeForward,
          format: TimeFormat.Frame
        });
        break;
      case "skip-forward-long":
        this.player?.api.pause();
        this.player?.api.seek({
          time: 10,
          mode: SeekMode.RelativeForward,
          format: TimeFormat.Frame
        });
        break;
      default:
        break;
    }
  }
}





