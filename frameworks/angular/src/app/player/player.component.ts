import { ApAudioMeterBasic } from '@accurate-player/accurate-player-components-external';
import { PlayerEventType, PlayerStatus } from '@accurate-player/accurate-player-core';
import { AudioScrubPlugin, ChannelControlPlugin, HotkeyPlugin, SmoothTimeUpdateEventType, SmoothTimeUpdatePlugin } from '@accurate-player/accurate-player-plugins';
import { ProgressivePlayer } from '@accurate-player/accurate-player-progressive';
import {Component, CUSTOM_ELEMENTS_SCHEMA, ElementRef, ViewChild} from '@angular/core';
import '@accurate-player/accurate-player-controls';
import {ToolbarComponent} from "../toolbar/toolbar.component";
ApAudioMeterBasic.register();

@Component({
  selector: 'player-component',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
  imports: [
    ToolbarComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PlayerComponent {
  private _player!: ProgressivePlayer;

  @ViewChild("apcRef", { static: true })
  apcRef: ElementRef | undefined;

  @ViewChild("masterRef", { static: true })
  set masterRef(masterRef: ElementRef) {
    const master = masterRef?.nativeElement;
    if (!master) {
      return;
    }
    // Create player
    const newPlayer = new ProgressivePlayer(master, window.LICENSE_KEY);
    // Load plugins
    new HotkeyPlugin(newPlayer);
    new SmoothTimeUpdatePlugin(newPlayer);
    new ChannelControlPlugin(newPlayer); // required by the audio meter
    new AudioScrubPlugin(newPlayer);
    // Load a video
    newPlayer.api.loadVideoFile({
      label: "Sintel (5.1)",
      src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/original/sintel-2048-surround.mp4",
      enabled: true,
      channelCount: 6,
      frameRate: {numerator: 24, denominator: 1},
      dropFrame: false,
    });
    newPlayer.on(PlayerEventType.StatusChanged, (event) => {
      this.status = event.status;
    });
    newPlayer.on(SmoothTimeUpdateEventType.TimeUpdate, (event) => {
      this.currentFrame = event.currentFrame;
    })
    this.player = newPlayer;

    this.apcRef?.nativeElement.init(
      master,
      this.player
    )
  }

  player!: ProgressivePlayer;
  status!: PlayerStatus;
  currentFrame!: number;
  channelLabels = ["L", "R", "C", "LFE", "Ls", "Rs"];

  constructor() {

  }

  togglePlay(): void {
    this.player?.api.togglePlay();
  }

}





