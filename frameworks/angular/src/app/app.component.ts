import { Component } from '@angular/core';
import "../../public/license-key.js";
import {PlayerComponent} from "./player/player.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  imports: [
    PlayerComponent
  ]
})
export class AppComponent {
  title = 'angular';

}
