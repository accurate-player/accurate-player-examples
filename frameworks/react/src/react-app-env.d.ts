/// <reference types="react-scripts" />

export declare global {
  namespace JSX {
    interface IntrinsicElements {
      "mwc-icon": any,
      "ap-audio-meter-basic": any,
      "ap-timeline-time-input": any,
      "apc-controls": any,
    }
  }

  interface Window {
    // Defined by public/license-key.js
    LICENSE_KEY: string;
  }
}
