import './App.css';
import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css";
import "@material/mwc-icon";
import logo from "./logo.svg"

import "@accurate-player/accurate-player-controls";

import React, { useCallback, useEffect, useRef, useState } from 'react';
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";
import { AudioScrubPlugin, ChannelControlPlugin, HotkeyPlugin, SmoothTimeUpdateEventType, SmoothTimeUpdatePlugin } from "@accurate-player/accurate-player-plugins";
import { frameToSMPTE, Player, PlayerEventType, PlayerStatus, SeekMode, TimeFormat } from "@accurate-player/accurate-player-core";
import { ApAudioMeterBasic } from "@accurate-player/accurate-player-components-external";


ApAudioMeterBasic.register();

function Toolbar({player, status, currentFrame}: { player?: Player, status?: PlayerStatus, currentFrame?: number }) {
  const onAction = useCallback<React.MouseEventHandler<HTMLButtonElement>>((event) => {
    const button = event.target as HTMLButtonElement;
    switch (button.value) {
      case "skip-back-long":
        player?.api.pause();
        player?.api.seek({
          time: 10,
          mode: SeekMode.RelativeBackward,
          format: TimeFormat.Frame
        });
        break;
      case "skip-back":
        player?.api.pause();
        player?.api.seek({
          time: 1,
          mode: SeekMode.RelativeBackward,
          format: TimeFormat.Frame
        });
        break;
      case "play-pause":
        player?.api.togglePlay();
        break;
      case "skip-forward":
        player?.api.pause();
        player?.api.seek({
          time: 1,
          mode: SeekMode.RelativeForward,
          format: TimeFormat.Frame
        });
        break;
      case "skip-forward-long":
        player?.api.pause();
        player?.api.seek({
          time: 10,
          mode: SeekMode.RelativeForward,
          format: TimeFormat.Frame
        });
        break;
      default:
        break;
    }
  }, [player]);
  const videoFile = player?.api.getActiveFile();
  const fps = videoFile ? videoFile.frameRate.numerator / videoFile.frameRate.denominator : 24;
  return (
    <div role="toolbar" aria-label="Video player controls" aria-controls="player">
      <div className="button-group">
        <button type="button" aria-pressed="false" value="skip-back-long" onClick={onAction}>
          <mwc-icon class="icon" aria-hidden="true">fast_rewind</mwc-icon>
        </button>
        <button type="button" aria-pressed="false" value="skip-back" onClick={onAction}>
          <mwc-icon class="icon" aria-hidden="true">skip_previous</mwc-icon>
        </button>
        <button type="button" aria-pressed="false" value="play-pause" onClick={onAction}>
          {status?.paused ? <mwc-icon class="icon" aria-hidden="true">play_arrow</mwc-icon> : <mwc-icon class="icon" aria-hidden="true">pause</mwc-icon>}
        </button>
        <button type="button" aria-pressed="false" value="skip-forward" onClick={onAction}>
          <mwc-icon class="icon" aria-hidden="true">skip_next</mwc-icon>
        </button>
        <button type="button" aria-pressed="false" value="skip-forward-long" onClick={onAction}>
          <mwc-icon class="icon" aria-hidden="true">fast_forward</mwc-icon>
        </button>
      </div>
      <span className="time-code">{frameToSMPTE(currentFrame ?? 0, fps, videoFile?.dropFrame!)}</span>
    </div>
  )
}

function PlayerComponent() {
  const [player, setPlayer] = useState<Player>();
  const [master, setMaster] = useState<HTMLVideoElement>();
  const [status, setStatus] = useState<PlayerStatus>();
  const [currentFrame, setCurrentFrame] = useState<number>();
  const audioMeterRef = useRef<ApAudioMeterBasic>();

  type ApcControls = {
    init: (master: HTMLVideoElement | undefined, player: Player | undefined) => void;
  };

  const apcRef = useCallback((apcRef: ApcControls) => {
    if (!apcRef || !player || !master) {
      return;
    }
    // Init accurate player controls
    apcRef.init(
      master,
      player
    );
  }, [player, master])

  const masterRef = useCallback((master: HTMLVideoElement) => {
    if (!master) {
      return;
    }
    // Create player
    const newPlayer = new ProgressivePlayer(master, window.LICENSE_KEY);
    // Load plugins
    new HotkeyPlugin(newPlayer);
    new SmoothTimeUpdatePlugin(newPlayer);
    new ChannelControlPlugin(newPlayer); // required by the audio meter
    new AudioScrubPlugin(newPlayer);
    // Load a video
    newPlayer.api.loadVideoFile({
      label: "Sintel (5.1)",
      src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/original/sintel-2048-surround.mp4",
      enabled: true,
      channelCount: 6,
      frameRate: {numerator: 24, denominator: 1},
      dropFrame: false,
    });
    newPlayer.on(PlayerEventType.StatusChanged, (event) => {
      setStatus(event.status);
    });
    newPlayer.on(SmoothTimeUpdateEventType.TimeUpdate, (event) => {
      setCurrentFrame(event.currentFrame)
    })
    setPlayer(newPlayer);
    setMaster(master);
  }, []);

  useEffect(() => {
    // TODO: Use @lit-labs/react when this is merged: https://github.com/lit/lit/pull/2800 and set properties in JSX
    const audioMeter = audioMeterRef.current;
    if (audioMeter != null) {
      if (player != null) {
        audioMeter.player = player;
        audioMeter.soloMuteEnabled = true;
        audioMeter.channelLabels = ["L", "R", "C", "LFE", "Ls", "Rs"];
      }
    }
  }, [player])

  return (
    <div className="player">
      <div className="side-by-side">
        <ap-audio-meter-basic ref={audioMeterRef}></ap-audio-meter-basic>
        <div className="video-container">
          <div className="video">
            <video id="player" ref={masterRef} crossOrigin="anonymous" onClick={() => player?.api.togglePlay()}></video>
            <apc-controls ref={apcRef}></apc-controls>
          </div>
          <Toolbar player={player} status={status} currentFrame={currentFrame}></Toolbar>
        </div>
      </div>
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <header>
        <img src={logo} alt="React logotype"/>
        <h1>React</h1>
      </header>
      <PlayerComponent></PlayerComponent>
    </div>
  );
}

export default App;
