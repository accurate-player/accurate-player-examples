import { createApp } from 'vue'
import App from './App.vue'
import "../public/license-key";

createApp(App).mount('#app')
