# Documentation

This is just a simple way of browsing through our documentation. Follow the instructions below to access it in your browser.

1. `npm install`
2. `npm run start`
3. Open `http://localhost:8080` in your browser.
