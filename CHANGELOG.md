This repo covers both SDK Player and SDK Components. See future release notes at [https://accurate.video/docs/](https://accurate.video/docs/)

## Unreleased
- Added examples for mono mix, solo/mute buttons and channel labels.

# 6.0.0

## Breaking
- HlsPlayer: Removed loadUrl API, use loadVideoFile instead.

## Features
- HlsPlayer: Exposed hls.js config object in player settings to let the user fine tune hls.js parameters.
- HlsPlayer: Updated hls.js dependency to 1.0.9 (previously 0.14.17)

## Fixes
- CutlistPlayer: Increased allowed duration of black cuts to 1 hour.
- CutlistPlayer and ProgressivePlayer: The player would sometimes stop working in Firefox because the network requests for the video would be blocked. This is now fixed by properly releasing network resources when the player is destroyed.
- SmoothTimeUpdatePlugin: Now emits an event correctly for each frame.

# 5.3.1

## Fixes
- Updated broken libraries from 5.3.0 to 5.3.1

# 5.3.0

## Features
- HotkeyPlugin
  - Hotkeys on OSX now uses CMD instead of CTRL. Affects all plugins that has built in hotkeys like the PointPlugin.
- IMSCPlugin
  - Updated dependencies to properly support SMPTE timebase.
- CutlistPlayer
  - Calling play() after playback ended now start playback from beginning.
- Updated all internal dependencies to the lastest versions.

# 5.2.0

## Features

- CutlistPlayer
  - Added support for black frame playback.
  - Added option to keep current time when reloading cutlist.

## Fixes

- Controls
  - Prevent controls from crashing when no video file is found
  - Fullscreen mode will now work even if IMSC plugin doesn't load any subtitles
  - Seek popup will now blur focus after popup is closed
  - Fixed layout issues in Firefox related to focus when tabbing
- ChannelControlPlugin
  - ChannelControlEventType.Added will now be emitted correctly
  - Fixed bug which caused discrete audio tracks to sound even though player was globally muted if the discrete audio was loaded after the player was
    globally muted
- Player API: api.muted and api.getStatus().muted will now report the same state

# 5.1.0

## Features
- All players: Added regular seek strategy. Can be selected through PlayerSettings when creating a player. See
[SeekStrategy documentation](https://accurate-player.gitlab.io/accurate-player-examples/api/progressive/classes/_accurate_player_accurate_player_core.regularseek.html).

## Fixes
- Controls: Fixed bug where you could not toggle the discrete audio tracks using the player controls if an id was set on the track
that differed from the src.

# 5.0.0

## Breaking
For migration see MIGRATION.md

- AudioScrubPlugin: The AudioScrubTrack interface changed. The nativeElement property is now called shadowElement, and a trackId and masterElement property were added.
- AudioScrubPlugin: updateOffset and unregisterTracks now takes a track ids or sources instead of html elements.
- AudioScrubPlugin: unregisterTracksFromSource has been replaced by unregisterTracks.
- ChannelControlPlugin: The events emitted will now have the track id if available, otherwise fall back to src (before they always contained the src regardless)

## Features
- Controls + AbrPlayer: Labels on audio tracks (if set) are now used in the audio select UI.
- AbrPlayer: `player.api.getAudioTracks()` now returns more information about the audio track (if available). Also, the `role` property has been deprecated in favor of `roles`. For more information see `ShakaPlayerTrack` in the documentation.
- All players: Added API methods for mute/unmute muxed audio only `player.api.muteMuxed()`, `player.api.unmuteMuxed()`, `player.api.toggleMuteMuxed()`.

## Fixes
- Controls: Fixed bug where muting the muxed audio did not work if ChannelControlPlugin was loaded.
- AbrPlayer: Fixed bug that caused the wrong audio track to be selected when calling `player.api.setAudioTrack`.
- Fixed a bug where `player.api.toggleMute()` did not mute/unmute all audio (discrete audio tracks for example). It now mutes/unmutes all audio exactly like the `player.api.mute()` and `player.api.unmute()` methods.

# 4.5.0

## Features

- ChannelControlPlugin: The plugin now overrides the default volume/muted controls when loaded to ensure that the volume does not
 affect the audio analyse values used by the VU-meter for example.
- SmoothTimeUpdatePlugin: Now uses requestVideoFrameCallback to improve performance on browsers that support it.

## Fixes

- ChannelControlPlugin: Added support for mono-output in the default audio router.
- ChannelControlPlugin: Emit channel change event when removing source, in case the implicitly muted state has changed.
- ImscSubtitlePlugin: Removed z-indexing from cues to prevent them from ending up above other elements.


# 4.4.0

## Features

- AudioScrubPlugin: Added events: PrePlay, PostPlay, PrePause and PostPause. These are used internally to get better timings when
 reading audio data from ChannelControlPlugin for our VU-meter.

## Fixes

- AudioScrubPlugin: Fixed bug where seeking to a position outside the current time resulted in the wrong audio being played (the
  audio from the previous position was played).
- ChannelControlPlugin: Fixed issue where getMaxRMSdB would sometimes report back empty values for shadow elements (from
 AudioScrubPlugin) caused by ChannelControlPlugin being out of sync with AudioScrubPlugin.
- ChannelControlPlugin: Fixed issue when removing track with soloed channels.
- Update internal dependencies.

# 4.3.1

## Features

- Core: Added API for looping playback of a part of the video.
- Core: Util for receiving platforms Windows/Linux/Mac.
- ImscSubtitlePlugin: Added load event to IMSC subtitle plugin.
- ImscSubtitlePlugin: Added text extraction to IMSC subtitle plugin for retrieving entire text content from an ISD.
- ImscSubtitlePlugin: Added startTime property to allow loading subtitles with offset in cue timings.
- PointPlugin: Added default hotkeys for looping playback between points.
- Probe: Add property to getMediaInfo API to indicate accuracy.
- VttSubtitlePlugin: Added startTime property to allow loading subtitles with offset in cue timings.

## Fixes

- Core: Fixed seek issues with VFR files.

# 4.2.0

## Features

- AudioScrubPlugin: Added support for when pre-signed urls are changed.
- ChannelControlPlugin: Additional events to detect web audio related events.
- DiscreteAudioPlugin: Added presigned-url support, you can now provide a updateSrc callback property that is used to provide the
 plugin with the updated url for the audio track in case the audio track produces a network error.
- DiscreteAudioPlugin: Added "id" property to DiscreteAudioTrack that will be used as identifier, url used as default.
- ImscSubtitlePlugin: Added compatibility tools.
- PointPlugin: Possibility to register custom point validation
    - Added default validator to validate that in point is set before the out point.
    - Added interface to allow for writing custom validators.

## Fixes

- AudioScrubPlugin: Set volume of scrub audio on player volume change.
- DiscreteAudioPlugin: Moved initial load after event listeners are added to the audio element to allow catching errors on the
 initial load

# 4.1.0

## @accurate-player/accurate-player-core

- Enable registering shaka player response filters through ABRPlayer api.
- Added presign-url support for Progressive Player.
- Improved performance of getMediaInfo().
- Properly propagate error from shakaplayer if load fails.

## @accurate-player/accurate-player-plugins

- IMSC Plugin: Fixed an issue where continuously disabling and re-enabling subtitles could affect performance negatively.
- IMSC Plugin: Subtitles now clear properly when loaded with zero files.

# 4.0.4

## @accurate-player/accurate-player-core

- Fixed npmpeer dependency causing `npm install` to fail

# 4.0.3

## @accurate-player/accurate-player-core

- Fixed issues in imsc subtitle plugin causing styling to break when leaving full screen.

# 4.0.2

## @accurate-player/accurate-player-core

- Fixed issue where player could crash if initialized improperly.

# 4.0.1

## @accurate-player/accurate-player-plugins

### Fix
- ImscSubtitlePlugin: cues missplaced after exiting fullscreen

# 4.0.0

## @accurate-player/accurate-player-core

### Breaking
For migration see MIGRATION.md

- Moved util `getMediaInfo` to separate package `@accurate-player/probe`.
- Removed deprecated player `DashPlayer`.

## @accurate-player/accurate-player-plugins

### Features
- New plugin for IMSC subtitles.
- Added filter api to Hotkey plugin.

## @accurate-player/accurate-controls

### Feature
- Support for new IMSC subtitle plugin.

# 3.4.2

## @accurate-player/accurate-player-core

### Fix
- Player status property were not initialized when loading video through `loadVideoFile` causing `accurtate-controls` to crash on init.

# 3.4.1

## @accurate-player/accurate-player-core

### Features

- Update license client to allow session time.
- **Support for VFR**
- Optional `userPause` parameter to pause. (Signal if plugin or user pauses)

## @accurate-player/accurate-player-plugins

### Features

- Don't trigger `UserPause` event if DAP is buffering.

## @accurate-player/accurate-controls

### Features

- Support for VFR videos.

# 3.3.2

## @accurate-player/accurate-player-core

### Fix
- Avoiding promises from play never resolving.

## @accurrate-player/accurate-player-plugins

### Fix
- [Discrete Audio Plugin] Avoid "Unhandled promise rejection" console error.

## @accurate-player/accurate-controls

### Feature
- Added settings to configure label text in "subtitle-settings" popup menu. See [documentation](https://accurate.video/docs/tutorials/how-integrate-accurate-player-controls/) for more details.

### Fix
- Fixed timing issue where initializing VTTSubtitlePlugin after init completes lead to "None" radio button if using ABRPlayer.

# 3.3.1

## @accurate-player/accurate-player-core

### Fix
 - AbrPlayer: Avoid crash if no active representation found.


# 3.3.0

## @accurate-player/accurate-player-core

### Feature
 - ABR-Player: Added api method to clear request interceptors
 - Added optional parameter "once" to EventMixin.
 - Master is retrieved from adapter layer, making cutlistPlayer return currently playing as master.
 - AbrPlayer & DashPlayer: Bumped Shaka-player dependency to 3.0.2

## Fix
 - CutlistPlayer: Emit VideoFileLoaded on internal eventbus.
 - AbrPlayer: Save shaka settings when loading new file.
 - AbrPlayer: Clear request-filters when loading files.
 - Return reason for rejected play promise
 - Avoid console error for internal promise rejections.

## @accurate-player/accurate-player-plugins

## Feature
 - Ported Audio scrub plugin to handle cutlist player.

## @accurate-player/accurate-player-controls

# 3.2.1

## @accurate-player/accurate-player-core

### Fix
- Video element stuck in seeking causing player freeze.

### @accurate-player/accurate-player-plugins
### @accurate-player/accurate-player-controls

# 3.2.0
## @accurate-player/accurate-player-core

### Feature
- Support trickmode in cutlist player.

### Fix
- `getMediaInfo` - ignore frames with a duration different from all other frame durations when detecting framerate.

## @accurate-player/accurate-player-plugins
## Fix
- ChannelControlPlugin throws exception if player destroyed instantly after plugin registred.

## @accurate-player/accurate-player-controls
## Feature
- Added setting to disable toggle play on video click.

# 3.1.0
## @accurate-player/accurate-player-core

### Feature
- Players destroy method now return promise. 
- Added new player "ABRPlayer" - shaka implementation to support DASH and HLS content.
- Enhance precision for CutlistPlayers transitions timing.
- Added api methods to update drop frame and frame offset property to already loaded video files.

### Fix
- Progressive player: reset playback rate on video change to align with video element.
- Progressive player: destroy missed event listener.

### Deprecation
- DashPlayer - Use ABR Player instead, see Migration.md

## @accurate-player/accurate-player-controls

### Feature
- Added support for new ABR Plater.

# 3.0.0
## @accurate-player/accurate-player-core

### Feature
- Pause return promise, that is resolved when the player have paused.
- Added optional pauseOn argument to pause. Make the player pause on a specific time.

### Fix
- Cutlistplayer: Sometimes cutlist player fails to switch cut.

## @accurate-player/accurate-player-plugins

### Breaking

Please consult Migration.md for additional information about breaking changes.

- ChannelControlPlugin: When using DASH or HLS player the master element is registred by manifest-url, not native element src.
- Removed deprecated S3Plugin

### Fix

- DiscreteAudioPlugin: Catch unhandled rejection from play operation.
- Corrected spelling of hotkey descriptions for volume and playback.

## @accurate-player/accurate-player-controls

## Feature
- Support for AP 3.0.0

# 2.3.0
## @accurate-player/accurate-player-core
### Feature
- Added `player.api.isSeeking` property that returns true if a seek is in progress and false otherwise.
- `player.api.seek()` now returns a promise that is resolved when the seek is complete.
- Updated license client 0.2.2
- Include accurate player version to license validation request.

### Fix
- Prevent setting out of limits [0, 1].
- Trigger status events when setting playback rate to values negative and above 8.
- getMediaInfo now throws an error with a proper error message for unknown file types
- Player illegal paused state if ended
- getStatus should compensate for videoOffset
- Cutlist player
  - unsubscribe events correctly on destroy
  - seek to 100%
  - end on wrong timecode.

## @accurate-player/accurate-player-plugins
### Feature
 - SubtitlePlugin: Added functionality for adding a cue to a track
 - SubtitlePlugin: Added functionality for deleting a cue in a track
 - SubtitlePlugin: Added functionality for adding a global offset to all subtitle tracks

### Fix
 - [bug] SubtitlePlugin: Fixed broken demo page

## @accurate-player/accurate-player-controls
### Fix
 - Adjust in/out point for frame offset.


# 2.2.2

## @accurate-player/accurate-player-core
### Feature
- Added utillity method `userInputToFrame` converting a user string to a frame.
### Fix
- Improved buffering logic: Don't trigger loading if seeked-time already buffered.
- Removed unused dependency: Axios.
- Player destroy sets property api to undefined.


## @accureate-player/accurate-player-plugins
### Feature
- SubtitlePlugin: Added functionality for editing a single cue in a track.
### Fix
- DiscreteAudioPlugin: Fixed offset issues.
### Deprecation
- S3SDKPlugin deprecated in favor of `getPresignedUrl` from `@accurate-player/accurate-player-core`.

# 2.2.1

Minor bug fixes in the Plugin package.

## @accurate-player/accurate-player-plugins:
- [DiscreteAudioPlugin] Fixed synchronisation bug when pausing.
- [AudioScrubPlugin] Fixed bug when updating offset of non-existing track.
- [PointPlugin] Pause player before seeking to in/out point.

# 2.2.0
## @accurate-player/accurate-player-core:
### Feature
- Emit event when loading VideoFile
- Internal seek event now contains frame

## @accurate-player/accurate-player-plugins:
### Feature:
- Set point at last requested seek when seeking.
- Added optional channel count to ASP.
### Fix:
- Console error when soloing channel > 2 in combination with AudioScrubPlugin.
- Pause discrete audio when load new videoFile.
- Bug when ASP Tracks removed from CCP.
- Bug ASP-shadows channel count not updated when master proxy changes.

## @accurate-player/accurate-player-controls
### Feature:
- Now supports smooth time update plugin.

# 2.1.0
# @accurate-player/accurate-player-core
### Feature
- New player: CutlistPlayer (Experimental).
- Trick mode available with DashPlayer and HlsPlayer.
- More internal player events, Seeked, Pause & TimeUpdate.
### Fix
- Current time same from status as getter in service.

### Miscellaneous
- Refactored progressive player.

# 2.0.2
## @accurate-player/accurate-player-core
### Fix:
- Improve getMediaInfo to extract channel layout from AAC tracks.
- Better handling of invalid SMPTE timecodes.


# 2.0.1
## @accurate-player/accurate-player-core
### Feature:
- Added util methods isChrome and isSafari to determine users browser.
- Added util getMediaInfo() to parse media info from mp4 container.

# 2.0.0
## @accurate-player/accurate-player-core
### Breaking:
- AssetService, MarkerService and adapter types moved to internal Codemill repository.
​
## @accurate-player/accurate-player-dash
### Feature:
- Support for request filters. Also utils for s3 presigned url filter.
​
## @accurate-player/accurate-player-plugins
### Breaking:
- SubtitlePlugin renamed to VttSubtitlePlugin. *See migration.md*
​​
### Feature:
- [SP] Added SubtitlePlugin that supports internal TimeSpan subtitle format.
- [CCP] Emit events when proxy changes.

# 1.7.1
## @accurate-player/accurate-player-core
### Feature:
- Additional util methods for timecode conversions added.

### Bugfix:
- Fixed issue with player throwing exception if calling `getCurrentVideoFile` when player initializes.

## @accurate-player/accurate-player-plugins
### Bugfix:
- [STU] Remove remaining event listener in destroy.

# 1.7.0
## @accurate-player/accurate-player-plugins
### Bugfix:
- Hotkeys stop working after ALT+TAB
- Non started tracks with offset plays when player paused.
- Audio scrub plugin now represents shadows as AudioScrubTrack in order to support audio offset. Internal API and events are affected.
### Feature:
- [CCP] Auto-resume AudioContext on Play and Seek

# 1.6.1
## @accurate-player/accurate-player-core
### Feature:
- Add update, delete and post metadata into asset service.

### Bugfix:
- Remove stuttering behaviour when pausing
- Don't overwrite global headers in asset and marker service

## @accurate-player/accurate-player-plugins
### Feature:
- [STU] Add smooth time update plugin (based on requestAnimationFrame).
- [CCP] Auto register shadows from audio scrub plugin.
- [CCP] Add util / convenience methods to parse audio data for VU-meter

### Bugfix:
- Abort connections if player hangs due to chrome bug.
- Workaround for discrete audio not loading in firefox.


## @accurate-player/accurate-player-controls
### Bugfix:
- Seek component does not handle frame offset


# 1.6.0
## @accurate-player/accurate-player-core
- AssetService and MarkerService: Expose HTTP client to extended classes.
## @accurate-player/accurate-player-plugins
- DiscreteAudioPlugin: Added interface to input for `initDiscreteAudioTracks`.
### Breaking:
- ChannelControlPlugin: Refactored solo and mute logic.
  - isMuted and isSolo replaced with getChannelMutedState. *See migration.md*
  - Solo affect muted state of all tracks. Therefor ChannelChangedEvent now contains an array of all registered tracks.
  - ChannelChanged event is no longer triggered by rerouting channels, replaced with `AudioChannelRouteChangedEvent`.

# 1.5.1

## @accurate-player/accurate-player-plugins

- ChannelControlPlugin: Error during cleanup of uninitialized track.

# 1.5.0

## @accurate-player/accurate-player-core

### Feature:

- Expose play() promise.
- Add asset list operation to asset service.

### Bugfix:

- Duration is undefined during proxy change.

## @accurate-player/accurate-player-plugins

### Breaking:

- All plugins: Deprecated plugin properties `isLoaded` and `isMuted` replaced with `loaded` and `muted` respectively.
- SubtitlePlugin: Deprecated property `getSubtitles` replaced with `subtitles`.
- DiscreteAudioPlugin: Deprecated property `getAudioTracks` replaced with `audioTracks`.
- ChannelControlPlugin: Replaced panning with channel reroute.
  - set/get panning is removed. Use `rerouteChannels` and `getRouteMap` instead.
  - Event `AudioChannelChanged` data field `pans: number[]` is replaced with `routeMap: RouteMap`.
  - Setting to enable panning is removed.
  - Setting `channelMerger` is replaced with `channelReroute`. `channelReroute` should return a default `RouteMap` to be used when audio tracks are created. Plugin assumes SMPTE-ordered input-file as default.

### Feature:

- ChannelControlPlugin: Allow mute of specific channels without affecting gain values.
- ChannelControlPlugin: Expose method for resuming audio context.
- ChannelControlPlugin: Add support to solo one or more channels.


## @accurate-player/accurate-player-controls

### Bugfix:

- Fixed issue with `autoHide` settings

# 1.4.3

## @accurate-player/accurate-player-core

### Feature:

- Optimize status retrieval.

### Bugfix:

- Leaving trickmode cases console error in Chrome.

## @accurate-player/accurate-player-plugins

### Feature:

- ChannelControlPlugin: Allow mute of specific channels without affecting gain values.
- ChannelControlPlugin: Expose method for resuming audio context.

## @accurate-player/accurate-player-controls

### Feature:

- Add `dropFrame` as an optional setting that can be modified in UI (for proxies with uneven framerate).
- Display error popup on media error.

### Bugfix:

- Flexbox issue where text is hidden by scrollbar.

# 1.4.2

## @accurate-player/accurate-player-core

### Bugfix:

- Seek to end now works

## @accurate-player/accurate-player-plugins

### Feature:

- DiscreteAudioPlugin now supports audio offset.
- PointPlugin allow setting points from frame input.
- Added experimental `AudioScrubPlugin`

## @accurate-player/accurate-player-controls

### Bugfix:

- Exception thrown when muting combined with ChannelCount.
- Minor UI fixes.

# 1.4.1

## @accurate-player/accurate-player-core

### Feature:

- Improve time adjustment on pause.
- Support non-zero timecodes (timecode offset).
- Dropframe setting is now optional.

## @accurate-player/accurate-player-controls

### Feature:

- Support non-zero timecode (timecode offset).

### Bugfix:

- Styling issue in Edge.
- Hotkeys doesn't work after clicking time-bar component.
- Added width and height to video element when in fullscreen.

# 1.4.0

## @accurate-player/accurate-player-core

### Feature:

- Add optional parameter offset to secondsToTimecode allowing an optional offset to be added.
- Add property player.about which returns the current version of all components.
- Update assetService and markerService to accept custom headers.

### Breaking:

- Channel control moved from core to a separate plugin. See [MIGRATION.md](./MIGRATION.md).

### Bugfix:

- Fix player.off() deregistration of listeners.
- Pre releases should not update the dist-tag latest.
- getCurrentTime(TimeFormat.Milliseconds) displays incorrect millisecond value.
- getCurrentTime(TimeFormat.Percent) shows fractions instead of percent value.

## @accurate-player/accurate-player-plugins
### Feature:

- Add version property that returns the current version the each plugin.
- `ChannelControlPlugin` added.

### Breaking:

- Channel control moved from `DiscreteAudioPlugin` to a separate plugin. See [MIGRATION.md](./MIGRATION.md).

### Bugfix:

- [HotkeyPlugin] Don't trigger hotkeys when certain HTMLElements are in focus.
- [SubtitlePlugin] Track is undefined in SubtitleRemovedEvent payload.
- Pre releases should not update the dist-tag latest.

## @accurate-player/accurate-player-controls

### Breaking:

Changed prefix from ap to apc. This affects component names and css-variables. See [MIGRATION.md](./MIGRATION.md).

### Bugfix:

Pre releases should not update the dist-tag latest.
Fix issue where discrete audio tracks did not react to global mute

# 1.3.0
First version of Accurate Player Examples released! Complete with examples for:
* Vanilla JavaScript
* React
* Angular
* Vue
